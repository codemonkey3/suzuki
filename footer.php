<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rs-theme
 */

$options = get_option( 'rs_theme_theme_options' );

// get ACF settings

// social media align-items-center
$facebook_username 	= get_field('field_5deb573179c45', 'option');
$instagram_username = get_field('field_5deb574179c46', 'option');
$youtube_url 		= get_field('field_5deb575279c47', 'option');

// Fixed Menu
$compare 			= get_field('field_5df9f031372e0', 'option');
$find_a_dealer 		= get_field('field_5df9f042372e1', 'option');
$contact_us 		= get_field('field_5df9f052372e2', 'option');
?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="site-footer__top">

			<div class="container">
				<div class="row">
					<div class="col-lg-4">

						<div class="company-info pr-lg-5 text-center text-lg-left">
							<?php
								if ( is_active_sidebar( 'footer-1' ) ) {
									dynamic_sidebar( 'footer-1' );
								}
							?>
						</div>
						
					</div>

					<div class="col-lg-4">
						<div class="suzuki-menu text-center text-lg-left">
							<?php
								if ( is_active_sidebar( 'footer-2' ) ) {
									dynamic_sidebar( 'footer-2' );
								}
							?>
						</div>
					</div>

					<div class="col-lg-2">
						<div class="suzuki-hotline text-center text-lg-left">
							<?php
								if ( is_active_sidebar( 'footer-3' ) ) {
									dynamic_sidebar( 'footer-3' );
								}
							?>
						</div>
					</div>
					
					<div class="col-lg-2">
						<div class="suzuki-connect float-lg-right text-center text-lg-left">
							<?php if ( is_active_sidebar( 'footer-4' ) ) :
								dynamic_sidebar( 'footer-4' );
								?>

								<ul class="m-0 list-unstyled d-flex justify-content-center justify-content-lg-right">
									<?php if( $facebook_username ) : ?>
										<li class="facebook">
											<a href="https://www.facebook.com/<?php echo $facebook_username; ?>">
												<svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path fill-rule="evenodd" clip-rule="evenodd" d="M7.43315 6.66514H10.8294L10.4049 9.63687H7.43315V18.5521H3.02862V9.63687H0.640625V6.66514H3.02862V4.66851C3.02862 3.36837 3.40009 2.38554 4.14302 1.72C4.88596 1.05445 6.12417 0.72168 7.85769 0.72168H10.8294V3.69341H9.02515C8.35297 3.69341 7.9196 3.78628 7.72502 3.97201C7.53044 4.15774 7.43315 4.4673 7.43315 4.90068V6.66514Z" fill="white"/>
												</svg>
											</a>
										</li>
									<?php endif; ?>

									<?php if( $instagram_username ) : ?>
										<li class="instagram">
											<a href="https://www.instagram.com/<?php echo $instagram_username; ?>">
												<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M12.8171 0.72168H5.39854C2.87922 0.72168 0.82959 2.77142 0.82959 5.29076V12.7094C0.82959 15.2288 2.87922 17.2785 5.39854 17.2785H12.8171C15.3366 17.2785 17.3863 15.2287 17.3863 12.7094V5.29076C17.3864 2.77142 15.3366 0.72168 12.8171 0.72168ZM15.9174 12.7094C15.9174 14.4188 14.5266 15.8095 12.8172 15.8095H5.39854C3.68922 15.8096 2.29858 14.4188 2.29858 12.7094V5.29076C2.29858 3.58143 3.68922 2.19068 5.39854 2.19068H12.8171C14.5266 2.19068 15.9173 3.58143 15.9173 5.29076V12.7094H15.9174Z" fill="white"/>
													<path d="M9.10804 4.73535C6.7556 4.73535 4.8418 6.64912 4.8418 9.00151C4.8418 11.3538 6.7556 13.2675 9.10804 13.2675C11.4605 13.2675 13.3743 11.3538 13.3743 9.00151C13.3743 6.64912 11.4605 4.73535 9.10804 4.73535ZM9.10804 11.7984C7.5657 11.7984 6.31079 10.5437 6.31079 9.00141C6.31079 7.459 7.5656 6.20422 9.10804 6.20422C10.6505 6.20422 11.9053 7.459 11.9053 9.00141C11.9053 10.5437 10.6504 11.7984 9.10804 11.7984Z" fill="white"/>
													<path d="M13.5538 3.48804C13.2708 3.48804 12.9928 3.60262 12.7929 3.80339C12.592 4.00318 12.4766 4.28132 12.4766 4.56533C12.4766 4.84846 12.5921 5.1265 12.7929 5.32727C12.9927 5.52706 13.2708 5.64262 13.5538 5.64262C13.8378 5.64262 14.115 5.52706 14.3157 5.32727C14.5165 5.1265 14.6311 4.84836 14.6311 4.56533C14.6311 4.28132 14.5165 4.00318 14.3157 3.80339C14.1159 3.60262 13.8378 3.48804 13.5538 3.48804Z" fill="white"/>
												</svg>
											</a>
										</li>
									<?php endif; ?>
									
									<?php if( $youtube_url ) : ?>
										<li class="youtube">
											<a href="<?php echo $youtube_url; ?>">
												<svg width="20" height="15" viewBox="0 0 20 15" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M19.8849 3.66237C19.9615 4.99966 19.9998 6.32457 19.9998 7.6371C19.9998 8.94962 19.9615 10.2745 19.8849 11.6118C19.8849 12.4538 19.604 13.172 19.0421 13.7663C18.5058 14.3359 17.8418 14.6207 17.0501 14.6207C15.0836 14.7198 12.798 14.7693 10.193 14.7693C7.58809 14.7693 5.3024 14.7198 3.33593 14.6207C2.54424 14.6207 1.86746 14.3359 1.30562 13.7663C0.769308 13.172 0.501154 12.4538 0.501154 11.6118C0.424538 10.2745 0.38623 8.94962 0.38623 7.6371C0.38623 6.77033 0.437308 5.44543 0.539462 3.66237C0.539462 2.82038 0.807616 2.11458 1.34393 1.545C1.88023 0.950646 2.54424 0.653471 3.33593 0.653471C5.20024 0.554412 7.37102 0.504883 9.84825 0.504883H10.5378C13.015 0.504883 15.1858 0.554412 17.0501 0.653471C17.8418 0.653471 18.5058 0.950646 19.0421 1.545C19.604 2.11458 19.8849 2.82038 19.8849 3.66237ZM8.31594 11.2775L13.8706 7.6371L8.31594 3.95955V11.2775Z" fill="white"/>
												</svg>
											</a>
										</li>
									<?php endif; ?>
								</ul>		
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="site-footer__bottom">
			
			<div class="container">
				<div class="row align-items-center">

					<div class="col-lg-6">
						<?php wp_nav_menu( array( 
							'theme_location' => 'footer_menu',
							'menu_id' 		 => 'footer-menu',
							'menu_class' 	 => 'footer-menu list-unstyled ml-0 d-flex justify-content-center justify-content-lg-start',
							'container' 	 => 'ul'
						) ); ?>
					</div>
					
					<div class="col-lg-6">
						<div class="copyright text-center text-lg-right">
							<?php
								if( isset( $options['footer_text'] ) ) {
									echo wpautop( $options['footer_text'] );
								}
							?>
						</div>
					</div>
					
				</div>
			</div>

		</div>
		
	</footer><!-- #colophon -->

	<div class="fixed-menu">
		<ul class="m-0 list-unstyled">
			<li class="compare">
				<a href="<?php echo $compare['url']; ?>" target="<?php echo $compare['target']; ?>">
					<div class="media align-items-center">
						<svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
							<g clip-path="url(#clip0)">
								<path d="M17.0072 16.7494C15.1688 16.7494 13.6738 15.2544 13.6738 13.416C13.6738 11.5777 15.1688 10.0827 17.0072 10.0827C18.8455 10.0827 20.3405 11.5777 20.3405 13.416C20.3405 15.2544 18.8455 16.7494 17.0072 16.7494ZM17.0072 10.916C15.6288 10.916 14.5072 12.0377 14.5072 13.416C14.5072 14.7944 15.6288 15.916 17.0072 15.916C18.3855 15.916 19.5072 14.7944 19.5072 13.416C19.5072 12.0377 18.3855 10.916 17.0072 10.916Z" fill="#003145"/>
								<path d="M3.67415 16.7494C1.83582 16.7494 0.34082 15.2544 0.34082 13.416C0.34082 12.1085 1.11499 10.916 2.31249 10.3785C2.52165 10.2835 2.76915 10.3785 2.86332 10.5877C2.95749 10.7977 2.86415 11.0444 2.65415 11.1385C1.75499 11.5427 1.17415 12.436 1.17415 13.416C1.17415 14.7944 2.29582 15.916 3.67415 15.916C5.05249 15.916 6.17415 14.7944 6.17415 13.416C6.17415 13.2335 6.14915 13.0452 6.09415 12.821C6.03915 12.5969 6.17665 12.3719 6.39999 12.3177C6.62082 12.2627 6.84832 12.4002 6.90332 12.6235C6.97332 12.9102 7.00749 13.1694 7.00749 13.416C7.00749 15.2544 5.51249 16.7494 3.67415 16.7494Z" fill="#003145"/>
								<path d="M16.0475 5.20019C15.9667 5.12186 15.8575 5.08102 15.7442 5.08269C15.4475 5.09186 12.8408 5.21769 12.8408 6.74936C12.8408 8.28102 15.4475 8.40686 15.7442 8.41602C15.7483 8.41602 15.7533 8.41602 15.7575 8.41602C15.8658 8.41602 15.97 8.37436 16.0475 8.29852C16.1283 8.22019 16.1742 8.11186 16.1742 7.99936V5.49936C16.1742 5.38686 16.1283 5.27852 16.0475 5.20019Z" fill="#003145"/>
								<path d="M13.2572 6.74936C13.2131 6.74936 13.1697 6.7427 13.1256 6.7277L10.6256 5.89436C10.4072 5.82186 10.2897 5.58603 10.3622 5.3677C10.4364 5.14936 10.6731 5.03353 10.8897 5.10436L13.3897 5.9377C13.6081 6.0102 13.7256 6.24603 13.6531 6.46436C13.5939 6.63936 13.4314 6.74936 13.2572 6.74936Z" fill="#003145"/>
								<path d="M17.0077 13.8327C16.8635 13.8327 16.7227 13.7577 16.6452 13.6227L13.3118 7.78937C13.1977 7.58937 13.2668 7.3352 13.4677 7.22103C13.6643 7.10687 13.9202 7.17437 14.0368 7.37603L17.3702 13.2094C17.4843 13.4094 17.4152 13.6635 17.2143 13.7777C17.1493 13.8152 17.0785 13.8327 17.0077 13.8327Z" fill="#003145"/>
								<path d="M6.59067 12.9993H6.06983L4.83983 12.016L4.87983 11.936C4.98233 11.7302 4.899 11.4802 4.69317 11.3768C4.48733 11.276 4.23817 11.3577 4.134 11.5635L3.30067 13.2302C3.2365 13.3593 3.24317 13.5127 3.31817 13.6352C3.39567 13.7577 3.52983 13.8327 3.674 13.8327H6.59067C6.82067 13.8327 7.00733 13.646 7.00733 13.416C7.00733 13.186 6.82067 12.9993 6.59067 12.9993Z" fill="#003145"/>
								<path d="M13.7921 7.21353C13.6713 6.78269 13.3521 6.74936 13.2571 6.74936C11.0063 6.74936 8.86212 7.90436 7.64962 8.89853C7.58378 8.85686 7.50712 8.83269 7.42378 8.83269C6.58378 8.83269 4.18462 8.42603 4.16045 8.42186C4.03795 8.40019 3.91545 8.43436 3.82212 8.51436C3.72712 8.59353 3.67378 8.71019 3.67378 8.83269V9.25519C2.31295 9.29436 1.41545 9.53269 1.00128 9.96353C0.763783 10.211 0.754616 10.4494 0.757116 10.5169C0.767116 10.7402 0.95045 10.916 1.17378 10.916C2.86128 10.916 6.68962 13.2702 8.02295 14.1769C8.09212 14.2244 8.17295 14.2494 8.25712 14.2494H11.5905C13.0063 14.2494 13.8488 8.74103 13.8488 7.75936C13.8488 7.53019 13.8296 7.35103 13.7921 7.21353Z" fill="#003145"/>
							</g>
							<defs>
								<clipPath id="clip0">
									<rect x="0.34082" y="0.916016" width="20" height="20" fill="white"/>
								</clipPath>
							</defs>
						</svg>
						<span><?php echo $compare['title'] ?? 'Compare'; ?></span>
					</div>
				</a>
			</li>
			<li class="find-dealer">
				<a class="" href="<?php echo $find_a_dealer['url']; ?>" target="<?php echo $find_a_dealer['target']; ?>">
					<div class="media align-items-center">
						<svg width="13" height="18" viewBox="0 0 13 18" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M6.00782 0.887207C2.69532 0.887207 0.0078125 3.24239 0.0078125 6.14411C0.0078125 10.2332 6.00782 17.2438 6.00782 17.2438C6.00782 17.2438 12.0078 10.2332 12.0078 6.14411C12.0078 3.24239 9.32033 0.887207 6.00782 0.887207ZM6.00782 8.39314C4.92866 8.39314 4.05365 7.56745 4.05365 6.54909C4.05365 5.53074 4.92866 4.70505 6.00782 4.70505C7.08699 4.70505 7.962 5.53074 7.962 6.54909C7.962 7.56745 7.08699 8.39314 6.00782 8.39314Z" fill="#003145"/>
						</svg>
						<span><?php echo $find_a_dealer['title'] ?? 'Find a Delear'; ?></span>
					</div>
				</a>
			</li>
			<li class="contact-us">
				<a href="<?php echo $contact_us['url']; ?>" target="<?php echo $contact_us['target']; ?>">
					<div class="media align-items-center">
						<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" clip-rule="evenodd" d="M0.5075 0.5H15.5V12.5H3.5L0.5 15.5L0.5075 0.5ZM12.5 5.75H3.5V7.25H12.5V5.75ZM9.5 9.5H3.5V8H9.5V9.5ZM3.5 5H12.5V3.5H3.5V5Z" fill="#003145"/>
						</svg>
						<span><?php echo $contact_us['title'] ?? 'Contact Us'; ?></span>
					</div>
				</a>
			</li>
		</ul>
	</div>

	<div class="mobile-menu">
		<a href="javascript: void(0);" class="close-mobile-menu">
			<span></span>
			<span></span>
		</a>
		
		<nav id="mobile-navigation" class="mobile-navigation" role="navigation">						
			<?php wp_nav_menu( array( 
				'theme_location' => 'mobile_menu',
				'menu_id' 		 => 'mobile-menu',
				'menu_class' 	 => 'primary-menu',
				'container' 	 => 'ul'
			) ); ?>
		</nav><!-- #site-navigation -->
	</div><!-- .mobile-menu -->
	
</div><!-- #page -->

<link href="https://fonts.googleapis.com/css?family=Overpass:300,400,700&display=swap" rel="stylesheet">
<?php
	// include WP footer for hook and styles
	wp_footer();

	// display tracking or analytics code
	if( isset( $options['analytics'] ) ) {
		echo $options['analytics'];
	}
?>

</body>
</html>
