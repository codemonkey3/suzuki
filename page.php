<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rs-theme
 */

get_header();
?>

	<main id="main" class="site-main" role="main">
		<div class="container">
			
			<div class="row">
				
				<div class="col-lg-8">
					<div id="primary" class="content-area">
						
						<?php
							while ( have_posts() ) : the_post();

								get_template_part( 'template-parts/content', 'page' );
								
							endwhile; // End of the loop.
						?>
						
					</div><!-- #primary -->
				</div>

				<!--sidebar-->
				<?php get_sidebar(); ?>
				
			</div>

		</div> <!-- .container -->
	</main><!-- #main -->
<?php
get_footer();
