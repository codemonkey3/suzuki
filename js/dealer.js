var map;
var markers = [];
var infoWindow;
var pinImage = theme_ajax.theme_url + "/images/map-marker-icon.svg";

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 12.1796514, lng: 122.0145809},
        zoom: 6.76,
        mapTypeId: 'roadmap',
        disableDefaultUI: true,
        styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
    });
    
    infoWindow = new google.maps.InfoWindow();
    
    // set lists of dealer markers
    setMarkers(map);
}

// set markers on-load
function setMarkers(map) {
    let dealerInfos = suzuki.dealer_infos;
    
    for( var i = 0; i < dealerInfos.length; i++ ) {
        
        let  dealer = dealerInfos[i];
        createMarker( dealer );

    }
}

function createMarker( dealer ) {
    let address         = dealer[3].replace(/\n/g, "<br />"),
        openingHours    = dealer[4].replace(/\n/g, "<br />");
    
    let html = `
        <div class="item">
            <h4>${dealer[0]}</h4>
            <div class="address">
                ${address}
            </div>
            <div class="opening-hours mt-3">
                ${openingHours}
            </div>
        </div>
    `;

    let latlng = new google.maps.LatLng(
            parseFloat(dealer[1]),
            parseFloat(dealer[2])
        );
    
    let marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: pinImage,
        title: dealer[0],
    });
    
    google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
    });
    
    markers.push( marker );
}

function clearMarkers() {
    infoWindow.close();

    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    
    markers.length = 0;
}

/**
 * @desc    This contains all scripts use in site themes
 */
(function( $ ) {
    'use strict';

    // Load below once the DOM is ready
    $(function() {

        /**
         * Find Dealer
         */
        if( $(".dealer__map") ) {
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((pos) => {
                    let lat = pos.coords.latitude,
                        lng = pos.coords.longitude;
                    
                    // save to COOKIE
                    let cname   = 'user-latlng',
                        cvalue  = lat + ',' + lng,
                        exdays  = 1;
                    
                    var d = new Date();
                    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                    var expires = "expires="+ d.toUTCString();
                    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
                }
                , (error) => {
                    console.log(error);
                }
                , { maximumAge: 10000, timeout: 0, enableHighAccuracy: true });
            }
            else {
                // Browser doesn't support Geolocation
                alert( "Geolocation is not supported by this browser." );
            }
        }
        
        let dealerForm          = document.querySelector('#dealerForm'),
            dealerSearchInput   = document.querySelector('#input_dealer_search'),
            dealerLists         = document.querySelector('#location_list'),
            resultCount         = document.querySelector('.result-count');
        
        if( dealerForm ) {
            dealerForm.addEventListener('submit', event => {
                event.preventDefault();

                let serialize_form  = $("#dealerForm").serializeArray();
                var form_data       = new FormData();
                
                /**
                 * Form security field
                 */
                form_data.append( 'action', 'dealer_search' );
                form_data.append( 'nonce', dealerForm.querySelector('#dealer_form_nonce').value );
                
                /**
                 * Loop through all the fields
                 */
                $.each( serialize_form, function( key, input ) {
                    // console.log( input.name + '=>' + input.value);
                    
                    if( 'dealer_cat' == input.name ) {
                        form_data.append( 'dealer_cat[]', input.value ); 
                    }
                    else {
                        form_data.append( input.name, input.value );
                    }
                });

                // console.log( contents.input_dealer_search );

                if( null == dealerSearchInput.value
                    || undefined == dealerSearchInput.value
                    || "" == dealerSearchInput.value ) {
                    // add "error" class
                    input_dealer_search.classList.add('error');
                }
                else {
                    // update map pens
                    clearMarkers();

                    input_dealer_search.classList.remove('error');
                    
                    dealerLists.innerHTML = `
                        <div class="item">
                            <p>loading dealer list.</p>
                        </div>
                    `;
                    
                    // Convert to $.ajax
                    $.ajax({
                        url: theme_ajax.url,
                        type: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(data) {
                            console.log( data );

                            // update dealer-lists in sidebar area
                            dealerLists.innerHTML = data.dealerHTML;

                            // display search count indicator
                            resultCount.style.display = 'block';
                            resultCount.querySelector('span').innerHTML = data.foundPost;
                            
                            let bounds = new google.maps.LatLngBounds();

                            let dealers = data.dealers;     
                            if( data.foundPost >= 0 ) {
                                for (var i = 0; i < dealers.length; i++) {
                                    let  dealer = dealers[i];
                                    createMarker( dealer );

                                    let latlng = new google.maps.LatLng(
                                        parseFloat(dealer[1]),
                                        parseFloat(dealer[2])
                                    );
                                    bounds.extend( latlng );
                                }

                                map.fitBounds( bounds );
                            }
                        },
                        error: function(e) {
                            // Debugging purposes only
                            console.log(e);
                        },
                        dataType: "json"
                    });
                }
                
            });
        }
        
        let dealerItems = document.querySelectorAll('.dealer__map .location-list .item');
        if( dealerItems ) {
            dealerItems.forEach(elem => {
                elem.addEventListener('click', (event) => {
                    // event.preventDefault();

                    // let target = event.target;
                    // console.log( target );
                    
                    let dealerID = elem.getAttribute('data-id');
                    // console.log( dealerID );
                    
                    google.maps.event.trigger(markers[dealerID], 'click');
                });
            });
        }
        
        /* var dealerMap = $('.dealer__map');
        if( dealerMap ) {
            dealerMap.on('click', '.item', (event) => {
                event.preventDefault();
                
                var dealerID = $(this).attr('data-id');
                console.log(dealerID  );
                
                google.maps.event.trigger(markers[dealerID], 'click');
                
            });
        } */            
    });
    
})(jQuery);