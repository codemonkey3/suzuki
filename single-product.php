<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package rs-theme
 */

get_header();

// get ACF value for later use
$brand_logo             = get_field( 'field_5dee25a33fe6f' );
$price                  = get_field( 'field_5dee25d53fe70' );
$fuel_efficiency        = get_field( 'field_5dee39e13fe71' );

// the trend
$promo_type 	        = get_field( 'field_5ebea50073307' );
?>
	
	<main id="main" class="site-main m-0" role="main">

        <div id="primary" class="content-area">

            <div class="intro-details">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <figure class="ml-0 mb-5 mb-sm-0 text-center text-sm-left">
                                <img src="<?php echo $brand_logo['url'] ?>" alt="<?php echo $brand_logo['alt']; ?>">
                            </figure>
                        </div>
                        <div class="col-md-7">
                            <div class="intro-details__content d-none d-md-block">
                                <ul class="m-0 list-unstyled d-md-flex justify-content-end">
                                    <li class="price">
                                        <p class="mb-0">
                                            Price
                                            <span class="amount d-block">PHP <?php echo number_format($price, 2); ?></span>
                                        </p>
                                    </li>
                                    <li class="fuel-efficiency">
                                        <p class="mb-0">
                                            Fuel Efficiency
                                            <span class="consumption d-block"><?php echo $fuel_efficiency; ?></span>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="view360">
                
                <?php
                /**
                 * Include view360-content
                 */
                get_template_part('template-parts/single-product/content', 'view360'); ?>
                
            </div>

            <div class="product-specifications">
                
                <?php
                /**
                 * Include specifications-content
                 */
                get_template_part('template-parts/single-product/content', 'specifications'); ?>

            </div>

            <?php if( have_rows('field_5dfb031ce92a1') ) : ?>
                <div class="video-review">
                    
                    <?php
                    /**
                     * Include the-trend
                     */
                    get_template_part('template-parts/single-product/content', 'video-review'); ?>
                    
                </div>
            <?php endif; ?>

            <?php if( $promo_type ) : ?>
                <div class="set-the-trend-wrap">
                    
                    <?php
                    /**
                     * Include the-trend
                     */
                    get_template_part('template-parts/single-product/content', 'the-trend'); ?>
                    
                </div>
            <?php endif; ?>

            <div class="related-product">
                
                <?php
                /**
                 * Include related products-content
                 */
                get_template_part('template-parts/single-product/content', 'related-products'); ?>

            </div>

            
        </div><!-- #primary -->

	</main><!-- #main -->

<?php
get_footer();