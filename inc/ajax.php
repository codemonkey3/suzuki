<?php 
/**
 * Ajax requests
 */

add_action( 'wp_ajax_nopriv_dealer_search', 'dealer_search_callback' );
add_action( 'wp_ajax_dealer_search', 'dealer_search_callback' );
/*
 *	@desc	Process theme contact
 */
function dealer_search_callback() {
    ob_start();

    $json   = [];
    $nonce 	= $_POST['nonce'];
    
    if ( ! wp_verify_nonce( $nonce, 'dealer_form_action' ) )
        die ( '<p class="error">Security checked!, Cheatn huh?</p>' );

	$input_dealer_search   = $_POST['input_dealer_search'];
    $dealer_cat            = $_POST['dealer_cat'];
    
    // Query arguments
    $args_s = [
        'post_type'       => 'dealer',
        's'               => $input_dealer_search,
        'posts_per_page'  => -1,
        'fields'          => 'ids'
    ];

    $query_s = new WP_Query( $args_s );

    if ( $query_s->have_posts() ) {
        $query_s_ids = $query_s->posts;
    }
    else {
        $query_s_ids = [];
    }

    $args_meta = [
        'post_type'       => 'dealer',
        'meta_query'      => [
            [
                'key'     => 'address',
                'value'   => $input_dealer_search,
                'compare' => 'LIKE'
            ],
        ],
        'posts_per_page'  => -1,
        'fields'          => 'ids'
    ];
    
    if( $dealer_cat ) {
        $args_meta['tax_query'] = [
            [
                'taxonomy' => 'dealer-cat',
                'terms'    => $dealer_cat
            ],
        ];
    }
    
    $query_meta = new WP_Query( $args_meta );
    
    if ( $query_meta->have_posts() ) {
        $query_meta_ids = $query_meta->posts;
    }
    else {
        $query_meta_ids = [];
    }
    
    // Merge ids to create a unique posts
    $dealer_ids = wp_parse_args( $query_s_ids, $query_meta_ids );

    // make sure we have something to work-on
    if( $dealer_ids ) {
        $dealer_ids = $dealer_ids;
    }
    else {
        $dealer_ids = [0];
    }
    
    // Main query
    $args = [
        'post_type'         => 'dealer',
        'post__in'          => $dealer_ids,
        'posts_per_page'    => -1
    ];
    
    // The Query
    $the_query = new WP_Query( $args );
    
    $json['foundPost'] = $the_query->found_posts;
    
    // The Loop
    if ( $the_query->have_posts() ) {
        
        // set our dealer counter
        $dealer_counter = 0;

        while ( $the_query->have_posts() ) { $the_query->the_post();
            $lat            = get_field('field_5e0b4cb79a038');
            $lng            = get_field('field_5e0b4cce9a039');
            $address        = get_field('field_5e0b86315a51b');
            $tel            = get_field('field_5e0b1827f613f');
            $email          = get_field('field_5e0b183cf6140');
            $website        = get_field('field_5ed90a4df9a48');
            $opening_hours  = get_field('field_5e0b1855f6142');
            
            // user save lat and lng
            $user_latlng    = explode( ',', $_COOKIE['user-latlng'] );
            $distance_meter = haversine_distance( $lat, $lng, $user_latlng[0], $user_latlng[1] );
            $distance       = number_format( ( $distance_meter / 1000 ), 2 );
            
            // dealer info for marker
            $json['dealers'][] = [
                get_the_title(),
                $lat,
                $lng,
                $address,
                $opening_hours,
            ];
            ?>
                <div class="item" data-id="<?php echo $dealer_counter; ?>">
                    <header>
                        <h2 class="dealer-name"><?php the_title(); ?></h2>
                    </header>
                    <div class="services d-flex align-items-center justify-content-between">
                        <div class="tags">
                            <?php
                                // check if the repeater field has rows of data
                                if( have_rows('field_5e0b17f0f613d') ):

                                    // loop through the rows of data
                                    while ( have_rows('field_5e0b17f0f613d') ) : the_row();
                                    
                                        // display a sub field value
                                        $tag_name = get_sub_field('field_5e0b17fbf613e');

                                        ?>
                                            <span><?php echo $tag_name; ?></span>
                                        <?php
                                    endwhile;

                                else :

                                    // no rows found

                                endif;
                            ?>
                        </div>
                        <div class="distance">
                            <span>
                                <?php
                                    if( $user_latlng ) {
                                        echo $distance . 'KM Away';
                                    }
                                    else {
                                        echo '0KM Away';
                                    }
                                ?>
                            </span>
                        </div>
                    </div>
                    <div class="address">
                        <?php echo wpautop( $address ); ?>
                    </div>
                    <div class="dealer-info">
                        <p>
                            <?php if( $tel ) { ?>
                                <span>Tel:</span> <?php echo $tel; ?><br/>
                            <?php } ?>
                            <?php if( $email ) { ?>
                                <span>Email:</span> <?php echo $email; ?>
                            <?php } ?>
                            <?php if( $website ) { ?>
                                <span>Website:</span> <a href="<?php echo esc_url($website); ?>"><?php echo $website; ?></a>
                            <?php } ?>
                        </p>
                    </div>
                    <div class="opening-hours">
                        <h3>Opening Hours</h3>
                        <?php echo $opening_hours ? wpautop( $opening_hours ) : ''; ?>
                    </div>
                    
                    <div class="directions">
                        <p><a href="https://www.google.com/maps/dir/<?php echo $_COOKIE['user-latlng']; ?>/<?php echo $address; ?>" target="_blank">Get Directions</a></p>
                    </div>
                </div>
            <?php

            // increment counter
            $dealer_counter++;
        }
        
    }
    else {
        ?>
            <div class="item" data-id="">
                <p>Oops ;( No dealer found in your search term.</p>
            </div>
        <?php
    }
    
    /* Restore original Post Data */
    wp_reset_postdata();
    
    $json['dealerHTML'] = ob_get_clean();
    wp_send_json( $json );
    
    // return proper result
	die();
}

add_action( 'wp_ajax_nopriv_motorcycle_filter', 'motorcycle_filter_callback' );
add_action( 'wp_ajax_motorcycle_filter', 'motorcycle_filter_callback' );
/*
 *	@desc	Process theme contact
 */
function motorcycle_filter_callback() {
    ob_start();

	$category   = $_POST['category'];
    $price      = $_POST['price'];

    // create from and to array
    $prices     = explode('-', $price);
    
    if( $category && "" == $price ) {

        $cat_id   = (int) $category;
        $category = get_term( $cat_id, 'product-cat' );
        
        if ( ! $category || is_wp_error( $category ) ) {
            return;
        }
        
        $args = [
            'post_type' => 'product',
            'tax_query' => [
                [
                    'taxonomy'  => 'product-cat',
                    'field'     => 'term_id',
                    'terms'     => $category
                ]
            ],
            'orderby'   => 'menu_order title',
            'order'     => 'ASC',
        ];

        // The Query
        $the_query = new WP_Query( $args );

        // The Loop
        if ( $the_query->have_posts() ) {

            // set backbone type for later use
            $backbone_type = $backbone_item_class = '';
            ?>

            <div class="motorcycles__cat cat-<?php echo $cat_id; ?>">

                <div class="text-center">
                    <header>
                        <h2><?php echo $category->name; ?></h2>
                    </header>

                    <div class="cat-description">
                        <div class="row justify-content-center">
                            <div class="col-md-5">
                                <?php echo wpautop( $category->description ); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php
                // if the current cateogry is backbone show the filter
                if( 7 == $cat_id ) : ?>
                    <div class="backbone__filter active">
                        <ul class="ml-0 list-unstyled d-flex justify-content-center">
                            <li><label class="d-flex align-items-center"><input type="radio" name="filter" data-filter="" checked /> <span>All Backbone</span></label></li>
                            <li><label class="d-flex align-items-center"><input type="radio" name="filter" data-filter=".solo" /> <span>Solo</span></label></li>
                            <li><label class="d-flex align-items-center"><input type="radio" name="filter" data-filter=".tricycle" /> <span>Tricycle</span></label></li>
                        </ul>
                    </div>
                <?php endif; ?>
                
                <div class="motorcycles__items">
                    <div class="row <?php echo ( 7 == $cat_id ) ? 'backcone-grid' : ''; ?> motorcycle-grid">
                    
                        <?php
                            while ( $the_query->have_posts() ) { $the_query->the_post();
                                
                                // 7 == Backbone
                                if( 7 == $cat_id ) :
                                    $backbone_type          = get_field('field_5dfc8fbeb748f');
                                    $backbone_item_class    = 'backbone-item';
                                endif;
                                ?>
                                    
                                    <div class="col-lg-6 col-xl-4 <?php echo $backbone_item_class . ' '. $backbone_type ?? ''; ?> motorcycle-item">
                                        <div class="motorcycles__item">
                                            
                                            <?php
                                            /**
                                             * Include content-grid
                                             */
                                            get_template_part('template-parts/single-product/content', 'grid'); ?>
                                            
                                        </div>
                                    </div>

                                <?php
                            }
                        ?>

                    </div>
                </div>

            </div>
            <?php
            
        }
        else {
            // no posts found
            ?>
            <div class="col-md-12">
                <div class="motorcycles__cat">
                    <div class="text-center">
                        <p class="mb-0"><em>This category does not contain any product ;-(.</em></p>
                    </div>
                </div>
            </div>
            <?php
        }

        /* Restore original Post Data */
        wp_reset_postdata();
    }

    if( $price && "" == $category ) {

        // get terms
        $temp_terms = get_terms( array(
            'taxonomy'      => 'product-cat',
            'hide_empty'    => false,
        ) );
        
        if ( ! empty( $temp_terms ) && ! is_wp_error( $temp_terms ) ) {
            $terms = [];
            
            // loop through the terms
            foreach ( $temp_terms as $temp_term ) {
                $order = get_field( 'order', $temp_term );
                
                $terms[$order] = $temp_term;
            }
            
            // order product-category
            ksort( $terms );
        }
        
        foreach( $terms as $term ) {

            $has_items = false;

            ?>
                <div class="motorcycles__cat cat-<?php echo $term->term_id; ?>">

                    <div class="text-center">
                        <header>
                            <h2><?php echo $term->name; ?></h2>
                        </header>

                        <div class="cat-description">
                            <div class="row justify-content-center">
                                <div class="col-md-5">
                                    <?php echo wpautop( $term->description ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="motorcycles__items">
                        <div class="row <?php echo ( 7 == $term->term_id ) ? 'backcone-grid' : ''; ?> motorcycle-grid">
                            <?php
                                $args = [
                                    'post_type'     => 'product',
                                    'tax_query'     => [
                                        [
                                            'taxonomy'  => 'product-cat',
                                            'field'     => 'term_id',
                                            'terms'     => $term->term_id
                                        ]
                                    ],
                                    'meta_query'    => [
                                        [
                                            'key'     => 'price',
                                            'value'   => $prices,
                                            'type'    => 'numeric',
                                            'compare' => 'BETWEEN',
                                        ]
                                    ],
                                    'orderby'   => 'menu_order title',
                                    'order'     => 'ASC',
                                ];
                    
                                // The Query
                                $the_query = new WP_Query( $args );
                                
                                // The Loop
                                if ( $the_query->have_posts() ) {

                                    // set backbone type for later use
                                    $backbone_type = $backbone_item_class = '';
                                    
                                    while ( $the_query->have_posts() ) { $the_query->the_post();
                                        
                                        // 7 == Backbone
                                        if( 7 == $term->term_id ) :
                                            $backbone_type          = get_field('field_5dfc8fbeb748f');
                                            $backbone_item_class    = 'backbone-item';
                                        endif;
                                        ?>
                                            
                                            <div class="col-lg-6 col-xl-4 <?php echo $backbone_item_class . ' '. $backbone_type ?? ''; ?> motorcycle-item">
                                                <div class="motorcycles__item">
                                                    
                                                    <?php
                                                    /**
                                                     * Include content-grid
                                                     */
                                                    get_template_part('template-parts/single-product/content', 'grid'); ?>
                                        
                                                </div>
                                            </div>

                                        <?php
                                    }
                                    
                                }
                                else {
                                    // no posts found
                                    ?>
                                    <div class="col-md-12">
                                        <div class="motorcycles__cat">
                                            <div class="text-center">
                                                <p class="mb-0"><em>This category does not contain any product ;-(.</em></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }

                                /* Restore original Post Data */
                                wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                </div>
                <?php
        }

    }

    // if both options are selected
    if( $category && $price ) {

        $cat_id   = (int) $category;
        $category = get_term( $cat_id, 'product-cat' );
        
        if ( ! $category || is_wp_error( $category ) ) {
            return;
        }
        
        $args = [
            'post_type' => 'product',
            'tax_query' => [
                [
                    'taxonomy'  => 'product-cat',
                    'field'     => 'term_id',
                    'terms'     => $category
                ]
            ],
            'meta_query'    => [
                [
                    'key'     => 'price',
                    'value'   => $prices,
                    'type'    => 'numeric',
                    'compare' => 'BETWEEN',
                ]
            ],
            'orderby'   => 'menu_order title',
            'order'     => 'ASC',
        ];

        // The Query
        $the_query = new WP_Query( $args );

        // The Loop
        if ( $the_query->have_posts() ) {

            // set backbone type for later use
            $backbone_type = $backbone_item_class = '';
            ?>

            <div class="motorcycles__cat cat-<?php echo $cat_id; ?>">

                <div class="text-center">
                    <header>
                        <h2><?php echo $category->name; ?></h2>
                    </header>

                    <div class="cat-description">
                        <div class="row justify-content-center">
                            <div class="col-md-5">
                                <?php echo wpautop( $category->description ); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                // if the current cateogry is backbone show the filter
                if( 7 == $cat_id ) : ?>
                    <div class="backbone__filter active">
                        <ul class="ml-0 list-unstyled d-flex justify-content-center">
                            <li><label class="d-flex align-items-center"><input type="radio" name="filter" data-filter="" checked /> <span>All Backbone</span></label></li>
                            <li><label class="d-flex align-items-center"><input type="radio" name="filter" data-filter=".solo" /> <span>Solo</span></label></li>
                            <li><label class="d-flex align-items-center"><input type="radio" name="filter" data-filter=".tricycle" /> <span>Tricycle</span></label></li>
                        </ul>
                    </div>
                <?php endif; ?>

                <div class="motorcycles__items">
                    <div class="row <?php echo ( 7 == $cat_id ) ? 'backcone-grid' : ''; ?> motorcycle-grid">
                    
                        <?php
                            while ( $the_query->have_posts() ) { $the_query->the_post();
                                
                                // 7 == Backbone
                                if( 7 == $cat_id ) :
                                    $backbone_type          = get_field('field_5dfc8fbeb748f');
                                    $backbone_item_class    = 'backbone-item';
                                endif;
                                ?>
                                    
                                    <div class="col-lg-6 col-xl-4 <?php echo $backbone_item_class . ' '. $backbone_type ?? ''; ?> motorcycle-item">
                                        <div class="motorcycles__item">
                                            
                                            <?php
                                            /**
                                             * Include content-grid
                                             */
                                            get_template_part('template-parts/single-product/content', 'grid'); ?>
                                
                                        </div>
                                    </div>

                                <?php
                            }
                        ?>

                    </div>
                </div>

            </div>
            <?php
            
        }
        else {
            // no posts found
            ?>
                <div class="col-md-12">
                    <div class="motorcycles__cat">
                        <header>
                            <h2>404 ;-(</h2>
                        </header>
                        <p class="mb-0">No product found, try changing your filter.</p>
                    </div>
                </div>
            <?php
        }

        /* Restore original Post Data */
        wp_reset_postdata();
    }

    echo ob_get_clean();
    
    // return proper result
	die();
}


add_action( 'wp_ajax_nopriv_racing_filter', 'racing_filter_callback' );
add_action( 'wp_ajax_racing_filter', 'racing_filter_callback' );
/*
 *	@desc	Process theme contact
 */
function racing_filter_callback() {
    ob_start();

    $racing_type   = $_POST['racing_type'];
    ?>
        <div class="container">
            <div class="row">
                
                <?php  
                    $paged          = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                    $posts_per_page = 9;
                    
                    $racing_args = [
                        'post_type'             => 'post',
                        'category_name'         => 'racing',
                        'post__not_in'          => [get_latest_post_id()],
                        'posts_per_page'        => $posts_per_page,
                        'paged'                 => $paged
                    ];
                    
                    if( '' == $racing_type ) {
                        // do nothing
                    }
                    else {
                        $racing_args['meta_query'] = [
                            [
                                'key'     => 'type',
                                'value'   => $racing_type,
                            ]
                        ];
                    }
                    
                    // The Query
                    $racing_query = new WP_Query( $racing_args );
                    
                    // The Loop
                    if ( $racing_query->have_posts() ) :
                        
                        while ( $racing_query->have_posts() ) : $racing_query->the_post();
                            
                            ?>
                                <div class="col-lg-6 col-xl-4">
                                    <div class="racing__item news-card">

                                        <a href="<?php the_permalink(); ?>">
                                            <figure class="mb-0">
                                                <?php
                                                    if ( has_post_thumbnail() ) :
                                                        the_post_thumbnail();
                                                    endif;
                                                ?>
                                            </figure>
                                            
                                            <div class="content">
                                                <div class="text-center">
                                                    <div class="date">
                                                        <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
                                                            <?php echo get_the_date('M j, Y'); ?>
                                                        </time>
                                                    </div>

                                                    <header>
                                                        <h2><?php the_title(); ?></h2>
                                                    </header>
                                                    
                                                    <div class="cta">
                                                        <p class="mb-0">
                                                            <span>Read Article</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                </div>
                            <?php

                        endwhile; // End of the loop.
                        
                    else :
                        ?>
                            <div class="col-lg-12">
                                <p>No racing available at the moment.</p>
                            </div>
                        <?php
                    endif;

                    // Restore original Post Data
                    wp_reset_postdata();
                ?>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    
                    <?php 
                        global $wp_rewrite;

                        // custom navigation
                        $big            = 999999999; 		// need an unlikely integer
                        $total_pages    = $racing_query->max_num_pages;
                        
                        // check if we have enough posts
                        if ( $total_pages > 1 ) {
                            $current_page = max( 1, get_query_var('paged') );
                            $nav_args = array(
                                // 'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'base'      => get_permalink(467) . "{$wp_rewrite->pagination_base}/%#%/?filter={$racing_type}",
                                'format' 	=> '/page/%#%',
                                'current' 	=> $current_page,
                                'total'		=> $total_pages,
                                'show_all'	=> false,
                                'prev_text'	=> 'Prev',
                                'next_text'	=> 'Next',
                                'type' 		=> 'list'
                            );
                            
                            echo paginate_links( $nav_args );
                        }
                    ?>

                </div>
            </div>

        </div> <!-- .container -->
    <?php

    echo ob_get_clean();
    
    // return proper result
	die();
}