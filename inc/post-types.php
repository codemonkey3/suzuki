<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package suzuki
 */

/**
 * Register a custom post type called "spare-part".
 *
 * @see get_post_type_labels() for label keys.
 */
function ati_register_post_type_init() {

    $post_types['spare-part'] = [
        'labels'             => [
            'name'                  => _x( 'Spare Parts', 'Post type general name', 'suzuki' ),
            'singular_name'         => _x( 'Spare Part', 'Post type singular name', 'suzuki' ),
            'menu_name'             => _x( 'Spare Parts', 'Admin Menu text', 'suzuki' ),
            'name_admin_bar'        => _x( 'Spare Part', 'Add New on Toolbar', 'suzuki' ),
            'add_new'               => __( 'Add New', 'suzuki' ),
            'add_new_item'          => __( 'Add New Spare Part', 'suzuki' ),
            'new_item'              => __( 'New Spare Part', 'suzuki' ),
            'edit_item'             => __( 'Edit Spare Part', 'suzuki' ),
            'view_item'             => __( 'View Spare Part', 'suzuki' ),
            'all_items'             => __( 'All Spare Parts', 'suzuki' ),
            'search_items'          => __( 'Search Spare Parts', 'suzuki' ),
            'parent_item_colon'     => __( 'Parent Spare Parts:', 'suzuki' ),
            'not_found'             => __( 'No spare parts found.', 'suzuki' ),
            'not_found_in_trash'    => __( 'No spare parts found in Trash.', 'suzuki' ),
            'featured_image'        => _x( 'Spare Part Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'archives'              => _x( 'Spare Part archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'suzuki' ),
            'insert_into_item'      => _x( 'Insert into spare part', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'suzuki' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this spare part', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'suzuki' ),
            'filter_items_list'     => _x( 'Filter spare parts list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'suzuki' ),
            'items_list_navigation' => _x( 'Spare Parts list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'suzuki' ),
            'items_list'            => _x( 'Spare Parts list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'suzuki' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'spare-part' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor' ),
    ];

    $post_types['apparel'] = [
        'labels'             => [
            'name'                  => _x( 'Apparels', 'Post type general name', 'suzuki' ),
            'singular_name'         => _x( 'Apparel', 'Post type singular name', 'suzuki' ),
            'menu_name'             => _x( 'Apparels', 'Admin Menu text', 'suzuki' ),
            'name_admin_bar'        => _x( 'Apparel', 'Add New on Toolbar', 'suzuki' ),
            'add_new'               => __( 'Add New', 'suzuki' ),
            'add_new_item'          => __( 'Add New Apparel', 'suzuki' ),
            'new_item'              => __( 'New Apparel', 'suzuki' ),
            'edit_item'             => __( 'Edit Apparel', 'suzuki' ),
            'view_item'             => __( 'View Apparel', 'suzuki' ),
            'all_items'             => __( 'All Apparels', 'suzuki' ),
            'search_items'          => __( 'Search Apparels', 'suzuki' ),
            'parent_item_colon'     => __( 'Parent Apparels:', 'suzuki' ),
            'not_found'             => __( 'No apparels found.', 'suzuki' ),
            'not_found_in_trash'    => __( 'No apparels found in Trash.', 'suzuki' ),
            'featured_image'        => _x( 'Apparel Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'archives'              => _x( 'Apparels archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'suzuki' ),
            'insert_into_item'      => _x( 'Insert into apparel', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'suzuki' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this apparel', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'suzuki' ),
            'filter_items_list'     => _x( 'Filter apparels list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'suzuki' ),
            'items_list_navigation' => _x( 'Apparels list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'suzuki' ),
            'items_list'            => _x( 'Apparels list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'suzuki' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'apparel' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor' ),
    ];

    $post_types['promo'] = [
        'labels'             => [
            'name'                  => _x( 'Promos', 'Post type general name', 'suzuki' ),
            'singular_name'         => _x( 'Promo', 'Post type singular name', 'suzuki' ),
            'menu_name'             => _x( 'Promos', 'Admin Menu text', 'suzuki' ),
            'name_admin_bar'        => _x( 'Promo', 'Add New on Toolbar', 'suzuki' ),
            'add_new'               => __( 'Add New', 'suzuki' ),
            'add_new_item'          => __( 'Add New Promo', 'suzuki' ),
            'new_item'              => __( 'New Promo', 'suzuki' ),
            'edit_item'             => __( 'Edit Promo', 'suzuki' ),
            'view_item'             => __( 'View Promo', 'suzuki' ),
            'all_items'             => __( 'All Promos', 'suzuki' ),
            'search_items'          => __( 'Search Promos', 'suzuki' ),
            'parent_item_colon'     => __( 'Parent Promos:', 'suzuki' ),
            'not_found'             => __( 'No promo found.', 'suzuki' ),
            'not_found_in_trash'    => __( 'No promos found in Trash.', 'suzuki' ),
            'featured_image'        => _x( 'Promo Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'archives'              => _x( 'Promos archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'suzuki' ),
            'insert_into_item'      => _x( 'Insert into promo', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'suzuki' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this promo', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'suzuki' ),
            'filter_items_list'     => _x( 'Filter promos list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'suzuki' ),
            'items_list_navigation' => _x( 'Promos list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'suzuki' ),
            'items_list'            => _x( 'Promos list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'suzuki' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'promo' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail' ),
    ];

    $post_types['product'] = [
        'labels'             => [
            'name'                  => _x( 'Products', 'Post type general name', 'suzuki' ),
            'singular_name'         => _x( 'Product', 'Post type singular name', 'suzuki' ),
            'menu_name'             => _x( 'Products', 'Admin Menu text', 'suzuki' ),
            'name_admin_bar'        => _x( 'Product', 'Add New on Toolbar', 'suzuki' ),
            'add_new'               => __( 'Add New', 'suzuki' ),
            'add_new_item'          => __( 'Add New Product', 'suzuki' ),
            'new_item'              => __( 'New Product', 'suzuki' ),
            'edit_item'             => __( 'Edit Product', 'suzuki' ),
            'view_item'             => __( 'View Product', 'suzuki' ),
            'all_items'             => __( 'All Products', 'suzuki' ),
            'search_items'          => __( 'Search Products', 'suzuki' ),
            'parent_item_colon'     => __( 'Parent Products:', 'suzuki' ),
            'not_found'             => __( 'No product found.', 'suzuki' ),
            'not_found_in_trash'    => __( 'No products found in Trash.', 'suzuki' ),
            'featured_image'        => _x( 'Product Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'archives'              => _x( 'Products archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'suzuki' ),
            'insert_into_item'      => _x( 'Insert into product', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'suzuki' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this product', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'suzuki' ),
            'filter_items_list'     => _x( 'Filter products list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'suzuki' ),
            'items_list_navigation' => _x( 'Products list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'suzuki' ),
            'items_list'            => _x( 'Products list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'suzuki' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'product' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'page-attributes' ),
    ];

    $post_types['dealer'] = [
        'labels'             => [
            'name'                  => _x( 'Dealers', 'Post type general name', 'suzuki' ),
            'singular_name'         => _x( 'Dealer', 'Post type singular name', 'suzuki' ),
            'menu_name'             => _x( 'Dealers', 'Admin Menu text', 'suzuki' ),
            'name_admin_bar'        => _x( 'Dealer', 'Add New on Toolbar', 'suzuki' ),
            'add_new'               => __( 'Add New', 'suzuki' ),
            'add_new_item'          => __( 'Add New Dealer', 'suzuki' ),
            'new_item'              => __( 'New Dealer', 'suzuki' ),
            'edit_item'             => __( 'Edit Dealer', 'suzuki' ),
            'view_item'             => __( 'View Dealer', 'suzuki' ),
            'all_items'             => __( 'All Dealers', 'suzuki' ),
            'search_items'          => __( 'Search Dealers', 'suzuki' ),
            'parent_item_colon'     => __( 'Parent Dealers:', 'suzuki' ),
            'not_found'             => __( 'No dealer found.', 'suzuki' ),
            'not_found_in_trash'    => __( 'No dealers found in Trash.', 'suzuki' ),
            'featured_image'        => _x( 'Dealer Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'archives'              => _x( 'Dealers archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'suzuki' ),
            'insert_into_item'      => _x( 'Insert into dealer', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'suzuki' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this dealer', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'suzuki' ),
            'filter_items_list'     => _x( 'Filter dealers list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'suzuki' ),
            'items_list_navigation' => _x( 'Dealers list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'suzuki' ),
            'items_list'            => _x( 'Dealers list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'suzuki' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'dealer' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'page-attributes' ),
    ];

    $post_types['club'] = [
        'labels'             => [
            'name'                  => _x( 'Clubs', 'Post type general name', 'suzuki' ),
            'singular_name'         => _x( 'Club', 'Post type singular name', 'suzuki' ),
            'menu_name'             => _x( 'Clubs', 'Admin Menu text', 'suzuki' ),
            'name_admin_bar'        => _x( 'Club', 'Add New on Toolbar', 'suzuki' ),
            'add_new'               => __( 'Add New', 'suzuki' ),
            'add_new_item'          => __( 'Add New Club', 'suzuki' ),
            'new_item'              => __( 'New Club', 'suzuki' ),
            'edit_item'             => __( 'Edit Club', 'suzuki' ),
            'view_item'             => __( 'View Club', 'suzuki' ),
            'all_items'             => __( 'All Clubs', 'suzuki' ),
            'search_items'          => __( 'Search Clubs', 'suzuki' ),
            'parent_item_colon'     => __( 'Parent Clubs:', 'suzuki' ),
            'not_found'             => __( 'No club found.', 'suzuki' ),
            'not_found_in_trash'    => __( 'No clubs found in Trash.', 'suzuki' ),
            'featured_image'        => _x( 'Club Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'archives'              => _x( 'Clubs archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'suzuki' ),
            'insert_into_item'      => _x( 'Insert into club', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'suzuki' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this club', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'suzuki' ),
            'filter_items_list'     => _x( 'Filter clubs list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'suzuki' ),
            'items_list_navigation' => _x( 'Clubs list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'suzuki' ),
            'items_list'            => _x( 'Clubs list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'suzuki' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'club' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'page-attributes' ),
    ];

    $post_types['wallpaper'] = [
        'labels'             => [
            'name'                  => _x( 'Wallpapers', 'Post type general name', 'suzuki' ),
            'singular_name'         => _x( 'Wallpaper', 'Post type singular name', 'suzuki' ),
            'menu_name'             => _x( 'Wallpapers', 'Admin Menu text', 'suzuki' ),
            'name_admin_bar'        => _x( 'Wallpaper', 'Add New on Toolbar', 'suzuki' ),
            'add_new'               => __( 'Add New', 'suzuki' ),
            'add_new_item'          => __( 'Add New Wallpaper', 'suzuki' ),
            'new_item'              => __( 'New Wallpaper', 'suzuki' ),
            'edit_item'             => __( 'Edit Wallpaper', 'suzuki' ),
            'view_item'             => __( 'View Wallpaper', 'suzuki' ),
            'all_items'             => __( 'All Wallpaper', 'suzuki' ),
            'search_items'          => __( 'Search Wallpaper', 'suzuki' ),
            'parent_item_colon'     => __( 'Parent Wallpaper:', 'suzuki' ),
            'not_found'             => __( 'No wallpaper found.', 'suzuki' ),
            'not_found_in_trash'    => __( 'No wallpaper found in Trash.', 'suzuki' ),
            'featured_image'        => _x( 'Wallpaper Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'archives'              => _x( 'Wallpaper archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'suzuki' ),
            'insert_into_item'      => _x( 'Insert into wallpaper', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'suzuki' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this wallpaper', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'suzuki' ),
            'filter_items_list'     => _x( 'Filter wallpaper list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'suzuki' ),
            'items_list_navigation' => _x( 'Wallpaper list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'suzuki' ),
            'items_list'            => _x( 'Wallpaper list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'suzuki' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        // 'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'wallpaper' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'page-attributes' ),
    ];

    $post_types['jingle'] = [
        'labels'             => [
            'name'                  => _x( 'Jingles', 'Post type general name', 'suzuki' ),
            'singular_name'         => _x( 'Jingle', 'Post type singular name', 'suzuki' ),
            'menu_name'             => _x( 'Jingles', 'Admin Menu text', 'suzuki' ),
            'name_admin_bar'        => _x( 'Jingle', 'Add New on Toolbar', 'suzuki' ),
            'add_new'               => __( 'Add New', 'suzuki' ),
            'add_new_item'          => __( 'Add New Jingle', 'suzuki' ),
            'new_item'              => __( 'New Jingle', 'suzuki' ),
            'edit_item'             => __( 'Edit Jingle', 'suzuki' ),
            'view_item'             => __( 'View Jingle', 'suzuki' ),
            'all_items'             => __( 'All Jingles', 'suzuki' ),
            'search_items'          => __( 'Search Jingles', 'suzuki' ),
            'parent_item_colon'     => __( 'Parent Jingle:', 'suzuki' ),
            'not_found'             => __( 'No jingles found.', 'suzuki' ),
            'not_found_in_trash'    => __( 'No jingles found in Trash.', 'suzuki' ),
            'featured_image'        => _x( 'Jingle Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'suzuki' ),
            'archives'              => _x( 'Jingles archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'suzuki' ),
            'insert_into_item'      => _x( 'Insert into jingle', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'suzuki' ),
            'uploaded_to_this_item' => _x( 'Uploaded to this jingle', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'suzuki' ),
            'filter_items_list'     => _x( 'Filter jingles list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'suzuki' ),
            'items_list_navigation' => _x( 'Jingle list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'suzuki' ),
            'items_list'            => _x( 'Jingle list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'suzuki' ),
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        // 'show_in_rest'       => true,
        'rewrite'            => array( 'slug' => 'jingle' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor' ),
    ];

    if( $post_types ) {
        // loop through all post-types
        foreach( $post_types as $post_type => $args ) {
            register_post_type( $post_type, $args );
        }
    }
    
    /**
     * Register a 'product-cat' taxonomy for post type 'product', with a rewrite to match product CPT slug.
     *
     * @see register_post_type for registering post types.
     */
    $taxonomies['product'] = [
        'term'      => 'product-cat',
        'post-type' => 'product',
        'taxonomy'  => [
            'labels'        => [
                'name'              => _x( 'Categories', 'taxonomy general name', 'suzuki' ),
                'singular_name'     => _x( 'Category', 'taxonomy singular name', 'suzuki' ),
                'search_items'      => __( 'Search Categories', 'suzuki' ),
                'all_items'         => __( 'All Categories', 'suzuki' ),
                'parent_item'       => __( 'Parent Category', 'suzuki' ),
                'parent_item_colon' => __( 'Parent Category:', 'suzuki' ),
                'edit_item'         => __( 'Edit Category', 'suzuki' ),
                'update_item'       => __( 'Update Category', 'suzuki' ),
                'add_new_item'      => __( 'Add New Category', 'suzuki' ),
                'new_item_name'     => __( 'New Category Name', 'suzuki' ),
                'menu_name'         => __( 'Categories', 'suzuki' ),
            ],
            'hierarchical'      => true,
            'public'            => true,
            'show_in_rest'      => true,
            'show_admin_column' => true,
            'rewrite'           => [ 'slug'  => 'product-cat' ]
        ]
    ];
    
    /**
     * Register a 'dealer-cat' taxonomy for post type 'dealer', with a rewrite to match dealer CPT slug.
     *
     * @see register_post_type for registering post types.
     */
    $taxonomies['dealer'] = [
        'term'      => 'dealer-cat',
        'post-type' => 'dealer',
        'taxonomy'  => [
            'labels'        => [
                'name'              => _x( 'Categories', 'taxonomy general name', 'suzuki' ),
                'singular_name'     => _x( 'Category', 'taxonomy singular name', 'suzuki' ),
                'search_items'      => __( 'Search Categories', 'suzuki' ),
                'all_items'         => __( 'All Categories', 'suzuki' ),
                'parent_item'       => __( 'Parent Category', 'suzuki' ),
                'parent_item_colon' => __( 'Parent Category:', 'suzuki' ),
                'edit_item'         => __( 'Edit Category', 'suzuki' ),
                'update_item'       => __( 'Update Category', 'suzuki' ),
                'add_new_item'      => __( 'Add New Category', 'suzuki' ),
                'new_item_name'     => __( 'New Category Name', 'suzuki' ),
                'menu_name'         => __( 'Categories', 'suzuki' ),
            ],
            'hierarchical'      => true,
            'public'            => true,
            'show_in_rest'      => true,
            'show_admin_column' => true,
            'rewrite'           => [ 'slug'  => 'dealer-cat' ]
        ]
    ];

    // jingle taxonomy
    $taxonomies['jingle'] = [
        'term'      => 'jingle-cat',
        'post-type' => 'jingle',
        'taxonomy'  => [
            'labels'        => [
                'name'              => _x( 'Categories', 'taxonomy general name', 'suzuki' ),
                'singular_name'     => _x( 'Category', 'taxonomy singular name', 'suzuki' ),
                'search_items'      => __( 'Search Categories', 'suzuki' ),
                'all_items'         => __( 'All Categories', 'suzuki' ),
                'parent_item'       => __( 'Parent Category', 'suzuki' ),
                'parent_item_colon' => __( 'Parent Category:', 'suzuki' ),
                'edit_item'         => __( 'Edit Category', 'suzuki' ),
                'update_item'       => __( 'Update Category', 'suzuki' ),
                'add_new_item'      => __( 'Add New Category', 'suzuki' ),
                'new_item_name'     => __( 'New Category Name', 'suzuki' ),
                'menu_name'         => __( 'Categories', 'suzuki' ),
            ],
            'hierarchical'      => true,
            'public'            => true,
            'show_in_rest'      => true,
            'show_admin_column' => true,
            'rewrite'           => [ 'slug'  => 'jingle-cat' ]
        ]
    ];

    // wallpaper taxonomy
    $taxonomies['wallpaper'] = [
        'term'      => 'wallpaper-cat',
        'post-type' => 'wallpaper',
        'taxonomy'  => [
            'labels'        => [
                'name'              => _x( 'Categories', 'taxonomy general name', 'suzuki' ),
                'singular_name'     => _x( 'Category', 'taxonomy singular name', 'suzuki' ),
                'search_items'      => __( 'Search Categories', 'suzuki' ),
                'all_items'         => __( 'All Categories', 'suzuki' ),
                'parent_item'       => __( 'Parent Category', 'suzuki' ),
                'parent_item_colon' => __( 'Parent Category:', 'suzuki' ),
                'edit_item'         => __( 'Edit Category', 'suzuki' ),
                'update_item'       => __( 'Update Category', 'suzuki' ),
                'add_new_item'      => __( 'Add New Category', 'suzuki' ),
                'new_item_name'     => __( 'New Category Name', 'suzuki' ),
                'menu_name'         => __( 'Categories', 'suzuki' ),
            ],
            'hierarchical'      => true,
            'public'            => true,
            'show_in_rest'      => true,
            'show_admin_column' => true,
            'rewrite'           => [ 'slug'  => 'wallpaper-cat' ]
        ]
    ];

    // loop through taxonomies
    if( $taxonomies ) {
        foreach( $taxonomies as $post_type => $taxonomy ) {
            register_taxonomy(
                $taxonomy['term'],
                $taxonomy['post-type'],
                $taxonomy['taxonomy']
            );
        }
    }
}

add_action( 'init', 'ati_register_post_type_init' );