<?php
global $wpdb;

// Don't load theme support functionality
define( 'WP_USE_THEMES', false );
require( '/var/www/public/suzuki/wp-load.php' );

// Get parameters from URL
$center_lat     = $_GET["lat"];
$center_lng     = $_GET["lng"];
$dealer_name    = $_GET["dealer_name"];
$dealer_cat     = $_GET['dealer_cat'];

// Start XML file, create parent node
$dom        = new DOMDocument("1.0");
$node       = $dom->createElement("markers");
$parnode    = $dom->appendChild( $node );

$dealers = $wpdb->get_results( $wpdb->prepare(
    "SELECT *, ( 3959 * ACOS( COS( RADIANS('%s') )
            * COS( RADIANS( lat ) )
            * COS( RADIANS( lng )
                - RADIANS('%s') )
            + SIN( RADIANS('%s' ) )
            * SIN( RADIANS( lat ) )
        ) ) AS distance
    FROM
    (SELECT ID,
        post_title,
        post_content,
        MAX(CASE WHEN meta_key='coordinates_lat' THEN meta_value END) lat,
        MAX(CASE WHEN meta_key='coordinates_long' THEN meta_value END) lng
    FROM `wp_posts`
        LEFT JOIN `wp_postmeta` meta
            ON ID = `post_id`
        LEFT JOIN `wp_term_relationships` term_rel
        	ON term_rel.`object_id` = `post_id`
    WHERE post_type = 'dealer'
        AND (meta_key='coordinates_lat' OR meta_key='coordinates_long')
    GROUP BY ID, post_title, term_rel.`term_taxonomy_id`
    ORDER BY ID ASC) AS A",
    $center_lat, $center_lng, $center_lat
) );

if( $dealers ) {
    /* echo '<pre>';
        print_r( $dealers );
    echo '</pre>'; */
    
    foreach( $dealers as $dealer ) {
        $post_id        = $dealer->ID;
        $dealer_name    = $dealer->post_title;
        $lat            = $dealer->lat;
        $lng            = $dealer->lng;
        $distance       = $dealer->distance;
        
        // get custom post-meta
        $address        = get_field('field_5e0b86315a51b', $post_id);
        $tel            = get_field('field_5e0b1827f613f', $post_id);
        $email          = get_field('field_5e0b183cf6140', $post_id);
        $opening_hour   = get_field('field_5e0b1855f6142', $post_id);
        
        // check if the repeater field has rows of data
        if( have_rows('field_5e0b17f0f613d', $post_id) ):

            // store tags to into array
            $tags = [];

            // loop through the rows of data
            while ( have_rows('field_5e0b17f0f613d', $post_id) ) : the_row();

                // display a sub field value
                $tags[] = get_sub_field('field_5e0b17fbf613e');
                
            endwhile;

        else :
            
            // no rows found

        endif;

        // convert tags to string
        $tags_html      = implode( '|', $tags );
        
        $node           = $dom->createElement("marker");
        $newnode        = $parnode->appendChild($node);
        
        $newnode->setAttribute("id", $post_id);
        $newnode->setAttribute("name", $dealer_name);
        $newnode->setAttribute("tags", $tags_html);
        $newnode->setAttribute("address", $address);
        $newnode->setAttribute("tel", $tel);
        $newnode->setAttribute("email", $email);
        $newnode->setAttribute("opening_hour", $opening_hour);
        $newnode->setAttribute("distance", $distance);
        $newnode->setAttribute("lat", $lat);
        $newnode->setAttribute("lng", $lng);
    }
}

echo $dom->saveXML();