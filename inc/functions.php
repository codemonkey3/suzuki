<?php

/**
 * Generate main navigation
 */
function generate_main_nav_menu() {

    $locations = get_nav_menu_locations();

    if( $locations && isset( $locations['primary'] ) ) {

        $menu       = get_term( $locations['primary'], 'nav_menu' );
        $menu_items = wp_get_nav_menu_items( $menu->term_id );

        ?>
        <ul id="primary-menu" class="primary-menu">
            <?php
                foreach( $menu_items as $menu_item ) {
                    if( $menu_item->menu_item_parent == 0 ) {

                        $menu_id = $menu_item->ID;

                        ?>
                        <li id="menu-item-<?php echo $menu_item->ID; ?>" class="menu-item menu-item-<?php echo $menu_item->ID . ' ' . implode($menu_item->classes); ?>">
                            <a href="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></a>

                            <?php if( 377 == $menu_id ) { ?>
                                <div class="motorcycle megamenu-item">
                                    <?php get_template_part('template-parts/menu/content', 'motorcycle'); ?>
                                </div> <!-- .motorcycle -->
                            <?php } ?>
                        </li>
                        <?php
                    }
                }
            ?>
        </ul>
        <?php
    }
    
}

function _get_post_ids( $post_type = 'post', $category = 'category', $order = 'DESC' ) {
    
    // make sure the post-type exist
    if( ! post_type_exists( $post_type ) ) 
        return;
    
    $args = [
        'post_type'         => $post_type,
        'numberposts'       => 1,
        'category_name'     => $category,
        'orderby'          => 'date',
        'order'             => $order,
        'fields'            => 'ids' // returned only the ID
    ];

    // get post_ids
    $post_ids = get_posts( $args )[0];

    // rest postdata
    wp_reset_postdata();
    
    return $post_ids;
}

/**
 * Get latest article post-type ID
 */
function get_latest_post_id() {
    return _get_post_ids( 'post', 'racing' );
}

/**
 * Get distance using Haversine formula
 */
function haversine_distance( $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000 ) {
    // convert from degrees to radians
    $latFrom    = deg2rad( $latitudeFrom );
    $lonFrom    = deg2rad( $longitudeFrom );
    $latTo      = deg2rad( $latitudeTo );
    $lonTo      = deg2rad( $longitudeTo );
    
    $latDelta   = $latTo - $latFrom;
    $lonDelta   = $lonTo - $lonFrom;
  
    $angle      = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    
    return $angle * $earthRadius;
}