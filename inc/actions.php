<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package suzuki
 */

/*
 * Remove default WP builtin actions
 */
remove_action( 'wp_head', 'wp_generator' ); 

/*
 * @desc Localize wp-ajax
 */
function rs_theme_init() {
	wp_enqueue_script( 'suzuki-request-script', get_template_directory_uri() . '/js/ajax.js', array( 'jquery' ) );
	wp_localize_script( 'suzuki-request-script', 'theme_ajax', array(
		'url'		=> admin_url( 'admin-ajax.php' ),
		'site_url' 	=> get_bloginfo('url'),
		'theme_url' => get_bloginfo('template_directory')
	) );
	
	/**
	 * Generate dealer
	*/
	$dealer_infos   = [];
	$dealer_args           = [
		'post_type'         => 'dealer',
		'posts_per_page'    => -1
	];

	$dealer_query = new WP_Query( $dealer_args );
	
	if ( $dealer_query->have_posts() ) {
		while ( $dealer_query->have_posts() ) { $dealer_query->the_post();
			$lat            = get_field('field_5e0b4cb79a038');
			$lng            = get_field('field_5e0b4cce9a039');
			$address        = get_field('field_5e0b86315a51b');
			$tel            = get_field('field_5e0b1827f613f');
			$email          = get_field('field_5e0b183cf6140');
			$website        = get_field('field_5ed90a4df9a48');
			$opening_hours  = get_field('field_5e0b1855f6142');

			// user save lat and lng
			$user_latlng    = explode( ',', $_COOKIE['user-latlng'] );
			$distance_meter = haversine_distance( $lat, $lng, $user_latlng[0], $user_latlng[1] );
			$distance       = number_format( ( $distance_meter / 1000 ), 2 );

			// dealer info for marker
			$dealer_infos[] = [
				get_the_title(),
				$lat,
				$lng,
				$address,
				$opening_hours,
			];
		}
	}
	else {
		// no posts found
	}

	/* Restore original Post Data */
	wp_reset_postdata();
	
	wp_enqueue_script( 'suzuki-dealer-script', get_template_directory_uri() . '/js/dealer.js', array( 'jquery' ) );
	wp_localize_script( 'suzuki-dealer-script', 'suzuki', array(
		'dealer_infos' => $dealer_infos,
	) );
}
add_action( 'init', 'rs_theme_init' );


/**********************************
 * Help site speed
 **********************************/
if ( ! is_admin() ) {
  add_action( "wp_enqueue_scripts", "rs_theme_jquery_enqueue", 10 );
}

function rs_theme_jquery_enqueue() {
	wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', true );

	if( is_front_page()
	|| is_single()
	|| is_page_template('page-templates/event.php') ) {
		// slick js
		wp_enqueue_script( 'slick-scripst-js', get_template_directory_uri() . '/js/slick.min.js', false, '1.8.1', true );
	}

	if( is_page_template('page-templates/motorcycle.php') ) {
		// isotope
		wp_enqueue_script( 'isotope-scripst-js', get_template_directory_uri() . '/js/isotope.pkgd.min.js', false, '3.0.6', true );
	}
	
	if( 'product' == get_post_type() ) {
		wp_enqueue_script( 'suzuki-createjs-js', get_template_directory_uri() . '/js/360/createjs.min.js', false, '1.8.1', true );
		wp_enqueue_script( 'suzuki-easeljs-js', get_template_directory_uri() . '/js/360/easeljs-0.6.0.min.js', false, '1.8.1', true );
		wp_enqueue_script( 'suzuki-360-viewer-js', get_template_directory_uri() . '/js/360/360-deg-viewer.js', false, '1.8.1', true );
	}

	if( is_front_page()
	|| is_page_template('page-templates/news.php')
	|| is_page_template('page-templates/racing.php')
	|| is_page_template('page-templates/promo.php')
	|| is_page_template('page-templates/spare-parts.php')
	|| is_page_template('page-templates/apparel.php')
	|| is_single() ) {
		wp_enqueue_script( 'suzuki-matchheight-js', get_template_directory_uri() . '/js/jquery.matchHeight-min.js', array('jquery'), '0.7.2', true );
	}

	wp_enqueue_style( 'suzuki-style', get_stylesheet_uri() );
	wp_enqueue_script( 'suzuki-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0', true );

	// load the google-apis
	if( is_front_page()
	|| is_page_template('page-templates/dealer.php') ) {
		// AIzaSyDCepyz8QbqtwxrhsALxZH_-4iBtj-FPMs
		// AIzaSyCHCrVa5dNF75HuJ_sDCXR4EK7sC3VdSi4
		
		// get Google API from Theme Settings
		$google_API = get_field('field_5ecf983a2c41d', 'option');
	
		wp_enqueue_script( 'suzuki-googleapis-scripst-js', 'https://maps.googleapis.com/maps/api/js?language=en&region=ph&key='. $google_API .'&libraries=places&callback=initMap', false, '1.0', true );
	}
}

// Remove wp version param from any enqueued scripts
function rs_theme_remove_wp_ver( $src ) {
   	if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
    	$src = remove_query_arg( 'ver', $src );
   	return $src;
}
add_filter( 'style_loader_src', 'rs_theme_remove_wp_ver', 9999 );
add_filter( 'script_loader_src', 'rs_theme_remove_wp_ver', 9999 );

function rs_theme_custom_footer() {
	ob_start();
	?>
		<script type="text/javascript">
			function downloadJSAtOnload() {
				var element = document.createElement("script");
				element.src = "<?php echo get_stylesheet_directory_uri(); ?>/js/defer.js";
				document.body.appendChild(element);
			}
			
			if (window.addEventListener)
				window.addEventListener("load", downloadJSAtOnload, false);
			else if (window.attachEvent)
				window.attachEvent("onload", downloadJSAtOnload);
			else window.onload = downloadJSAtOnload;
		</script>
	<?php
	echo ob_get_clean();
}
add_action( 'wp_footer', 'rs_theme_custom_footer' );

/**********************************
 * Customize WP Login
 **********************************/
function rs_theme_login_logo() 
{
	$options = get_option( 'rs_theme_theme_options' );
	?>
	<style type="text/css">
		.login h1 a {
			background: url('<?php echo esc_url( $options['logo'] ) ?>') top center no-repeat;
		}
	</style>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,500" rel="stylesheet">
	<?php 
}
add_action( 'login_head', 'rs_theme_login_logo' );

function rs_login_stylesheet() {
    wp_enqueue_style( 'rstheme-custom-login-css', get_stylesheet_directory_uri() . '/css/login-form.css' );
    wp_enqueue_script( 'rstheme-custom-login-js', get_stylesheet_directory_uri() . '/js/login-form.js' );
}
add_action( 'login_enqueue_scripts', 'rs_login_stylesheet' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}