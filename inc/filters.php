<?php
/**
 * @desc	If you have something to add in add_filter function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package rs-theme
 */

/*
 * Enable shortcode in Menu and Sidebar
 */
add_filter( 'wp_nav_menu', 'do_shortcode' );
add_filter( 'widget_text', 'do_shortcode' );
add_filter( 'show_admin_bar', '__return_false' );

/*
 * Replace WP Login header URL
 */
function rs_theme_headerurl( $url ) {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'rs_theme_headerurl' );

function rs_theme_login_headertext() {
	return get_bloginfo('name');
}
add_filter( 'login_headertext', 'rs_theme_login_headertext' );

/**
 * Add data-rel link attribute to attachment media or link
 */
function rs_theme_add_rel_attribute( $link ) {
	return str_replace( '<a href', '<a data-rel="fancybox" href', $link );
}
add_filter( 'wp_get_attachment_link', 'rs_theme_add_rel_attribute' );


/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function rs_theme_custom_excerpt_length( $length ) {
    return 18;
}
add_filter( 'excerpt_length', 'rs_theme_custom_excerpt_length', 9999 );


/**
 * Filter the "read more" on auto excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function rs_theme_excerpt_more( $more ) {
    return '.';
}
add_filter( 'excerpt_more', 'rs_theme_excerpt_more' );

/*
 * Removed Continue reading on manual generated excerpt;
 */
function rs_theme_custom_excerpt_more( $output ) {
	global $post;
	
	if ( has_excerpt() ) {
		$output .= '<p class="cta">
			<a class="btn btn-outline-secondary read-more" href="'. get_permalink( $post->ID ) .'">Read More</a>
		</p>';
	}

	return preg_replace( '/<a[^>]+>Continue reading.*?<\/a>/i', '', $output );
}
add_filter( 'get_the_excerpt', 'rs_theme_custom_excerpt_more', 20 );


/*
 * Remove more-link jumper
 */
function rsthem_remove_more_jump_link( $link ) 
{
	$offset = strpos( $link, '#more-' );
	
	if( $offset )
		$end = strpos( $link, '"', $offset );
	
	if( $end )
		$link = substr_replace( $link, '', $offset, $end-$offset );
	
	return $link;
}
add_filter( 'the_content_more_link', 'rsthem_remove_more_jump_link' );


/*
 * Add new defaults in comment form
 */
function rsthem_new_comment_form_default( $default ) {
	$defaults = array(
		'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) .'</p>',
		'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
	);
	
	return $defaults;
}
add_filter( 'comment_form_defaults', 'rsthem_new_comment_form_default' );

/**
 * Help site speed
 **/

/*
Remove query strings from CSS and JS inclusions
*/
function _remove_script_version($src) {
	$parts = explode('?ver', $src);
	return $parts[0];
}
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );

/*
Force scripts to defer for better performance
*/

/*
Remove jquery migrate for enhanced performance
*/
function remove_jquery_migrate($scripts) {
   	if (is_admin())
		return;
	
	$scripts->remove('jquery');
	$scripts->add('jquery', false, array('jquery-core'), '1.10.2');
}
add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

/**
 * Allow SVG file-type
 * @param  $mimes file-type to add
 * @return New file-type added
 */
function make_mime_types( $mimes ) {
	$mimes['svg'] 	= 'image/svg+xml';
	$mimes['svgz'] 	= 'image/svg+xml';
	$mimes['ogg'] 	= 'audio/ogg';
	$mimes['ogv'] 	= 'audio/ogv';
	
	return $mimes;
}
add_filter( 'upload_mimes', 'make_mime_types' );

/**
 * Filter Nav Menu Display
 */
function add_extra_item_to_nav_menu( $items, $args ) {

	$facebook_username 	= get_field('field_5deb573179c45', 'option');
	$instagram_username = get_field('field_5deb574179c46', 'option');
	$youtube_username 	= get_field('field_5deb575279c47', 'option');

	// var_dump( $args );
	
	if( 'header_top_menu' == $args->theme_location
		|| 'mobile_header_top_menu' == $args->theme_location ) {
		$items .= '
			<li class="menu-item facebook">
				<a href="https://www.facebook.com/'. $facebook_username .'">
					<svg width="13" height="13" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
						<g>
							<g>
								<path d="M288,176v-64c0-17.664,14.336-32,32-32h32V0h-64c-53.024,0-96,42.976-96,96v80h-64v80h64v256h96V256h64l32-80H288z"/>
							</g>
						</g>
					</svg>
				</a>
			</li>
			<li class="menu-item instagram">
				<a href="https://www.facebook.com/'. $instagram_username .'">
					<svg width="13" height="13" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
						<g>
							<g>
								<path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
									C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
									h192c61.76,0,112,50.24,112,112V352z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
									c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"/>
							</g>
						</g>
						<g>
							<g>
								<circle cx="393.6" cy="118.4" r="17.056"/>
							</g>
						</g>
					</svg>
				</a>
			</li>
			<li class="menu-item youtube">
				<a href="https://www.facebook.com/'. $youtube_username .'">
					<svg width="13" height="13" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
						<g>
							<g>
								<path d="M490.24,113.92c-13.888-24.704-28.96-29.248-59.648-30.976C399.936,80.864,322.848,80,256.064,80
									c-66.912,0-144.032,0.864-174.656,2.912c-30.624,1.76-45.728,6.272-59.744,31.008C7.36,138.592,0,181.088,0,255.904
									C0,255.968,0,256,0,256c0,0.064,0,0.096,0,0.096v0.064c0,74.496,7.36,117.312,21.664,141.728
									c14.016,24.704,29.088,29.184,59.712,31.264C112.032,430.944,189.152,432,256.064,432c66.784,0,143.872-1.056,174.56-2.816
									c30.688-2.08,45.76-6.56,59.648-31.264C504.704,373.504,512,330.688,512,256.192c0,0,0-0.096,0-0.16c0,0,0-0.064,0-0.096
									C512,181.088,504.704,138.592,490.24,113.92z M192,352V160l160,96L192,352z"/>
							</g>
						</g>
					</svg>
				</a>
			</li>
		';
	}

	if( 'tablet_menu' == $args->theme_location ) {
		
		$items .= '
			<li class="menu-item menu-icon">
				<a href="#" class="menu-toggle">
					<svg width="19" height="12" viewBox="0 0 19 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M0.201172 2V0H18.2012V2H0.201172ZM0.201172 7H18.2012V5H0.201172V7ZM0.201172 12H18.2012V10H0.201172V12Z" fill="#003145"/>
					</svg>		
				</a>
			</li>
		';
	}
	
	if( 'primary' == $args->theme_location
		|| 'tablet_menu' == $args->theme_location ) {
		
		$items .= '
			<li class="menu-item search">
				<a href="#">
					<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12.9808 8.09807C12.9808 8.37387 12.9611 8.64573 12.9256 8.91759C12.9414 8.81121 12.9532 8.70877 12.969 8.60239C12.8941 9.13035 12.7523 9.64255 12.5474 10.135C12.5868 10.0405 12.6262 9.94593 12.6656 9.85137C12.5159 10.202 12.3346 10.5409 12.1179 10.8561C12.0707 10.927 12.0194 10.9979 11.9682 11.0688C11.9131 11.1437 11.7791 11.2816 12.0273 10.994C11.9997 11.0255 11.9722 11.0609 11.9446 11.0925C11.8224 11.2382 11.6964 11.3801 11.5624 11.514C11.4363 11.6401 11.3024 11.7623 11.1645 11.8805C11.1526 11.8923 11.062 11.979 11.0423 11.979C11.062 11.979 11.3063 11.782 11.1369 11.9041C11.062 11.9593 10.9832 12.0144 10.9044 12.0696C10.5892 12.2823 10.2504 12.4636 9.89972 12.6172C9.99428 12.5778 10.0888 12.5384 10.1834 12.499C9.6909 12.7039 9.17871 12.8458 8.65075 12.9206C8.75713 12.9049 8.85957 12.893 8.96595 12.8773C8.41829 12.9482 7.87063 12.9482 7.32297 12.8773C7.42935 12.893 7.53179 12.9049 7.63817 12.9206C7.11021 12.8458 6.59801 12.7039 6.10551 12.499C6.20007 12.5384 6.29463 12.5778 6.38919 12.6172C6.03853 12.4675 5.69969 12.2863 5.38449 12.0696C5.31357 12.0223 5.24265 11.9711 5.17173 11.9199C5.09687 11.8647 4.95897 11.7307 5.24659 11.979C5.21507 11.9514 5.17961 11.9238 5.14809 11.8962C5.00231 11.7741 4.86047 11.648 4.72651 11.514C4.60043 11.388 4.47829 11.254 4.36009 11.1161C4.34827 11.1043 4.26159 11.0137 4.26159 10.994C4.26159 11.0137 4.45859 11.2579 4.33645 11.0885C4.28129 11.0137 4.22613 10.9349 4.17097 10.8561C3.95821 10.5409 3.77697 10.202 3.62332 9.85137C3.66271 9.94593 3.70212 10.0405 3.74151 10.135C3.53664 9.64255 3.3948 9.13035 3.31994 8.60239C3.3357 8.70877 3.34752 8.81121 3.36328 8.91759C3.29236 8.36993 3.29236 7.82227 3.36328 7.27461C3.34752 7.38099 3.3357 7.48343 3.31994 7.58981C3.3948 7.06185 3.53664 6.54965 3.74151 6.05715C3.70212 6.15171 3.66271 6.24627 3.62332 6.34083C3.77303 5.99017 3.95427 5.65133 4.17097 5.33613C4.21825 5.26521 4.26947 5.19429 4.32069 5.12337C4.37585 5.04851 4.50981 4.91061 4.26159 5.19823C4.28917 5.16671 4.31675 5.13125 4.34433 5.09973C4.46647 4.95395 4.59255 4.81211 4.72651 4.67815C4.85259 4.55208 4.98655 4.42994 5.12445 4.31174C5.13627 4.29992 5.22689 4.21324 5.24659 4.21324C5.22689 4.21324 4.98261 4.41024 5.15203 4.2881C5.22689 4.23294 5.30569 4.17778 5.38449 4.12262C5.69969 3.90986 6.03853 3.72862 6.38919 3.57496C6.29463 3.61436 6.20007 3.65376 6.10551 3.69316C6.59801 3.48828 7.11021 3.34644 7.63817 3.27158C7.53179 3.28734 7.42935 3.29916 7.32297 3.31492C7.87063 3.244 8.41829 3.244 8.96595 3.31492C8.85957 3.29916 8.75713 3.28734 8.65075 3.27158C9.17871 3.34644 9.6909 3.48828 10.1834 3.69316C10.0888 3.65376 9.99428 3.61436 9.89972 3.57496C10.2504 3.72468 10.5892 3.90592 10.9044 4.12262C10.9753 4.1699 11.0463 4.22112 11.1172 4.27234C11.192 4.3275 11.3299 4.46146 11.0423 4.21324C11.0738 4.24082 11.1093 4.2684 11.1408 4.29598C11.2866 4.41812 11.4284 4.54419 11.5624 4.67815C11.6885 4.80423 11.8106 4.93819 11.9288 5.07609C11.9406 5.08791 12.0273 5.17853 12.0273 5.19823C12.0273 5.17853 11.8303 4.93425 11.9525 5.10367C12.0076 5.17853 12.0628 5.25733 12.1179 5.33613C12.3307 5.65133 12.5119 5.99017 12.6656 6.34083C12.6262 6.24627 12.5868 6.15171 12.5474 6.05715C12.7523 6.54965 12.8941 7.06185 12.969 7.58981C12.9532 7.48343 12.9414 7.38099 12.9256 7.27461C12.9611 7.55041 12.9769 7.82621 12.9808 8.09807C12.9847 8.71665 13.5206 9.30765 14.1628 9.28007C14.8011 9.25249 15.3487 8.75999 15.3448 8.09807C15.3369 6.65209 14.9035 5.17459 14.0564 3.99654C13.166 2.75938 11.98 1.8453 10.5459 1.31734C7.81153 0.304763 4.50981 1.1952 2.65802 3.44888C1.67302 4.65057 1.09384 6.05321 0.96776 7.60557C0.84956 9.05549 1.22386 10.5724 1.98034 11.8174C2.7053 13.0112 3.79667 14.0317 5.07717 14.6069C6.52709 15.261 8.09127 15.454 9.65545 15.1388C12.4528 14.5754 14.7971 12.172 15.2345 9.34311C15.2975 8.92941 15.3448 8.51965 15.3487 8.10201C15.3527 7.48343 14.805 6.89243 14.1667 6.92001C13.5206 6.94759 12.9847 7.43615 12.9808 8.09807Z" fill="black"/>
						<path d="M11.6255 13.2043C12.2559 13.8347 12.8863 14.4651 13.5167 15.0955C14.5253 16.1041 15.5379 17.1167 16.5465 18.1253C16.779 18.3578 17.0075 18.5863 17.24 18.8188C17.6773 19.2561 18.4811 19.2876 18.9105 18.8188C19.3439 18.346 19.3794 17.6131 18.9105 17.1482C18.2801 16.5178 17.6497 15.8874 17.0193 15.257C16.0107 14.2484 14.9981 13.2358 13.9895 12.2272C13.757 11.9947 13.5285 11.7662 13.296 11.5337C12.8587 11.0964 12.0549 11.0649 11.6255 11.5337C11.1921 12.0065 11.1566 12.7394 11.6255 13.2043Z" fill="black"/>
					</svg>	
				</a>
			</li>
		';
	}
	
    return $items;
}
add_filter( 'wp_nav_menu_items', 'add_extra_item_to_nav_menu', 10, 2 );