<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package rs-theme
 */

get_header();
?>
	
	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">
			
			<?php
				while ( have_posts() ) : the_post();
					
					?>
					<div class="container">

						<div class="row align-items-center">
							<div class="col-lg-2">
								<div class="d-none d-sm-block">
									<?php
										/**
										 * Include share menu
										 */
										get_template_part('template-parts/partials/content', 'menu');
									?>
								</div>
							</div>
							
							<div class="col-lg-8">
								
								<header class="entry-header">
									<div class="cat">
										<?php
											/* translators: used between list items, there is a space after the comma */
											$categories_list = get_the_category_list( esc_html__( ', ', 'rs-theme' ) );
											if ( $categories_list && rs_theme_categorized_blog() ) {
												printf( $categories_list ); // WPCS: XSS OK.
											}
										?>
									</div>

									<?php
										// get the post-title
										the_title( '<h1 class="entry-title">', '</h1>' );

										if ( 'post' === get_post_type() ) :
											?>
												<div class="entry-meta">
													<time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
														<?php echo get_the_date('M j, Y'); ?>
													</time>
												</div><!-- .entry-meta -->
											<?php
										endif;
									?>

									<div class="d-block d-sm-none mt-3 mt-sm-0">
										<?php
											/**
											 * Include share menu
											 */
											get_template_part('template-parts/partials/content', 'menu');
										?>
									</div>
								</header><!-- .entry-header -->

								<?php if( ! empty( apply_filters('the_content', get_the_content() ) ) ) : ?>
									<div class="entry-content">
										<?php
											the_content( sprintf(
												/* translators: %s: Name of current post. */
												wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'rs-theme' ), array( 'span' => array( 'class' => array() ) ) ),
												the_title( '<span class="screen-reader-text">"', '"</span>', false )
											) );
										?>
									</div><!-- .entry-content -->
								<?php endif; ?>

							</div>
						</div>
					</div> <!-- .container -->
					<?php
					
					// check if the flexible content field has rows of data
					if( have_rows('field_5dea5eb6a92de') ):
						
						// loop through the rows of data
						while ( have_rows('field_5dea5eb6a92de') ) : the_row();
							
							/**
							 * include the flex partials
							 */
							get_template_part( 'page-templates/partials/stripe', get_row_layout() );

						endwhile;

					else :

						// no layouts found

					endif;
					
				endwhile; // End of the loop.

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
						
		</div><!-- #primary -->

		<?php
			// get next-post
			$next_post = get_next_post();
			
			// next is next_post is a POST
			if ( is_a( $next_post , 'WP_Post' ) ) :

				// get next post-id
				$next_post_id =  $next_post->ID;
				?>
				<div class="next-post" style="background-image: url(<?php echo get_the_post_thumbnail_url( $next_post_id ) ?>);">
					<div class="container">

						<a href="<?php echo get_permalink( $next_post_id ); ?>">
							<div class="row justify-content-center">
								<div class="col-md-8">
									
									<div class="content">
										<div class="media align-items-center">
											<div class="pr-lg-4">
												<p class="next-post__label">
													Next Article
												</p>
												<h3 class="mb-0">
													<?php echo get_the_title( $next_post_id ); ?>
												</h3>
											</div>
											<div>
												<svg width="32" height="33" viewBox="0 0 32 33" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M32 17.2217L15.6596 32.7744H0.972645L16.3404 17.2217L0 0.77442H15.6596L32 17.2217Z" fill="white"/>
												</svg>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</a>

					</div> <!-- .container -->
				</div>
				<?php
			endif;
		?>

		<div class="related-posts">

			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<div class="text-center">
							<header class="heading">
								<h2>Related Articles</h2>
							</header>
						</div>

					</div>
				</div>
			</div>

			<div class="container">
				<div class="row justify-content-center">

					<?php
						$post_not_in 		= [get_the_ID()];
						
						$related_post_args 	= [
							'post_type' 		=> 'post',
							'posts_per_page' 	=> 3,
							'post__not_in'  	=> $post_not_in,
							// 'orderby' 		=> 'rand',
    						'order'   		=> 'DESC',
						];
						
						// The Query
						$related_post_query = new WP_Query( $related_post_args );
						
						// The Loop
						if ( $related_post_query->have_posts() ) {

							// declare counter for later use
							$related_item_counter = 1;
							
							while ( $related_post_query->have_posts() ) { $related_post_query->the_post();
								?>
									<div class="col-md-6 col-lg-4 <?php echo 'item-'. $related_item_counter; ?>">
										<div class="related-posts__item news-card">

											<a href="<?php the_permalink(); ?>">

												<figure class="mb-0">
													<?php
														if ( has_post_thumbnail() ) :
															the_post_thumbnail();
														endif;
													?>
												</figure>
												
												<div class="content">
													<div class="text-center">
														<div class="date">
															<time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
																<?php echo get_the_date('M j, Y'); ?>
															</time>
														</div>

														<header>
															<h3><?php the_title(); ?></h3>
														</header>
														
														<div class="cta">
															<p class="mb-0">
																<span>Read Article</span>
															</p>
														</div>
													</div>
												</div>
											</a>

										</div>
									</div>
								<?php

								// increment counter
								$related_item_counter++;
							}

						}
						else {
							// no posts found
						}
						
						/* Restore original Post Data */
						wp_reset_postdata();
					?>

				</div>
			</div>

		</div>

	</main><!-- #main -->

<?php
get_footer();