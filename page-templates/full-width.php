<?php
/**
 * Template Name: Full Width
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header(); ?>
	
	<main id="main" class="site-main" role="main">
        <div id="primary" class="content-area">
            <div class="container">
                
                <div class="row justify-content-center">
                    <div class="col-md-12">

                        <?php
                            while ( have_posts() ) : the_post();

                                get_template_part( 'template-parts/content', 'page' );

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;

                            endwhile; // End of the loop.
                        ?>

                    </div>
                </div>

            </div> <!-- .container -->
        </div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();