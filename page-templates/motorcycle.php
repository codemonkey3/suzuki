<?php
/**
 * Template Name: Motorcycle
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header();

// get ACF value for later use
$compare = get_field('field_5df9f031372e0', 'option');

// get product category list
$temp_terms = get_terms( array(
    'taxonomy'      => 'product-cat',
    'hide_empty'    => false,
) );  

if ( ! empty( $temp_terms ) && ! is_wp_error( $temp_terms ) ) {
    $terms = [];
    
    // loop through the terms
    foreach ( $temp_terms as $temp_term ) {
        $order = get_field( 'order', $temp_term );

        $terms[$order] = $temp_term;
    }
    
    // order product-category
    ksort( $terms );
}
?>
	
	<main id="main" class="site-main m-0" role="main">

        <div class="filter">
            <div class="container">
                <div class="row no-gutters no-sm-gutters">
                    <div class="col-md-8">
                        <div class="filter-wrap">
                            <ul class="m-0 list-unstyled d-sm-flex filter__list">
                                <li>
                                    <a href="javascript: void(0);" id="filterby_btn">
                                        <div class="media align-items-center justify-content-center justify-content-sm-start">
                                            <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M4.88889 8H7.55556V6.66683H4.88889V8ZM0 0V1.33317H12.4444V0H0ZM2.22222 4.68842H10.2222V3.31189H2.22222V4.68842Z" fill="#003145"/>
                                            </svg>
                                            <span class="ml-3">Filter By</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="filter__list--category">
                                    <a href="javascript: void(0);">
                                        <div class="media align-items-center justify-content-sm-center">
                                            <span class="filter-orderby__category mr-3">Category</span>
                                            <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.5 6L0.736861 0.75L10.2631 0.749999L5.5 6Z" fill="#007BBB"/>
                                            </svg>
                                        </div>
                                    </a>
                                    
                                    <?php if( $terms ) : ?>
                                        <ul class="m-0 list-unstyled sub-menu button-group" data-filter-group="category">
                                            <li class="active">
                                                <a href="javascript:void(0);" data-filter="">Any</a>
                                            </li>
                                            
                                            <?php foreach( $terms as $term ) : ?>
                                                <li>
                                                    <a href="javascript:void(0);" data-filter="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </li>
                                <li class="filter__list--price">
                                    <a href="javascript: void(0);" data-filter="*">
                                        <div class="media align-items-center justify-content-sm-center">
                                            <span class="filter-orderby__price mr-3">Price Range</span>
                                            <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.5 6L0.736861 0.75L10.2631 0.749999L5.5 6Z" fill="#007BBB"/>
                                            </svg>
                                        </div>
                                    </a>
                                    
                                    <ul class="m-0 list-unstyled sub-menu button-group" data-filter-group="price">
                                        <li class="active">
                                            <a href="javascript:void(0);" data-filter="">Any</a>
                                        </li>
                                        <li>
                                            <a href="javascript: void(0);" data-filter="50000-100000">50,000 - 100,000</a>
                                        </li>
                                        <li>
                                            <a href="javascript: void(0);" data-filter="150000-300000">150,000 - 300,000</a>
                                        </li>
                                        <li>
                                            <a href="javascript: void(0);" data-filter="350000-500000">350,000 - 500,000</a>
                                        </li>
                                        <li>
                                            <a href="javascript: void(0);" data-filter="550000-700000">550,000 - 700,000</a>
                                        </li>
                                        <li>
                                            <a href="javascript: void(0);" data-filter="750000-900000">750,000 - 900,000</a>
                                        </li>
                                        <li>
                                            <a href="javascript: void(0);" data-filter="900500-1500000">900,500 - 1,500,000</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            
                            <div class="filter-apply mt-5">
                                <p class="mb-0 d-block d-sm-none">
                                    <a class="btn w-100 filter-apply__btn" href="javascript: void(0);">Apply</a>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="compare-btn text-right d-none d-sm-block">
                            <p class="m-0">
                                <a class="btn" href="<?php echo $compare['url']; ?>"><?php echo $compare['title'] ?? 'Compare'; ?></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="primary" class="content-area">
            <div class="container">
                
                <div class="row justify-content-center">
                    <div class="col-md-12">

                        <div class="motorcycles-ajax">
                            <!-- .ajax display goes here! -->
                        </div>
                        
                        <?php
                            // loop through all the ordered terms
                            if( $terms ) {

                                ?>
                                <div class="motorcycles">
                                    <?php

                                    foreach( $terms as $term ) {
                                        
                                        ?>
                                        <div class="motorcycles__cat cat-<?php echo $term->term_id; ?>">
                                            
                                            <div class="text-center">
                                                <header id="<?php echo $term->slug; ?>">
                                                    <h2><?php echo $term->name; ?></h2>
                                                </header>
                                                
                                                <div class="cat-description">
                                                    <div class="row justify-content-center">
                                                        <div class="col-md-5">
                                                            <?php echo wpautop( $term->description ); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="motorcycles__items">
                                                <div class="row <?php echo ( 7 == $term->term_id ) ? 'backcone-grid' : ''; ?> motorcycle-grid">
                                                    <?php
                                                        $args = [
                                                            'post_type' => 'product',
                                                            'tax_query' => [
                                                                [
                                                                    'taxonomy'  => 'product-cat',
                                                                    'field'     => 'term_id',
                                                                    'terms'     => $term->term_id
                                                                ]
                                                            ],
                                                            'orderby' => 'menu_order title',
                                                            'order'   => 'ASC',
                                                        ];
                                                        
                                                        // The Query
                                                        $the_query = new WP_Query( $args );
                                                        
                                                        // The Loop
                                                        if ( $the_query->have_posts() ) {
                                                            
                                                            // set backbone type for later use
                                                            $backbone_type = $backbone_item_class = '';
                                                            
                                                            while ( $the_query->have_posts() ) { $the_query->the_post();
                                                                
                                                                // 7 == Backbone
                                                                if( 7 == $term->term_id ) :
                                                                    $backbone_type          = get_field('field_5dfc8fbeb748f');
                                                                    $backbone_item_class    = 'backbone-item';
                                                                endif;
                                                                ?>
                                                                    
                                                                    <div class="col-lg-6 col-xl-4 <?php echo $backbone_item_class . ' '. $backbone_type ?? ''; ?> motorcycle-item">
                                                                        <div class="motorcycles__item">
                                                                            
                                                                            <?php
                                                                            /**
                                                                             * Include content-grid
                                                                             */
                                                                            get_template_part('template-parts/single-product/content', 'grid'); ?>
                                                                
                                                                        </div>
                                                                    </div>
                            
                                                                <?php
                                                            }
                                                            
                                                        }
                                                        else {
                                                            // no posts found
                                                        }
                            
                                                        /* Restore original Post Data */
                                                        wp_reset_postdata();
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php

                                    }

                                    ?>
                                </div>
                                <?php

                            }
                        ?>

                    </div>
                </div>

            </div> <!-- .container -->
            
            <div class="back-to-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="text-center">
                                <a href="#page" id="back_top" class="back-to-top__btn">
                                    <div class="media justify-content-center align-items-center">
                                        <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.07229 3.59468e-07L16.8486 8.17021L16.8486 15.5137L9.07229 7.82979L0.848635 16L0.848635 8.17021L9.07229 3.59468e-07Z" fill="#003B70"/>
                                        </svg>
                                        <span class="ml-3">Back to Top</span>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div><!-- #primary -->

	</main><!-- #main -->

<?php
get_footer();