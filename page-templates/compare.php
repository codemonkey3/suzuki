<?php
/**
 * Template Name: Compare
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header();

// Get ACF value
$title              = get_field('field_5ec9dd15d9ef1');
$excerpt            = get_field('field_5ec9dd39d9ef2');
$mobile_excerpt     = get_field('field_5edf61bb91727');

// dummy products in to compare
$products   = [
    1 => 393,
    2 => 1340,
    3 => 394
];

$args = [
    'post_type' => 'product',
    'orderby'   => 'menu_order title',
    'order'     => 'ASC',
];

// The Query
$the_query = new WP_Query( $args );
?>
    
    <div class="compare-header">
        <div class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        <header>
                            <h1>
                                <?php
                                    if( $title )
                                        echo $title;
                                    else
                                        the_title();
                                ?>
                            </h1>
                        </header>
                        
                        <div class="excerpt">
                            <p class="mb-0 d-none d-md-block"><?php echo $excerpt; ?></p>
                            <p class="mb-0 d-block d-md-none"><?php echo $mobile_excerpt; ?></p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <div class="compared-items">
            <div class="container">
                
                <div class="row">
                    <?php if( $products ) { ?>
                        <div class="col-md-3 order-3 order-md-1">

                            <div class="selector">
                                <ul class="m-0 list-unstyled">
                                    <?php
                                    // set initial display
                                    foreach( $products as $key => $product_id ) { ?>
                                        <li>
                                            <div class="media align-items-center justify-content-between">
                                                <div class="indicator" data-order="<?php echo $key; ?>">
                                                    <span><?php echo get_the_title( $product_id ); ?></span>
                                                </div>
                                                <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M5.34623 11L2.45528e-07 5.38298L4.6621e-07 0.334346L5.34624 5.61702L11 0L11 5.38298L5.34623 11Z" fill="#C81820"/>
                                                </svg>
                                            </div>
                                            
                                            <?php
                                                // The Loop
                                                if ( $the_query->have_posts() ) {
                                                    ?>
                                                    <ul class="m-0 list-unstyled">
                                                        <?php
                                                            while ( $the_query->have_posts() ) { $the_query->the_post();
                                                                ?>
                                                                    
                                                                    <li>
                                                                        <a href="javascript: void(0);" data-id="<?php the_ID(); ?>" data-order="<?php echo $key; ?>">
                                                                            <?php the_title(); ?>
                                                                        </a>
                                                                    </li>
                                                                    
                                                                <?php
                                                            }
                                                        ?>
                                                    </ul>
                                                    <?php
                                                }

                                                /* Restore original Post Data */
                                                wp_reset_postdata();
                                            ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        
                        <?php                           
                            // The Loop
                            if ( $the_query->have_posts() ) {
                                while ( $the_query->have_posts() ) { $the_query->the_post();
                                    $product_thumbnail  = get_field( 'field_5df382a2fa7ec', get_the_ID() );
                                    $price              = get_field( 'field_5dee25d53fe70', get_the_ID() );

                                    ?>
                                    <div class="col-md-3 compare-column product-<?php the_ID(); echo in_array( get_the_ID(), $products ) ? ' order-' . array_search(get_the_ID(), $products) : ''; ?>">
                                        <div class="compare-card text-center">
                                            <figure>
                                                <img src="<?php echo esc_url( $product_thumbnail['url'] ); ?>" alt="<?php echo esc_attr( $product_thumbnail['alt'] ); ?>">
                                            </figure>
                                            
                                            <header>
                                                <h2><?php echo get_the_title( get_the_ID() ); ?></h2>
                                            </header>
                                            
                                            <div class="media align-items-center justify-content-center">
                                                <div class="price">
                                                    <span class="amount" data-price="<?php echo $price; ?>">
                                                        <span class="currency">PHP</span> <?php echo number_format( $price, 0 ); ?>
                                                    </span>
                                                </div>
                                                <div class="cta">
                                                    <a href="<?php echo get_permalink( get_the_ID() ) ?>" class="text-uppercase">Explore</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            
                            /* Restore original Post Data */
                            wp_reset_postdata();
                        ?>
                        
                    <?php } ?>
                </div>

            </div> <!-- .container -->
        </div>
    </div>

	<main id="main" class="site-main" role="main">
        <div id="primary" class="content-area">

            <div class="compare-data">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="engine compare-data__item">
                                <header>
                                    <h3>Engine</h3>
                                </header>

                                <div class="items">
                                    <?php
                                        $engines = [];

                                        // The Loop
                                        if ( $the_query->have_posts() ) {
                                            
                                            while ( $the_query->have_posts() ) { $the_query->the_post();

                                                // check if the repeater field has rows of data
                                                if( have_rows('field_5df6ec3b3c401') ) :
                                                    
                                                    // loop through the rows of data
                                                    while ( have_rows('field_5df6ec3b3c401') ) : the_row();
                                                        
                                                        // display a sub field value
                                                        $label = get_sub_field('field_5df6ec3b3c402');
                                                        $value = get_sub_field('field_5df6ec3b3c403');
                                                        
                                                        $engines[sanitize_title($label)][get_the_ID()] = [
                                                            $value
                                                        ];
                                                        
                                                    endwhile;

                                                else :

                                                    // no rows found

                                                endif;

                                            }
                                        }

                                        /* Restore original Post Data */
                                        wp_reset_postdata();
                                        
                                        if( is_array( $engines ) ) {

                                            // initialize our counter
                                            $item_counter = 1;

                                            foreach( $engines as $label => $engine ) {

                                                ?>
                                                <div class="item <?php echo $item_counter % 2 == 0 ? 'even' : 'odd'; ?>">
                                                    <div role="row" class="row compare-row">
                                                        <div class="col-md-3">
                                                            <div class="specification">
                                                                <span class="text-uppercase"><?php echo str_replace(['-'], [' '], $label); ?></span>
                                                            </div>
                                                        </div>
                                                        
                                                        <?php
                                                            // The Loop
                                                            if ( $the_query->have_posts() ) {   
                                                                while ( $the_query->have_posts() ) { $the_query->the_post();
                                                                    ?>
                                                                    <div class="offset-md-1 col-md-2 compare-column product-<?php the_ID(); echo in_array( get_the_ID(), $products ) ? ' order-' . array_search(get_the_ID(), $products) : ''; ?>">
                                                                        <?php echo $engine[get_the_ID()][0] ?? 'n/a'; ?>

                                                                        <div class="specification d-block d-md-none mt-1 mt-md-0">
                                                                            <span class="text-uppercase"><?php echo str_replace(['-'], [' '], $label); ?></span>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php

                                                // increment counter
                                                $item_counter++;
                                            }
                                            
                                        }
                                    ?>
                                </div>
                                
                            </div>
                            
                            <div class="chassis compare-data__item">
                                <header>
                                    <h3>Chassis</h3>
                                </header>
                                
                                <div class="items">
                                    <?php
                                        $chassis = [];

                                        // The Loop
                                        if ( $the_query->have_posts() ) {
                                            
                                            while ( $the_query->have_posts() ) { $the_query->the_post();
                                                if( have_rows('field_5df6ec00f8015') ) :
                                                
                                                    // loop through the rows of data
                                                    while ( have_rows('field_5df6ec00f8015') ) : the_row();
    
                                                        // display a sub field value
                                                        $label = get_sub_field('field_5df6ec00f8016');
                                                        $value = get_sub_field('field_5df6ec00f8017');
                                                        
                                                        $chassis[sanitize_title($label)][get_the_ID()] = [
                                                            $value
                                                        ];
    
                                                    endwhile;
    
                                                else :
                                                    
                                                    // no rows found
                                                    
                                                endif;
                                            }

                                        }

                                        if( is_array( $chassis ) ) {

                                            // item-counter
                                            $item_counter = 1;

                                            foreach( $chassis as $label => $chassi ) {

                                                ?>
                                                    <div class="item <?php echo $item_counter % 2 == 0 ? 'even' : 'odd'; ?>">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="specification">
                                                                    <span class="text-uppercase"><?php echo str_replace(['-'], [' '], $label); ?></span>
                                                                </div>
                                                            </div>
                                                            
                                                            <?php
                                                                // The Loop
                                                                if ( $the_query->have_posts() ) {   
                                                                    while ( $the_query->have_posts() ) { $the_query->the_post();
                                                                        ?>
                                                                        <div class="offset-md-1 col-md-2 compare-column product-<?php the_ID(); echo in_array( get_the_ID(), $products ) ? ' order-' . array_search(get_the_ID(), $products) : ''; ?>">
                                                                            <?php echo $chassi[get_the_ID()][0] ?? 'n/a'; ?>

                                                                            <div class="specification d-block d-md-none mt-1 mt-md-0">
                                                                                <span class="text-uppercase"><?php echo str_replace(['-'], [' '], $label); ?></span>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                <?php

                                                // increment counter
                                                $item_counter++;
                                            }
                                            
                                        }
                                    ?>
                                </div>
                            </div>

                            <div class="dimensions compare-data__item">
                                <header>
                                    <h3>Dimensions</h3>
                                </header>
                                
                                <div class="items">
                                    <?php
                                        $dimensions = [];

                                        // The Loop
                                        if ( $the_query->have_posts() ) {
                                            
                                            while ( $the_query->have_posts() ) { $the_query->the_post();
                                                // check if the repeater field has rows of data
                                                if( have_rows('field_5df6ebc5e11d9') ) :

                                                    // loop through the rows of data
                                                    while ( have_rows('field_5df6ebc5e11d9') ) : the_row();
                                                        
                                                        // display a sub field value
                                                        $label = get_sub_field('field_5df6ebd4e11da');
                                                        $value = get_sub_field('field_5df6ebdee11db');
                                                        
                                                        $dimensions[sanitize_title($label)][get_the_ID()] = [
                                                            $value
                                                        ];
                                                    endwhile;

                                                else :

                                                    // no rows found

                                                endif;
                                            }

                                        }
                                        
                                        if( is_array( $dimensions ) ) {
                                            
                                            // item-counter
                                            $item_counter = 1;
                                            
                                            foreach( $dimensions as $label => $dimension ) {
                                                
                                                ?>
                                                    <div class="item <?php echo $item_counter % 2 == 0 ? 'even' : 'odd'; ?>">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="specification">
                                                                    <span class="text-uppercase"><?php echo str_replace(['-'], [' '], $label); ?></span>
                                                                </div>
                                                            </div>
                                                            
                                                            <?php
                                                                // The Loop
                                                                if ( $the_query->have_posts() ) {   
                                                                    while ( $the_query->have_posts() ) { $the_query->the_post();
                                                                        ?>
                                                                        <div class="offset-md-1 col-md-2 compare-column product-<?php the_ID(); echo in_array( get_the_ID(), $products ) ? ' order-' . array_search(get_the_ID(), $products) : ''; ?>">
                                                                            <?php echo $dimension[get_the_ID()][0] ?? 'n/a'; ?>

                                                                            <div class="specification d-block d-md-none mt-1 mt-md-0">
                                                                                <span class="text-uppercase"><?php echo str_replace(['-'], [' '], $label); ?></span>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                <?php

                                                // increment counter
                                                $item_counter++;
                                            }
                                            
                                        }
                                    ?>
                                </div>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
           
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="text-center">
                            <a href="#page" id="back_top">
                                <div class="media justify-content-center align-items-center">
                                    <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.07229 3.59468e-07L16.8486 8.17021L16.8486 15.5137L9.07229 7.82979L0.848635 16L0.848635 8.17021L9.07229 3.59468e-07Z" fill="#003B70"/>
                                    </svg>
                                    <span class="ml-3">Back to Top</span>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>

        </div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();