<?php
/**
 * Template Name: Events
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header();

// Find todays date in Ymd format.
$date_now = date('Y-m-d H:i:s');
?>
	
	<main id="main" class="site-main mt-0" role="main">
        <div id="primary" class="content-area">

            <div class="current-events">
                
                <?php
                    $current_args = [
                        'post_type'         => 'post',
                        'category_name'     => 'events',
                        'meta_query'        => array(
                            array(
                                'key'     => 'event_date',
                                'compare' => '>=',
                                'value'   => $date_now,
                                'type'    => 'DATETIME',
                            ),
                        ),
                        'posts_per_page'    => -1,
                    ];
                    
                    // The Query
                    $current_query = new WP_Query( $current_args );
                ?>
                
                <div class="current-events__items-wrap">
                    <div class="current-events__items">
                        <?php
                            // The Loop
                            if ( $current_query->have_posts() ) {
                                
                                while ( $current_query->have_posts() ) { $current_query->the_post();
                                    $hero      = get_field( 'field_5decd1feac80f' );
                                    $date      = get_field( 'field_5decd16efbcce' );
                                    $cta       = get_field( 'field_5e0219fddaa6c' );

                                    ?>
                                        <div class="current-events__item">
                                            
                                            <figure class="mb-0">
                                                <img src="<?php echo esc_url($hero['url']); ?>" alt="<?php echo esc_url($hero['alt']); ?>">
                                            </figure>
                                            
                                            <div class="detail-wrap">
                                                <div class="row align-items-end">
                                                    <div class="col-md-6 col-lg-5">

                                                        <div class="detail">
                                                            <div class="media">
                                                                <div class="date text-center">
                                                                    <time datetime="<?php date('c', strtotime( $date )); ?>" itemprop="datePublished">
                                                                        <span class="month"><?php echo date('M', strtotime( $date )); ?></span>
                                                                        <span class="day"><?php echo date('j', strtotime( $date )); ?></span>
                                                                    </time>
                                                                </div>

                                                                <div class="content">
                                                                    <p class="mb-0 news-label">
                                                                        Latest News
                                                                    </p>

                                                                    <header>
                                                                        <h3><?php the_title(); ?></h3>
                                                                    </header>

                                                                    <div class="excerpt">
                                                                        <?php the_excerpt(); ?>
                                                                    </div>

                                                                    <?php if( $cta ) : ?>
                                                                        <div class="cta">
                                                                            <a class="btn" href="<?php echo $cta['url'] ?>">
                                                                                <span>Event Details</span>
                                                                            </a>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    <?php
                                }

                            }
                            else {
                                // No current event
                            }

                            // Restore original Post Data
                            wp_reset_postdata();
                        ?>
                    </div>
                </div>

                <div class="current-events__nav-wrap">
                    <div class="current-events__nav">
                        <?php
                            // The Loop
                            if ( $current_query->have_posts() ) {
                                
                                while ( $current_query->have_posts() ) { $current_query->the_post();
                                    $excerpt   = get_field( 'field_5e041e1edbaa1' );
                                    $date      = get_field( 'field_5decd16efbcce' );
                                    $cta       = get_field( 'field_5e0219fddaa6c' );
                                    
                                    ?>
                                        <div class="current-events__nav__item">
                                            <div class="detail">
                                                <div class="media">
                                                    <div class="date text-center">
                                                        <time datetime="<?php date('c', strtotime( $date )); ?>" itemprop="datePublished">
                                                            <span class="month"><?php echo date('M', strtotime( $date )); ?></span>
                                                            <span class="day"><?php echo date('j', strtotime( $date )); ?></span>
                                                        </time>
                                                    </div>

                                                    <div class="content">
                                                        <div class="loading"></div>
                                                        
                                                        <header>
                                                            <h3><?php the_title(); ?></h3>
                                                        </header>

                                                        <div class="excerpt">
                                                            <?php echo wpautop( $excerpt ); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                }

                            }
                            else {
                                // No current event nav
                            }

                            /* Restore original Post Data */
                            wp_reset_postdata();
                        ?>
                    </div>
                </div>
                    
            </div>
            
            <div class="past-events">

                <div class="text-center">
                    <header class="heading">
                        <h2>Past Events</h2>
                    </header>
                </div>

                <div class="container">
                    
                    <div class="row justify-content-center">
                        <div class="col-md-9">

                            <?php
                                $event_args = [
                                    'post_type'         => 'post',
                                    'category_name'     => 'events',
                                    'meta_query'        => array(
                                        array(
                                            'key'     => 'event_date',
                                            'compare' => '<',
                                            'value'   => $date_now,
                                            'type'    => 'DATETIME',
                                        ),
                                    ),
                                    'posts_per_page'    => -1,
                                ];
                                
                                // The Query
                                $event_query = new WP_Query( $event_args );
                                
                                // The Loop
                                if ( $event_query->have_posts() ) {
                                    
                                    while ( $event_query->have_posts() ) { $event_query->the_post();
                                        $thumbnail    = get_field( 'field_5decd2d5282e6' );
                                        $date         = get_field( 'field_5decd16efbcce' );
                                        $cta          = get_field( 'field_5e0219fddaa6c' );
                                        
                                        ?>
                                            <div class="past-events__item">
                                                <a href="<?php echo esc_url( $cta['url'] ?? get_the_permalink() ); ?>" target="<?php echo $cta['target'] ?? '_blank'; ?>">
                                                    <div class="row row align-items-end">
                                                        <div class="col-md-4">
                                                            <figure class="m-0">
                                                                <img src="<?php echo esc_url($thumbnail['url']); ?>" alt="<?php echo $thumbnail['alt'] ?>" />
                                                            </figure>                      
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="content">
                                                            
                                                                <div class="date">
                                                                    <time datetime="<?php date('c', strtotime( $date )); ?>" itemprop="datePublished">
                                                                        <?php echo date('M j, Y', strtotime( $date )); ?>
                                                                    </time>
                                                                </div>

                                                                <header>
                                                                    <h3><?php the_title(); ?></h3>
                                                                </header>

                                                                <div class="excerpt">
                                                                    <?php the_excerpt(); ?>
                                                                </div>

                                                                <?php if( $cta ) : ?>
                                                                    <div class="cta">
                                                                        <p class="mb-0">
                                                                            <span>View Event Details</span>
                                                                        </p>
                                                                    </div>
                                                                <?php endif; ?>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                            </div>  
                                        <?php
                                    }
                                    
                                }
                                else {
                                    // no posts found
                                    _e( 'No events available yet.', 'suzuki' );
                                }

                                // Restore original Post Data
                                wp_reset_postdata();
                            ?>

                        </div>
                    </div>

                </div> <!-- .container -->
            </div>

        </div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();