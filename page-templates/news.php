<?php
/**
 * Template Name: News
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header(); ?>
	
	<main id="main" class="site-main" role="main">
        <div id="primary" class="content-area">

            <div class="news">
                
                <div class="news__latest">
                    
                    <?php
                        // get post sticky posts
                        $sticky      = get_option( 'sticky_posts' );
                        
                        // var_dump( $sticky );

                        $latest_args = [
                            'post_type'             => 'post',
                            'category_name'         => 'news',
                            'posts_per_page'        => 1,
                        ];
                        
                        if( $sticky && is_array( $sticky ) ) {
                            $latest_args['post__in'] = $sticky;
                        }
                        else {
                            $latest_args['order']   = 'DESC';
                            $latest_args['orderby'] = 'date';
                        }
                        
                        // The Query
                        $latest_query = new WP_Query( $latest_args );
                        
                        // The Loop
                        if ( $latest_query->have_posts() ) {
                            while ( $latest_query->have_posts() ) { $latest_query->the_post();

                                /* grab the url for the full size featured image */
                                $hero_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                                ?>
                                
                                <div class="hero" style="background-image: url(<?php echo $hero_url; ?>);">
                                
                                    <div class="container">
                                        <div class="row align-items-end">
                                            <div class="col-md-7">
                                                
                                                <p class="news-label">
                                                    Latest News 
                                                </p>
                                                
                                                <header>
                                                    <h2 class="entry-title"><?php the_title(); ?></h2>
                                                </header>

                                            </div>
                                        </div>
                                    </div>
                                
                                </div>

                                <?php
                            }
                        }
                        else {
                            // no posts found
                        }
                        
                        // Restore original Post Data
                        wp_reset_postdata();
                    ?>

                </div>
                
                <div class="news__items">
                    
                    <div class="container">
                        <div class="row">
                            <?php
                                $paged          = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                
                                $posts_per_page = 9;
                                $offset_start   = 1;
                                $offset         = ( $paged - 1 ) * $posts_per_page + $offset_start;
                                
                                $news_args = [
                                    'post_type'             => 'post',
                                    'category_name'         => 'news',
                                    'posts_per_page'        => $posts_per_page,
                                    'paged'                 => $paged
                                ];
                                
                                if( $sticky && is_array( $sticky ) ) {
                                    $news_args['post__not_in']        = $sticky;
                                    $news_args['ignore_sticky_posts'] = 1;
                                }
                                else {
                                    $news_args['offset']    = $offset;
                                }

                                // The Query
                                $news_query = new WP_Query( $news_args );
                                
                                // The Loop
                                if ( $news_query->have_posts() ) :
                                    
                                    while ( $news_query->have_posts() ) : $news_query->the_post();
                                        
                                        ?>
                                            <div class="col-lg-6 col-xl-4">
                                                <div class="news__item news-card">

                                                    <a href="<?php the_permalink(); ?>">
                                                        <figure class="mb-0">
                                                            <?php
                                                                if ( has_post_thumbnail() ) :
                                                                    the_post_thumbnail();
                                                                endif;
                                                            ?>
                                                        </figure>
                                                        
                                                        <div class="content">
                                                            <div class="text-center">
                                                                <div class="date">
                                                                    <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
                                                                        <?php echo get_the_date('M j, Y'); ?>
                                                                    </time>
                                                                </div>

                                                                <header>
                                                                    <h2><?php the_title(); ?></h2>
                                                                </header>

                                                                <div class="cta">
                                                                    <p class="mb-0">
                                                                        <span>Read Article</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>

                                                </div>
                                            </div>
                                        <?php

                                    endwhile; // End of the loop.

                                else :
                                    ?>
                                        <div class="col-lg-12">
                                            <p>No news available at the moment.</p>
                                        </div>
                                    <?php
                                endif;

                                // Restore original Post Data
                                wp_reset_postdata();
                            ?>
                        </div>

                        <div class="pagination">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <?php
                                        // custom navigation
                                        $big            = 999999999; 		// need an unlikely integer
                                        
                                        if( $sticky && is_array( $sticky ) ) {
                                            $total_pages = $news_query->max_num_pages;
                                        }
                                        else {
                                            $total_rows     = max( 0, $news_query->found_posts - $offset_start );
                                            $total_pages    = ceil( $total_rows / $posts_per_page );
                                        }
                                        
                                        // check if we have enough posts
                                        if ( $total_pages > 1 ) {
                                            $current_page = max( 1, get_query_var('paged') );
                                            $nav_args = array(
                                                'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                                'format' 	=> '/page/%#%',
                                                'current' 	=> $current_page,
                                                'total'		=> $total_pages,
                                                'show_all'	=> false,
                                                'prev_text'	=> 'Prev',
                                                'next_text'	=> 'Next',
                                                'type' 		=> 'list'
                                            );
                                            
                                            echo paginate_links( $nav_args );
                                        }
                                    ?>

                                </div>
                            </div>
                        </div>

                    </div> <!-- .container -->
                
                </div>

            </div> <!-- .news -->

        </div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();