<?php
    $randon_key = mt_rand();
    
    $heading    = get_sub_field('heading');
    $fontsize   = get_sub_field('fontsize');
    $color      = get_sub_field('color');

    $id         = get_sub_field('id');
    $class      = get_sub_field('class');
?>

<style>
    
</style>

<section class="flex-heading flex-heading-<?php echo $randon_key; ?> <?php echo $class; ?>" id="<?php echo $id; ?>">
    
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-8">
                
                <header>
                    <h2>
                        <?php echo $heading; ?>
                    </h2>
                </header>
                
            </div>
        </div>
    </div>

</section>