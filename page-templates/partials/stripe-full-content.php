<?php
    $random_key = mt_rand();

    $content    = get_sub_field('content');
    $id         = get_sub_field('id');
    $class      = get_sub_field('class');
?>

<section class="full-content full-content-<?php echo $random_key; ?> <?php echo $class; ?>" id="<?php echo $id; ?>">
    
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-8">
                
                <article <?php post_class(); ?> id="id-<?php the_ID(); ?>">
                    <?php echo wpautop( $content ); ?>
                </article>

            </div>
        </div>
    </div>

</section>