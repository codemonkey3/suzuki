<?php
    $random_key = mt_rand();

    $id         = get_sub_field('id');
    $class      = get_sub_field('class');
?>

<section class="flex-gallery flex-gallery-<?php echo $random_key; ?> <?php echo $class; ?>" id="<?php echo $id; ?>">
    
    <div class="flex-gallery__items">
        <?php 
            $images = get_sub_field('field_5dea6010e331d');
            $size   = 'full'; // (thumbnail, medium, large, full or custom size)
            
            if( $images ) :
                foreach( $images as $image ) :
                
                    // var_dump( $image_id );
                    
                    ?>
                        <div class="flex-gallery__item">
                            <?php
                                echo wp_get_attachment_image( $image['ID'], $size );
                            ?>
                        </div>
                    <?php

                endforeach; 
            endif;
        ?>
    </div>
    
    <div class="flex-gallery__meta">
        <!--span class="label">
            Gallery
        </span-->
        
        <div class="count">
            <div class="media justify-content-center align-items-center">
                <span>
                    <?php echo count( $images ); ?>
                </span>
                <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M22 18.8516V2.85156H6V18.8516H22ZM11 12.8516L13.03 15.5616L16 11.8516L20 16.8516H8L11 12.8516ZM2 22.8516V6.85156H4V20.8516H18V22.8516H2Z" fill="white"/>
                </svg>
            </div>
        </div>
    </div>
    
</section>