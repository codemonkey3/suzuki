<?php
/**
 * Template Name: Find a Dealer
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

// https://developers.google.com/maps/solutions/store-locator/clothing-store-locator

get_header(); ?>
	
	<main id="main" class="site-main dealer m-0" role="main">
        <div id="primary" class="content-area">
            
            <div class="dealer__filter">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <header class="page-header">
                                <h1 class="page-title"><?php the_title() ?></h1>
                            </header>

                            <form id="dealerForm" method="post">
                                <?php wp_nonce_field( 'dealer_form_action', 'dealer_form_nonce' ); ?>
                                
                                <p>
                                    <input type="text" id="input_dealer_search" class="input-address" name="input_dealer_search" placeholder="Search for area, dealer, or dealer name" />
                                </p>
                                
                                <?php
                                    $terms = get_terms( array(
                                        'taxonomy'      => 'dealer-cat',
                                        'hide_empty'    => false,
                                    ) );
                                    
                                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
                                        ?>
                                        <ul class="list-unstyled row">
                                            <?php foreach ( $terms as $term ) { ?>
                                                <li>
                                                    <label class="mb-0">
                                                        <div class="d-flex align-items-center">
                                                            <input name="dealer_cat" type="checkbox" value="<?php echo $term->term_id; ?>"> 
                                                            <span><?php echo $term->name; ?></span>
                                                        </div>
                                                    </label>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <?php
                                    }
                                ?>
                                
                                <div class="result-count">
                                    <p class="mb-0"><span class="count">0</span> dealers found</p>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
            
            <div class="dealer__map d-flex flex-wrap">
                <div class="dealers">        
                    <div id="location_list" class="location-list">
                        <?php
                            $dealer_infos   = [];
                            $args           = [
                                'post_type'         => 'dealer',
                                'posts_per_page'    => -1
                            ];

                            // The Query
                            $the_query = new WP_Query( $args );
                            
                            // The Loop
                            if ( $the_query->have_posts() ) {

                                // set our dealer counter
                                $dealer_counter = 0;
                                
                                while ( $the_query->have_posts() ) { $the_query->the_post();
                                    $lat            = get_field('field_5e0b4cb79a038');
                                    $lng            = get_field('field_5e0b4cce9a039');
                                    $address        = get_field('field_5e0b86315a51b');
                                    $tel            = get_field('field_5e0b1827f613f');
                                    $email          = get_field('field_5e0b183cf6140');
                                    $website        = get_field('field_5ed90a4df9a48');
                                    $opening_hours  = get_field('field_5e0b1855f6142');

                                    // user save lat and lng
                                    $user_latlng    = explode( ',', $_COOKIE['user-latlng'] );
                                    $distance_meter = haversine_distance( $lat, $lng, $user_latlng[0], $user_latlng[1] );
                                    $distance       = number_format( ( $distance_meter / 1000 ), 2 );
                                    ?>
                                        <div class="item" data-id="<?php echo $dealer_counter; ?>">
                                            <header>
                                                <h2 class="dealer-name"><?php the_title(); ?></h2>
                                            </header>
                                            <div class="services d-flex align-items-center justify-content-between">
                                                <div class="tags">
                                                    <?php
                                                        // check if the repeater field has rows of data
                                                        if( have_rows('field_5e0b17f0f613d') ):

                                                            // loop through the rows of data
                                                            while ( have_rows('field_5e0b17f0f613d') ) : the_row();
                                                            
                                                                // display a sub field value
                                                                $tag_name = get_sub_field('field_5e0b17fbf613e');

                                                                ?>
                                                                    <span><?php echo $tag_name; ?></span>
                                                                <?php
                                                            endwhile;

                                                        else :

                                                            // no rows found

                                                        endif;
                                                    ?>
                                                </div>
                                                <div class="distance">
                                                    <span>
                                                        <?php
                                                            if( $user_latlng ) {
                                                                echo $distance . 'KM Away';
                                                            }
                                                            else {
                                                                echo '0KM Away';
                                                            }
                                                        ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="address">
                                                <?php echo wpautop( $address ); ?>
                                            </div>
                                            <div class="dealer-info">
                                                <p>
                                                    <?php if( $tel ) { ?>
                                                        <span>Tel:</span> <?php echo $tel; ?><br/>
                                                    <?php } ?>
                                                    <?php if( $email ) { ?>
                                                        <span>Email:</span> <?php echo $email; ?><br/>
                                                    <?php } ?>
                                                    <?php if( $website ) { ?>
                                                        <span>Website:</span> <a href="<?php echo esc_url($website); ?>"><?php echo $website; ?></a>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                            <div class="opening-hours">
                                                <h3>Opening Hours</h3>
                                                <?php echo $opening_hours ? wpautop( $opening_hours ) : ''; ?>
                                            </div>
                                            
                                            <div class="directions">
                                                <p><a href="https://www.google.com/maps/dir/<?php echo $_COOKIE['user-latlng']; ?>/<?php echo $address; ?>" target="_blank">Get Directions</a></p>
                                            </div>
                                        </div>
                                    <?php

                                    // set our dealer counter
                                    $dealer_counter++;
                                }
                                
                            }
                            else {
                                // no posts found
                            }

                            /* Restore original Post Data */
                            wp_reset_postdata();
                        ?>
                    </div>
                </div>
                
                <div class="interactive-map">
                    
                    <!--load Google map-->
                    <div id="map"></div>
                    
                </div>
            </div> <!-- .dealer__map -->
            
        </div><!-- #primary -->
	</main><!-- #main -->
    
<?php
get_footer();