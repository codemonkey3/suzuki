<?php
/**
 * Template Name: Promos
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header();

// get ACF settings
$subheading = get_field('field_5deb75465bfed');
?>
	
	<main id="main" class="site-main" role="main">

        <div id="primary" class="content-area">
            <div class="container">
                
                <header class="entry-header text-center">
                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                </header><!-- .entry-header -->

                <div class="promo">
                    <div class="row">
                        <?php
                            $args = [
                                'post_type'         => 'promo',
                                'posts_per_page'    => -1,
                                'orderby'           => 'menu_order title',
                                'order'             => 'ASC',
                            ];
                            
                            // The Query
                            $the_query = new WP_Query( $args );
                            
                            // The Loop
                            if ( $the_query->have_posts() ) {
                                
                                while ( $the_query->have_posts() ) { $the_query->the_post();
                                    
                                    $product_thumbnail  = get_field('field_5dec69b5b0129');
                                    $start_date         = get_field('field_5dec6947a6c6b');
                                    $end_date           = get_field('field_5dec6969a6c6c');

                                    ?>
                                        <div class="col-lg-6 col-xl-4">
                                            <div class="promo__item promo-card">
                                                
                                                <a href="<?php the_permalink(); ?>" class="d-block">
                                                    <div class="text-center">
                                                        <div class="product-thumbnail">
                                                            <figure class="mb-0">
                                                                <img src="<?php echo esc_url( $product_thumbnail['url'] ); ?>" alt="<?php echo $product_thumbnail['alt'] ?>" />
                                                            </figure> 
                                                        </div>

                                                       <div class="content">
                                                        <div class="promo-period">
                                                                <p class="mb-0">
                                                                    <span class="d-block d-md-inline">Promo Period:</span> <?php echo date('M. j', strtotime($start_date)); ?> - <?php echo date('M. j, Y', strtotime($end_date)); ?>
                                                                </p>
                                                            </div>

                                                            <header>
                                                                <h2><?php the_title(); ?></h2>
                                                            </header>

                                                            <div class="cta">
                                                                <p class="mb-0">
                                                                    <span>See Mechanics</span>
                                                                </p>
                                                            </div>
                                                       </div>
                                                    </div>
                                                </a>

                                            </div>
                                        </div>
                                    <?php
                                    
                                }
                                
                            }
                            else {
                                // no posts found
                            }

                            /* Restore original Post Data */
                            wp_reset_postdata();
                        ?>
                    </div>
                </div>
                
            </div> <!-- .container -->
            
            <div class="back-to-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="text-center">
                                <a href="#page" id="back_top" class="back-to-top__btn">
                                    <div class="media justify-content-center align-items-center">
                                        <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.07229 3.59468e-07L16.8486 8.17021L16.8486 15.5137L9.07229 7.82979L0.848635 16L0.848635 8.17021L9.07229 3.59468e-07Z" fill="#003B70"/>
                                        </svg>
                                        <span class="ml-3">Back to Top</span>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();