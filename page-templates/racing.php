<?php
/**
 * Template Name: Racing
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header(); ?>
	
	<main id="main" class="site-main" role="main">
        <div id="primary" class="content-area">

            <div class="racing">
                
                <div class="racing__latest">
                    
                    <?php
                        $latest_args = [
                            'post_type'         => 'post',
                            'category_name'     => 'racing',
                            'post__in'          => [get_latest_post_id()],
                        ];
                        
                        // The Query
                        $latest_query = new WP_Query( $latest_args );
                        
                        // The Loop
                        if ( $latest_query->have_posts() ) {
                            while ( $latest_query->have_posts() ) { $latest_query->the_post();

                                /* grab the url for the full size featured image */
                                $hero_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                                ?>
                                
                                <div class="hero" style="background-image: url(<?php echo $hero_url; ?>);">
                                
                                    <div class="container">
                                        <div class="row align-items-end">
                                            <div class="col-md-7">
                                                
                                                <p class="mb-0 racing-label">
                                                    Suzuki Racing
                                                </p>
                                                
                                                <header>
                                                    <h2 class="entry-title"><?php the_title(); ?></h2>
                                                </header>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>

                                <?php
                            }
                        }
                        else {
                            // no posts found
                            _e( 'No latest racing', 'suzuki' );
                        }
                        
                        // Restore original Post Data
                        wp_reset_postdata();
                    ?>

                </div>

                <div class="racing__filter">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                    // get GET racing filter
                                    $current_filter  = $_GET['filter'] ?? '';
                                ?>
                                
                                <ul class="m-0 list-unstyled d-flex flex-wrap justify-content-center justify-content-lg-end">
                                    <li>
                                        <label class="mb-0 media align-items-center">
                                            <input type="radio" name="racing-filter" value="local" <?php checked($current_filter, 'local'); ?> /> 
                                            <span class="ml-2 text-uppercase">Local</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="mb-0 media align-items-center">
                                            <input type="radio" name="racing-filter" value="international" <?php checked($current_filter, 'international'); ?> /> 
                                            <span class="ml-2 text-uppercase">International</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="mb-0 media align-items-center">
                                            <input type="radio" name="racing-filter" value="" <?php checked($current_filter, ''); ?> /> 
                                            <span class="ml-2 text-uppercase">View All</span>
                                        </label>
                                    </li>
                                </ul>
                                
                            </div>
                        </div>
                    </div>

                </div>
                
                <div class="racing__items">
                    
                    <div class="container">
                        <div class="row">
                            
                            <?php
                                $paged          = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                                
                                if( wp_is_mobile() ) {
                                    $posts_per_page = 6;
                                }
                                else {
                                    $posts_per_page = 9;
                                }
                                
                                $racing_args = [
                                    'post_type'             => 'post',
                                    'category_name'         => 'racing',
                                    'post__not_in'          => [get_latest_post_id()],
                                    'posts_per_page'        => $posts_per_page,
                                    'paged'                 => $paged
                                ];
                                
                                if( $current_filter ) {
                                    $racing_args['meta_query'] = [
                                        [
                                            'key'     => 'type',
                                            'value'   => $current_filter,
                                        ]
                                    ];
                                }
                                
                                // The Query
                                $racing_query = new WP_Query( $racing_args );
                                
                                // The Loop
                                if ( $racing_query->have_posts() ) :
                                    
                                    while ( $racing_query->have_posts() ) : $racing_query->the_post();
                                        
                                        ?>
                                            <div class="col-lg-6 col-xl-4">
                                                <div class="racing__item news-card">

                                                    <a href="<?php the_permalink(); ?>">
                                                        <figure class="mb-0">
                                                            <?php
                                                                if ( has_post_thumbnail() ) :
                                                                    the_post_thumbnail();
                                                                endif;
                                                            ?>
                                                        </figure>
                                                        
                                                        <div class="content">
                                                            <div class="text-center">
                                                                <div class="date">
                                                                    <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
                                                                        <?php echo get_the_date('M j, Y'); ?>
                                                                    </time>
                                                                </div>

                                                                <header>
                                                                    <h2><?php the_title(); ?></h2>
                                                                </header>

                                                                <div class="cta">
                                                                    <p class="mb-0">
                                                                        <span>Read Article</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>

                                                </div>
                                            </div>
                                        <?php

                                    endwhile; // End of the loop.

                                else :
                                    ?>
                                        <div class="col-lg-12">
                                            <p>No racing available at the moment.</p>
                                        </div>
                                    <?php
                                endif;
                                
                                // Restore original Post Data
                                wp_reset_postdata();
                            ?>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                
                                <?php
                                    // custom navigation
                                    $big            = 999999999; 		// need an unlikely integer
                                    $total_pages    = $racing_query->max_num_pages;
                                    
                                    // check if we have enough posts
                                    if ( $total_pages > 1 ) {
                                        $current_page = max( 1, get_query_var('paged') );
                                        $nav_args = array(
                                            'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                            'format' 	=> '/page/%#%',
                                            'current' 	=> $current_page,
                                            'total'		=> $total_pages,
                                            'show_all'	=> false,
                                            'prev_text'	=> 'Prev',
                                            'next_text'	=> 'Next',
                                            'type' 		=> 'list'
                                        );
                                        
                                        echo paginate_links( $nav_args );
                                    }
                                ?>

                            </div>
                        </div>
                        
                    </div> <!-- .container -->
                
                </div>

            </div> <!-- .racing -->

        </div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();