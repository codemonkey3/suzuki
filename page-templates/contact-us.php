<?php
/**
 * Template Name: Contact Us
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header();

// get ACF value for later use
$hero_background 			= get_field('field_5deba200b754d');
$hero_heading 				= get_field('field_5deba1cb13cd1');
$hero_product_thumbnail 	= get_field('field_5deba21eb754e');

$contact_info_heading		= get_field('field_5deb9679fadc4');
$contact_info_address		= get_field('field_5deb970ffadc5');
$contact_info_locations		= get_field('field_5deb9a1eb39c3');

$contact_form_heading		= get_field('field_5deb9a3f2bd8b');
$contact_form_shortcode		= get_field('field_5deb9a582bd8c');
?>
	<div class="hero">
		<div class="hero-wrap" style="background-image: url(<?php echo $hero_background['url']; ?>)">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-6">
						<header class="text-center text-sm-left">
							<h1><?php echo $hero_heading; ?></h1>	
						</header>
					</div>
				</div>	
			</div>
		</div>
		
		<figure class="ml-0 product-thumbnail text-center pr-4 pr-sm-0 pl-4 pl-sm-0">
			<img src="<?php echo $hero_product_thumbnail['url']; ?>" alt="<?php echo $hero_product_thumbnail['alt']; ?>" />
		</figure>
	</div>
	
	<main id="main" class="site-main m-0" role="main">
		<div id="primary" class="content-area">
			<div class="container">
				
				<div class="row justify-content-center flex-sm-row-reverse">
					<div class="offset-lg-1 offset-xl-1 col-lg-6 col-xl-7">
						
						<div class="form-wrap">
							<div class="form-wrap__heading d-none d-lg-block">
								<?php echo wpautop( $contact_form_heading ); ?>
							</div>
							
							<div class="form">
								<?php echo do_shortcode($contact_form_shortcode); ?>
							</div>
						</div>
						
					</div>

					<div class="col-lg-5 col-xl-4">
						
						<div class="contact-info">
							<header>
								<h2><?php echo $contact_info_heading; ?></h2>
							</header>
							<div class="address">
								<?php echo wpautop( $contact_info_address ); ?>
							</div>

							<div class="locations">
								<?php echo wpautop( $contact_info_locations ); ?>
							</div>
						</div>

					</div>
				</div>

			</div> <!-- .container -->
		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();