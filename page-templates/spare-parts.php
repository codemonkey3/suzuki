<?php
/**
 * Template Name: Spare Parts
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header();

// get ACF settings
$subheading = get_field('field_5deb75465bfed');
?>
	
	<main id="main" class="site-main" role="main">
        
        <header class="entry-header text-center">
            <h2>
                <?php echo $subheading ?? 'Suzuki Guaranted'; ?>
            </h2>
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->

        <div class="spare-part-wrap">
            <div class="container">
                
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        
                        <div id="primary" class="content-area">

                            <div class="spare-part">
                                <div class="row">
                                    <?php
                                        $args = [
                                            'post_type'         => 'spare-part',
                                            'posts_per_page'    => -1,
                                        ];
                                        
                                        // The Query
                                        $the_query = new WP_Query( $args );
                                        
                                        // The Loop
                                        if ( $the_query->have_posts() ) {
                                            
                                            while ( $the_query->have_posts() ) { $the_query->the_post();
                                                
                                                $product_thumbnail  = get_field('field_5deb4a97b057e');
                                                $price              = get_field('field_5deb4aa1b057f');

                                                ?>
                                                    <div class="col-lg-6 col-xl-4">
                                                        <div class="spare-part__item product-card">

                                                            <div class="text-center">
                                                                <div class="product-thumbnail">
                                                                    <figure>
                                                                        <img src="<?php echo esc_url( $product_thumbnail['url'] ); ?>" alt="<?php echo $product_thumbnail['alt'] ?>" />
                                                                    </figure> 
                                                                </div>

                                                                <header>
                                                                    <h2><?php the_title(); ?></h2>
                                                                </header>

                                                                <?php if( $price ) : ?>
                                                                    <div class="price">
                                                                        <p class="mb-0">
                                                                            <span class="price__amount">
                                                                                PHP <?php echo number_format( $price, 2 ); ?>
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                <?php endif; ?>
                                                                
                                                                <div class="excerpt">
                                                                    <?php the_content(); ?>
                                                                </div>

                                                                <div class="cta">
                                                                    <p class="mb-0">
                                                                        <a href="<?php the_permalink(); ?>">
                                                                            View Item
                                                                        </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                <?php
                                                
                                            }
                                            
                                        }
                                        else {
                                            // no posts found
                                        }

                                        /* Restore original Post Data */
                                        wp_reset_postdata();
                                    ?>
                                </div>
                            </div>

                        </div><!-- #primary -->

                    </div>
                </div>

            </div> <!-- .container -->
        </div>
	</main><!-- #main -->

<?php
get_footer();