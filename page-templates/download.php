<?php
/**
 * Template Name: Download
 * 
 * The template for displaying full width pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package suzuki
 */

get_header();
?>
    
    <header class="d-none">
        <h1><?php the_title(); ?></h1>
    </header>

	<main id="main" class="site-main" role="main">
		<div id="primary" class="content-area">

			<div class="motorcycles">
				<div class="motorcycles__filter">

					<div class="container">

                        <div class="filter-inner">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <header>
                                        <h2>Motorcycles</h2>
                                    </header>
                                </div>
                                <div class="col-lg-8">
                                    <?php
                                        // get product category list
                                        $temp_motorcycle_terms = get_terms( array(
                                            'taxonomy'      => 'product-cat',
                                            'hide_empty'    => false,
                                        ) );  

                                        if ( ! empty( $temp_motorcycle_terms ) && ! is_wp_error( $temp_motorcycle_terms ) ) {
                                            $motorcycle_terms = [];
                                            
                                            // loop through the terms
                                            foreach ( $temp_motorcycle_terms as $motorcycle_term ) {
                                                $order = get_field( 'order', $motorcycle_term );

                                                $motorcycle_terms[$order] = $motorcycle_term;
                                            }
                                            
                                            // order product-category
                                            ksort( $motorcycle_terms );
                                        }
                                        
                                        // loop through all the ordered terms
                                        if( $motorcycle_terms ) {
                                            ?>
                                            <ul class="m-0 list-unstyled d-flex flex-wrap justify-content-start justify-content-lg-end">
                                                <li>
                                                    <label class="mb-0 media align-items-center">
                                                        <input type="radio" name="motorcycle_filter" value="*" checked>
                                                        <span class="ml-2 text-uppercase">All Types</span>
                                                    </label>
                                                </li>
                                                
                                                <?php
                                                    foreach( $motorcycle_terms as $motorcycle_term ) {
                                                        ?>
                                                            <li>
                                                                <label class="mb-0 media align-items-center">
                                                                    <input type="radio" name="motorcycle_filter" value="<?php echo $motorcycle_term->term_id; ?>">
                                                                    <span class="ml-2 text-uppercase"><?php echo $motorcycle_term->name; ?></span>
                                                                </label>
                                                            </li>
                                                        <?php
                                                    }
                                                ?>
                                            </ul>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>

					</div>
				</div>

				<div class="motorcycles__items">
                    <div class="container">
                        <?php
                            // loop through all the ordered terms
                            if( $motorcycle_terms ) {
                                foreach( $motorcycle_terms as $motorcycle_term ) {
                                    $args = [
                                        'post_type' => 'product',
                                        'tax_query' => [
                                            [
                                                'taxonomy'  => 'product-cat',
                                                'field'     => 'term_id',
                                                'terms'     => $motorcycle_term->term_id
                                            ]
                                        ],
                                        'orderby' => 'menu_order title',
                                        'order'   => 'ASC',
                                    ];
                                    
                                    // The Query
                                    $the_query = new WP_Query( $args );
                                    
                                    // The Loop
                                    if ( $the_query->have_posts() ) {
                                        while ( $the_query->have_posts() ) { $the_query->the_post();
                                            $product_thumbnail  = get_field('field_5df382a2fa7ec');
                                            $excerpt            = get_field('field_5df37d1dc71de'); 
                                            $overview_brochure  = get_field('field_5dee3e1c52d49');
        
                                            $terms = get_the_terms( get_the_ID() , 'product-cat' );
                                            if( $terms ) {
                                                foreach( $terms as $term ) {
                                                    $term_id = $term->term_id;
                                                }
                                            }

                                            ?>
                                                <div class="item category-<?php echo $term_id; ?>">
                                            
                                                    <div class="row align-items-center">
                                                        <div class="offset-lg-1 col-lg-4 order-2 order-lg-1">
                                                            <div class="product-thumbnail">
                                                                <figure>
                                                                    <img src="<?php echo esc_url( $product_thumbnail['url'] ); ?>" alt="<?php echo esc_attr( $product_thumbnail['alt'] ); ?>">
                                                                </figure>
                                                            </div>
                                                        </div>
                                                        <div class="offset-lg-1 col-lg-5 order-1  order-lg-2">
                                                            <div class="product-details text-center text-lg-left">
                                                                <header>
                                                                    <h3><?php the_title(); ?></h3>
                                                                </header>

                                                                <div class="excerpt">
                                                                    <?php echo wpautop( $excerpt ); ?>
                                                                </div>

                                                                <div class="media justify-content-center justify-content-lg-start">
                                                                    <?php if( $overview_brochure ) : ?>
                                                                        <div class="brochure-download mr-4 mr-md-5">
                                                                            <a class="brochure-btn text-uppercase" href="<?php echo $overview_brochure['url'] ?>" download>
                                                                                <div class="media align-items-center text-left">
                                                                                    <div class="icon mr-2">
                                                                                        <svg width="36" height="35" viewBox="0 0 36 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M25.1672 12.3959H35.1882L17.6514 26.6875L0.114502 12.3959H10.1356V0.145874H25.1672V12.3959ZM0.114502 34.8542V30.7709H35.1882V34.8542H0.114502Z" fill="#007BBB"/>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <span>Download <span class="d-block">Brochure</span>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="model-review">
                                                                        <?php
                                                                            $video_url = '';

                                                                            if( have_rows('field_5dfb031ce92a1') ) :
                                                                                $video_counter = 0;

                                                                                // loop through the rows of data
                                                                                while ( have_rows('field_5dfb031ce92a1') ) : the_row(); 
                                                                                    $video_url  = get_sub_field('field_5dfb0424e92a4');

                                                                                    if( $video_url ) {
                                                                                        break;
                                                                                    }
                                                                                    
                                                                                    // increment counter
                                                                                    $video_counter++;
                                                                                endwhile;
                                                                            endif;
                                                                        ?>

                                                                        <?php if( $video_url ) { ?>
                                                                            <a class="text-uppercase" href="<?php echo $video_url; ?>" target="_blank">
                                                                                <div class="media align-items-center text-left">
                                                                                    <div class="icon mr-2">
                                                                                        <svg width="41" height="39" viewBox="0 0 41 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                            <g style="mix-blend-mode:multiply" opacity="0.8">
                                                                                            <ellipse cx="20.4123" cy="19.5" rx="19.8723" ry="19.5" fill="#007BBB"/>
                                                                                            </g>
                                                                                            <path d="M26.4136 18.4501C27 18.8466 27 19.7103 26.4136 20.1068L18.6981 25.3239C18.034 25.773 17.138 25.2972 17.138 24.4955L17.138 14.0614C17.138 13.2597 18.034 12.7839 18.6981 13.233L26.4136 18.4501Z" fill="white"/>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <span>
                                                                                        Watch Model <span class="d-block">Review</span>
                                                                                    </span>
                                                                                </div>
                                                                            </a>
                                                                        <?php } ?>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        
                                                </div>
                                            <?php
                                        }
                                    }
                                    else {
                                        /* ?>
                                            <div class="item">
                                            
                                                <div class="row justify-content-center">
                                                    <div class="col-lg-10">
                                                        <p>No product under this category.</p>
                                                    </div>
                                                </div>

                                            </div>
                                        <?php */
                                    }
                                }
                            }

                            /* Restore original Post Data */
                            wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div> <!-- .motorcycle -->

            
            <div class="wallpaper">
                
                <div class="wallpaper__filter">
                    
                    <div class="container">

                        <div class="filter-inner">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <header>
                                        <h2>Suzuki Wallpapers</h2>
                                    </header>
                                </div>
                                <div class="col-lg-8">
                                    <?php
                                        // get product category list
                                        $temp_wallpaper_terms = get_terms( array(
                                            'taxonomy'      => 'wallpaper-cat',
                                            'hide_empty'    => false,
                                        ) );  
                                        
                                        if ( ! empty( $temp_wallpaper_terms ) && ! is_wp_error( $temp_wallpaper_terms ) ) {
                                            $wallpaper_terms = [];

                                            // loop through the terms
                                            foreach ( $temp_wallpaper_terms as $temp_wallpaper_term ) {
                                                $order = get_field( 'order', $temp_wallpaper_term );
                                                if( $order ) {
                                                    $order = $order;
                                                }
                                                else {
                                                    $order = $temp_wallpaper_term->term_id;
                                                }

                                                $wallpaper_terms[$order] = $temp_wallpaper_term;
                                            }
                                            
                                            // order product-category
                                            ksort( $wallpaper_terms );
                                        }

                                        // loop through all the ordered terms
                                        if( $wallpaper_terms ) {
                                            ?>
                                            <ul class="m-0 list-unstyled d-flex flex-wrap justify-content-center justify-content-lg-end">
                                                <li>
                                                    <label class="mb-0 media align-items-center">
                                                        <input type="radio" name="wallpaper_filter" value="*" checked>
                                                        <span class="ml-2 text-uppercase">View All</span>
                                                    </label>
                                                </li>

                                                <?php
                                                    foreach( $wallpaper_terms as $wallpaper_term ) {
                                                        ?>
                                                        <li>
                                                            <label class="media align-items-center">
                                                                <input type="radio" name="wallpaper_filter" value="<?php echo $wallpaper_term->term_id; ?>">
                                                                <span class="ml-2 text-uppercase">For <?php echo $wallpaper_term->name; ?></span>
                                                            </label>
                                                            </li>
                                                        <?php
                                                    }
                                                ?>
                                            </ul>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!--.wallpaper__filter-->

                <div class="wallpaper__items">
                    <div class="container">
                        <?php
                            $wallpaper_args = [
                                'post_type'         => 'wallpaper',
                                'orderby'           => 'menu_order title',
                                'order'             => 'ASC',
                                'posts_per_page'    => -1,
                            ];
                            
                            // The Query
                            $wallpaper_query = new WP_Query( $wallpaper_args );
                            
                            // The Loop
                            if ( $wallpaper_query->have_posts() ) {

                                ?>
                                <div class="row">
                                    <?php
                                        while ( $wallpaper_query->have_posts() ) { $wallpaper_query->the_post();
                                            $wallpaper = get_field('field_5ec3c68379ce6');

                                            $terms = get_the_terms( get_the_ID() , 'wallpaper-cat' );

                                            if( $terms ) {
                                                foreach( $terms as $term ) {
                                                    $term_id = $term->term_id;
                                                }
                                            }
                                            ?>

                                            <div class="col-lg-4 item category-<?php echo $term_id; ?>">
                                                <div class="item-inner">
                                                    <?php
                                                        if( $wallpaper ) {
                                                            $wallpaper_url = $wallpaper['sizes']['medium_large'];
                                                        }
                                                        else {
                                                            $wallpaper_url = get_bloginfo('template_directory') . 'images/image-placeholder.jpg';
                                                        }
                                                    ?>
                                                    <figure class="mb-1 wallpaper-thumbnail">
                                                        <img src="<?php echo esc_url( $wallpaper_url ); ?>" alt="<?php echo $wallpaper['alt']; ?>">
                                                    </figure>
                                                    
                                                    <div class="download-btn text-center">
                                                        <a class="text-uppercase" href="<?php echo esc_url( $wallpaper['url'] ); ?>" download>
                                                            <div class="media align-items-center justify-content-center">
                                                                <div class="icon">
                                                                    <svg width="10" height="13" viewBox="0 0 10 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.91053 4.65963H9.28666L5.12844 9.56673L0.970215 4.65963H3.34634V0.453552H6.91053V4.65963ZM0.970215 12.3708V10.9688H9.28666V12.3708H0.970215Z" fill="#007BBB"/>
                                                                    </svg>
                                                                </div>
                                                                <span class="ml-2">Download Wallpaper</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                    </div>
                                <?php
                                
                            }
                            else {
                                // no posts found
                            }

                            /* Restore original Post Data */
                            wp_reset_postdata();
                        ?>
                    </div>
                </div> <!--.motorcycles__items-->

            </div> <!--.wallpaper-->

            
            <div class="jingles">
                
                <div class="jingles__filter">

                    <div class="container">

                        <div class="filter-inner">
                            <div class="row">
                                <div class="col-lg-12">
                                    <header>
                                        <h2>Suzuki Jingles</h2>
                                    </header>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!--.jingles__filter-->

                <div class="jingles__items">
                    <div class="container">
                        <?php
                            $jingle_args = [
                                'post_type'         => 'jingle',
                                'posts_per_page'    => -1,
                                'orderby'           => 'menu_order title',
                                'order'             => 'ASC',
                            ];

                            // The Query
                            $jingle_query = new WP_Query( $jingle_args );
                            
                            // The Loop
                            if ( $jingle_query->have_posts() ) {

                                // set jingle counter
                                $jingle_counter = 0;
                                ?>
                                <div class="row">
                                    <?php
                                        while ( $jingle_query->have_posts() ) { $jingle_query->the_post();
                                            $audio_ogg  = get_field('field_5ec8758e91dab');
                                            $audio_mp3  = get_field('field_5ec3ba49d889a');
                                            $duration   = get_field('field_5ec3ba2ad8899');                                            
                                            ?>
                                            <div class="col-lg-12">
                                                <div class="item">
                                                    
                                                    <div class="item__inner">
                                                        <div class="row align-items-center">
                                                            <div class="col-lg-7">
                                                                
                                                                <div class="audio-details">
                                                                    <div class="media align-items-lg-center">
                                                                        <div class="controls">
                                                                            <a class="play-btn" href="javascript: void(0);" data-id="<?php echo $jingle_counter; ?>">
                                                                                <svg width="35" height="40" viewBox="0 0 35 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path d="M35 20L0.499998 39.9186L0.5 0.0814137L35 20Z" fill="white"/>
                                                                                </svg>
                                                                            </a>

                                                                            <a class="pause-btn d-none" href="javascript: void(0);" data-id="<?php echo $jingle_counter; ?>">
                                                                                <svg width="26" height="35" viewBox="0 0 26 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                    <line x1="3.07935" y1="0.15625" x2="3.07934" y2="34.6094" stroke="white" stroke-width="5"/>
                                                                                    <line x1="22.8931" y1="0.15625" x2="22.8931" y2="34.6094" stroke="white" stroke-width="5"/>
                                                                                </svg>
                                                                            </a>
                                                                            
                                                                            <audio id="jingle_audio_<?php echo $jingle_counter; ?>">
                                                                                <source src="<?php echo esc_url( $audio_ogg['url'] ); ?>" type="audio/ogg">
                                                                                <source src="<?php echo esc_url( $audio_mp3['url'] ); ?>" type="audio/mpeg">
                                                                                Your browser does not support the audio element.
                                                                            </audio>
                                                                        </div>
                                                                        <div class="jingle-info">
                                                                            <div class="mb-2 term text-uppercase d-none d-lg-block">
                                                                                <?php
                                                                                    $terms = get_the_terms( get_the_ID() , 'jingle-cat' );

                                                                                    if( $terms ) {
                                                                                        foreach( $terms as $term ) {
                                                                                            echo '<span>'. $term->name .'</span>';
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </div>
                                                                            <header>
                                                                                <h3><?php the_title(); ?></h3>
                                                                            </header>
                                                                        </div>

                                                                        <div class="duration d-block d-lg-none">
                                                                            <div class="media align-items-center justify-content-end">
                                                                                <span class="value mr-2"><?php echo $duration; ?></span>
                                                                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M5.33346 2.66661V-6.10352e-05H6.00012C9.31346 -6.10352e-05 12.0001 2.68661 12.0001 5.99994C12.0001 9.31327 9.31346 11.9999 6.00012 11.9999C2.68012 11.9999 0.00012207 9.31327 0.00012207 5.99994C0.00012207 4.03327 0.946789 2.29994 2.40679 1.20661V1.19327L6.94012 5.72661L6.00012 6.66661L2.38679 3.05327C1.72679 3.85327 1.33346 4.87994 1.33346 5.99994C1.33346 8.57994 3.42012 10.6666 6.00012 10.6666C8.58012 10.6666 10.6668 8.57994 10.6668 5.99994C10.6668 3.64661 8.92679 1.71327 6.66679 1.38661V2.66661H5.33346ZM6.00018 9.99991C5.63351 9.99991 5.33351 9.69991 5.33351 9.33324C5.33351 8.96657 5.63351 8.66657 6.00018 8.66657C6.36685 8.66657 6.66685 8.96657 6.66685 9.33324C6.66685 9.69991 6.36685 9.99991 6.00018 9.99991ZM10.0001 5.99989C10.0001 5.63322 9.70009 5.33322 9.33342 5.33322C8.96676 5.33322 8.66676 5.63322 8.66676 5.99989C8.66676 6.36656 8.96676 6.66656 9.33342 6.66656C9.70009 6.66656 10.0001 6.36656 10.0001 5.99989ZM2.66694 6.66656C2.30027 6.66656 2.00027 6.36656 2.00027 5.99989C2.00027 5.63322 2.30027 5.33322 2.66694 5.33322C3.03361 5.33322 3.33361 5.63322 3.33361 5.99989C3.33361 6.36656 3.03361 6.66656 2.66694 6.66656Z" fill="white"/>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-lg-5 d-none d-lg-block">
                                                                <div class="audio-meta">
                                                                    <div class="duration">
                                                                        <div class="media align-items-center justify-content-end">
                                                                            <span class="value mr-2"><?php echo $duration; ?></span>
                                                                            <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M5.33346 2.66661V-6.10352e-05H6.00012C9.31346 -6.10352e-05 12.0001 2.68661 12.0001 5.99994C12.0001 9.31327 9.31346 11.9999 6.00012 11.9999C2.68012 11.9999 0.00012207 9.31327 0.00012207 5.99994C0.00012207 4.03327 0.946789 2.29994 2.40679 1.20661V1.19327L6.94012 5.72661L6.00012 6.66661L2.38679 3.05327C1.72679 3.85327 1.33346 4.87994 1.33346 5.99994C1.33346 8.57994 3.42012 10.6666 6.00012 10.6666C8.58012 10.6666 10.6668 8.57994 10.6668 5.99994C10.6668 3.64661 8.92679 1.71327 6.66679 1.38661V2.66661H5.33346ZM6.00018 9.99991C5.63351 9.99991 5.33351 9.69991 5.33351 9.33324C5.33351 8.96657 5.63351 8.66657 6.00018 8.66657C6.36685 8.66657 6.66685 8.96657 6.66685 9.33324C6.66685 9.69991 6.36685 9.99991 6.00018 9.99991ZM10.0001 5.99989C10.0001 5.63322 9.70009 5.33322 9.33342 5.33322C8.96676 5.33322 8.66676 5.63322 8.66676 5.99989C8.66676 6.36656 8.96676 6.66656 9.33342 6.66656C9.70009 6.66656 10.0001 6.36656 10.0001 5.99989ZM2.66694 6.66656C2.30027 6.66656 2.00027 6.36656 2.00027 5.99989C2.00027 5.63322 2.30027 5.33322 2.66694 5.33322C3.03361 5.33322 3.33361 5.63322 3.33361 5.99989C3.33361 6.36656 3.03361 6.66656 2.66694 6.66656Z" fill="white"/>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="sound-wave" id="sound_wave_<?php echo $jingle_counter; ?>"></div>
                                                                </div>
                                                            </div>
                                                        </div> <!--.row-->
                                                    </div>
                                                    
                                                    <div class="download-btn">
                                                        <?php
                                                            $download_href = $audio_ogg['url'] ?? $audio_mp3['url'];
                                                        ?>
                                                    
                                                        <a class="text-uppercase" href="<?php echo esc_url( $download_href ); ?>" download>
                                                            <div class="media align-items-center justify-content-center justify-content-lg-end">
                                                                <div class="icon">
                                                                    <svg width="10" height="13" viewBox="0 0 10 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.91053 4.65963H9.28666L5.12844 9.56673L0.970215 4.65963H3.34634V0.453552H6.91053V4.65963ZM0.970215 12.3708V10.9688H9.28666V12.3708H0.970215Z" fill="#007BBB"/>
                                                                    </svg>
                                                                </div>
                                                                <span class="ml-2">Download Jingle</span>
                                                            </div>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                            <?php

                                            // increment jingle counter
                                            $jingle_counter++;
                                        }
                                    ?>
                                    </div>
                                <?php
                                
                            }
                            else {
                                // no posts found
                            }

                            /* Restore original Post Data */
                            wp_reset_postdata();
                        ?>
                    </div>
                </div> <!--.jingles__items-->

            </div> <!--.jingles-->

            <div class="back-to-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="text-center">
                                <a href="#page" id="back_top" class="back-to-top__btn">
                                    <div class="media justify-content-center align-items-center">
                                        <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.07229 3.59468e-07L16.8486 8.17021L16.8486 15.5137L9.07229 7.82979L0.848635 16L0.848635 8.17021L9.07229 3.59468e-07Z" fill="#003B70"/>
                                        </svg>
                                        <span class="ml-3">Back to Top</span>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

		</div><!-- #primary -->
	</main><!-- #main -->

<?php
get_footer();