<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rs-theme
 */

get_header();

// get ACF value for later use

// Suzuki Motors
$suzuki_motors_heading 		= get_field( 'field_5de9c34000539' );
$suzuki_motors_subheading 	= get_field( 'field_5de9c3530053a' );

// Find a Dealer
$dealer_heading				= get_field( 'field_5de9c4af0f6a5' );
$lat 						= get_field( 'field_5de9c4bf0f6a6' );
$long 						= get_field( 'field_5df25814c3756' );
$dealer_cta					= get_field( 'field_5de9c4c80f6a7' );

// Set the Trend
$the_trend_type				= get_field( 'field_5ebe90e1222f6' );
$the_trend_hero 			= get_field( 'field_5df23a8a1f4a8' );
$the_trend_branding_logo 	= get_field( 'field_5de9c84e317ee' );
$the_trend_heading 			= get_field( 'field_5de9c864317ef' );
$the_trend_subheading 		= get_field( 'field_5de9c874317f0' );
$the_trend_prod_thumbnail 	= get_field( 'field_5de9c89c317f1' );

$promo_desktop				= get_field( 'field_5ebe9aeb325ad' );
$promo_tablet				= get_field( 'field_5ebe9b01325ae' );
$promo_phone				= get_field( 'field_5ebe9b0d325af' );

$the_trend_cta 				= get_field( 'field_5df23a2e93dba' );

// Video
$video_heading				= get_field( 'field_5df23b07e6ddd' );
?>
	
	<div class="hero slider">

		<div class="hero__items">
			<?php
				// check if the repeater field has rows of data
				if( have_rows('field_5de9c19ba3ad1') ):
					
					// loop through the rows of data
					while ( have_rows('field_5de9c19ba3ad1') ) : the_row();
						
						// display a sub field value
						$opacity_text_heading 	= get_sub_field('field_5eae326b227c8');
						$opacity_text 			= get_sub_field('field_5de9c1a5a3ad2');
						$hero 					= get_sub_field('field_5de9c23dfd594');
						$heading 				= get_sub_field('field_5de9c1eda3ad3');
						$logo 					= get_sub_field('field_5de9c1faa3ad4');
						$excerpt 				= get_sub_field('field_5de9c212a3ad5');
						$cta 					= get_sub_field('field_5de9c228a3ad6');

						?>
							<div class="hero__item">
								<div class="container">

									<p class="opacity-text mb-0">
										<?php echo "" != $opacity_text_heading ? '<span class="heading d-block">'. $opacity_text_heading .'</span>' : ''; ?>
										<?php echo "" != $opacity_text ? '<span class="sub-heading">'. $opacity_text .'</span>' : ''; ?>
									</p>
									
									<div class="row justify-content-center">
										<div class="col-lg-10 col-xl-11">
										
											<div class="row no-gutters align-items-center">
												<div class="col-lg-5 col-xl-4">

													<div class="content text-center text-lg-left">
														<h3><?php echo $heading; ?></h3>
														
														<figure>
															<img class="d-inline" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
														</figure>

														<div class="excerpt">
															<?php echo wpautop( $excerpt ); ?>
														</div>
														
														<?php if( $cta ) : ?>
															<div class="cta">
																<p class="mb-0">
																	<a class="btn" href="<?php echo $cta['url']; ?>" target="<?php echo $cta['target']; ?>">
																		<?php echo $cta['title'] ? : 'View Motorcycle' ?>
																	</a>
																</p>
															</div>
														<?php endif; ?>
													</div>
													
												</div>
												<div class="col-lg-7 col-xl-8">
													
													<figure class="product-thumbnail mb-0 text-center text-lg-left">
														<img class="d-inline" src="<?php echo $hero['url']; ?>" alt="<?php echo $hero['alt']; ?>">
													</figure>

												</div>
											</div>
										
										</div>
									</div>

								</div>
							</div>

						<?php

					endwhile;

				else :

					// no rows found

				endif;
			?>
		</div>
	</div>
	
	<main id="main" class="site-main m-0" role="main">

		<div id="primary" class="content-area">
			
			<div class="motors">
				<div class="container">
					
					<div class="row">
						<div class="col-md-12">
							
							<header class="text-center">
								<h3><?php echo $suzuki_motors_heading; ?></h3>
								<h2>
									<?php echo $suzuki_motors_subheading; ?>
								</h2>
							</header>
							
						</div>
					</div>

				</div><!-- .container -->

				<?php
					// check if the repeater field has rows of data
					if( have_rows('field_5de9c30b00537') ) :

						$item_counter = 1;
						
						?>
						<div class="row no-gutters">
							<?php
								// loop through the rows of data
								while ( have_rows('field_5de9c30b00537') ) : the_row();

									// display a sub field value
									$backround_hero 		= get_sub_field('field_5de9c414bebf7');
									$motor_heading 			= get_sub_field('field_5de9c3e8bebf5');
									$motor_subheading 		= get_sub_field('field_5de9c3ecbebf6');
									$motor_logo 			= get_sub_field('field_5de9c43dbebf8');
									$motor_product_image 	= get_sub_field('field_5de9c459bebf9');
									$cta 					= get_sub_field('field_5ed66bc2aa7de');
									
									?>
										<div class="col-lg-3">
											<div class="motors__item">

												<a href="<?php echo esc_url($cta['url']) ?>">
													<div class="item <?php echo 'item--'. $item_counter; ?>" style="background-image: url(<?php echo $backround_hero['url']; ?>);">
														<header class="text-center">
															<h3>
																<span class="d-block"><?php echo $motor_heading; ?></span>
																<?php echo $motor_subheading; ?>
															</h3>

															<figure class="mb-0">
																<img src="<?php echo $motor_logo['url']; ?>" alt="<?php echo $motor_logo['alt']; ?>">
															</figure>
														</header>
													</div>

													<div class="product-image">
														<img src="<?php echo $motor_product_image['url']; ?>" alt="<?php echo $motor_product_image['alt']; ?>">
													</div>
												</a>

											</div>

										</div>
									<?php

									// increment counter
									$item_counter++;

								endwhile;
							?>
						</div>
						<?php

					endif;
				?>

			</div><!-- .motors -->

			<div class="dealer-finder">
				
				<div id="map" class="fadein"></div>
				
				<div class="dealer-finder__content">
					<div class="container h-100">
						<div class="row h-100 align-items-center">
							<div class="col-md-12">

								<div class="info-box text-center">
									<div class="heading justify-content-center align-items-center">
										<figure class="mb-0">
											<svg width="71" height="75" viewBox="0 0 71 75" fill="none" xmlns="http://www.w3.org/2000/svg">
												<g filter="url(#filter0_d)">
												<ellipse cx="35.1289" cy="46.5" rx="15" ry="4.5" fill="#E90000" fill-opacity="0.2"/>
												</g>
												<g filter="url(#filter1_d)">
												<path d="M34.0436 29.8971C33.0477 29.8971 32.0925 29.5118 31.3883 28.826C30.6841 28.1401 30.2885 27.2099 30.2885 26.24C30.2885 25.2701 30.6841 24.3399 31.3883 23.654C32.0925 22.9682 33.0477 22.5829 34.0436 22.5829C35.0395 22.5829 35.9946 22.9682 36.6988 23.654C37.4031 24.3399 37.7987 25.2701 37.7987 26.24C37.7987 26.7203 37.7016 27.1958 37.5128 27.6395C37.3241 28.0832 37.0475 28.4864 36.6988 28.826C36.3501 29.1656 35.9362 29.435 35.4806 29.6188C35.025 29.8025 34.5367 29.8971 34.0436 29.8971ZM34.0436 16C31.255 16 28.5807 17.0789 26.6089 18.9992C24.637 20.9196 23.5293 23.5242 23.5293 26.24C23.5293 33.92 34.0436 45.2571 34.0436 45.2571C34.0436 45.2571 44.5579 33.92 44.5579 26.24C44.5579 23.5242 43.4501 20.9196 41.4783 18.9992C39.5065 17.0789 36.8321 16 34.0436 16Z" fill="#EE1A26"/>
												</g>
												<defs>
													<filter id="filter0_d" x="0.128906" y="26" width="70" height="49" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
														<feFlood flood-opacity="0" result="BackgroundImageFix"/>
														<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
														<feOffset dy="4"/>
														<feGaussianBlur stdDeviation="10"/>
														<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/>
														<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
														<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
													</filter>
													<filter id="filter1_d" x="3.5293" y="0" width="61.0286" height="69.2571" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
														<feFlood flood-opacity="0" result="BackgroundImageFix"/>
														<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
														<feOffset dy="4"/>
														<feGaussianBlur stdDeviation="10"/>
														<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/>
														<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
														<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
													</filter>
												</defs>
											</svg>
										</figure>
										<header class="fadein">
											<h2><?php echo $dealer_heading; ?></h2>
										</header>
									</div>

									<div class="cta fadein">
										<?php if( $dealer_cta ) : ?>
											<a class="btn btn-outline-primary" href="<?php echo $dealer_cta['url']; ?>">
												<?php echo isset( $dealer_cta['title'] ) ? $dealer_cta['title'] : 'Explore'; ?>
											</a>
										<?php endif; ?>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				
				<script>
					function initMap() {
						var myLatLng = {lat: <?php echo isset( $lat ) ? $lat : '14.6868401' ?>, lng: <?php echo isset( $long ) ? $long : '121.0312995' ?>};
						
						const map = new google.maps.Map(document.getElementById('map'), {
							center: myLatLng,
							zoom: 18,
							disableDefaultUI: true,
							styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
						});
						
						/* var marker = new google.maps.Marker({
							position: myLatLng,
							map: map,
							icon: theme_ajax.theme_url + '/images/map-marker-icon.svg'
						}); */
					}
				</script>
				
			</div><!-- .dealer-finder -->
			
			<style>
				@media (min-width: 1024px) {
					.set-the-trend.interactive {
						background-image: url(<?php echo $the_trend_hero['url'] ?>);
					}
						.set-the-trend.interactive:after {
							background-image: url(<?php echo esc_url( $the_trend_prod_thumbnail['url'] ); ?>);
						}
					
						.set-the-trend.image .promo-lg-image {
							background-image: url(<?php echo $promo_tablet['url'] ?? $promo_desktop['url']; ?>);
						}
					}

					@media (min-width: 1200px) {
						.set-the-trend.image .promo-lg-image {
							background-image: url(<?php echo $promo_desktop['url']; ?>);
						}
					}
			</style>

			<div class="set-the-trend <?php echo $the_trend_type; ?> fadein">
				<?php if( 'image' == $the_trend_type ) { ?>
					
					<a href="<?php echo esc_url( $the_trend_cta['url'] ); ?>">
						
						<!--.desktop-->
						<div class="d-none d-lg-block">
							<figure class="promo-lg-image"></figure>
						</div>

						<!--.mobile-->
						<div class="d-block d-lg-none">
							<img src="<?php echo $promo_desktop['url'] ?>" />
						</div>
						
					</a>

				<?php } else { ?>

					<div class="container">
						<div class="row">
							<div class="offset-lg-3 col-lg-6">
								
								<div class="content text-center">
									<figure class="fadein">
										<img src="<?php echo esc_url( $the_trend_branding_logo['url'] ); ?>" alt="<?php echo esc_attr( $the_trend_branding_logo['alt'] ); ?>" />
									</figure>	

									<h3 class="text-uppercase fadein"><?php echo $the_trend_heading; ?></h3>
									<div class="excerpt fadein">
										<?php echo wpautop( $the_trend_subheading ); ?>
									</div>
								</div>

							</div>
							
							<div class="col-lg-3">
								
								<div class="product-thumbnail">
									<div class="d-block d-lg-none text-center">
										<img src="<?php echo esc_url( $the_trend_prod_thumbnail['url'] ); ?>" alt="<?php echo esc_attr( $the_trend_prod_thumbnail['alt'] ); ?>">
									</div>
								</div>

							</div>
						</div>
					</div>
				<?php } ?>
			</div><!-- .set-the-trend -->

			<div class="video">
				<div class="container">
					<div class="d-flex flex-wrap flex-lg-nowrap video__items">
						<div class="col-md-12">
							
							<header class="text-center text-lg-left">
								<h2><?php echo $video_heading; ?></h2>
							</header>
							
						</div>
					</div>
				</div>
				
				<div class="video-wrap">
					<div class="container">
						<div class="d-flex flex-wrap flex-lg-nowrap video__items">

							<?php
								// check if the repeater field has rows of data
								if( have_rows('field_5df23af7e6ddb') ) :

									// video counter
									$video_counter = 1;
									
									// loop through the rows of data
									while ( have_rows('field_5df23af7e6ddb') ) : the_row();

										// display a sub field value
										$video_thumbnail 	= get_sub_field('field_5df36328ef6e7');
										$video_url 			= get_sub_field('field_5df23b5910def');
										$date 				= get_sub_field('field_5df23b01e6ddc');
										$title 				= get_sub_field('field_5df23b20e6dde');
										$duration 			= get_sub_field('field_5df23b31e6ddf');

										if( 3 < $video_counter ) {
											$hide_item_class = 'd-none d-lg-block';
										}
										else {
											$hide_item_class = '';
										}
										?>
											<div class="video__item item-<?php echo $video_counter; ?> <?php echo $hide_item_class; ?>">
												
												<div class="video-card">
													<a href="<?php echo $video_url; ?>">
														<div class="thumbnail">
															<figure class="mb-0 fadein">
																<img src="<?php echo esc_url( $video_thumbnail['url'] ); ?>" alt="<?php echo $video_thumbnail['alt']; ?>" />
															</figure>

															<div class="play-icon"> 
																<svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
																	<g style="mix-blend-mode:multiply" opacity="0.8">
																		<circle cx="44" cy="44" r="44" fill="#003145"/>
																	</g>
																	<path d="M58.8051 42.6766C59.3818 43.074 59.3818 43.926 58.8051 44.3234L38.3174 58.4414C37.654 58.8985 36.75 58.4236 36.75 57.6179L36.75 29.3821C36.75 28.5764 37.654 28.1015 38.3174 28.5586L58.8051 42.6766Z" fill="white"/>
																</svg>
															</div>
															
															<div class="duration fadein">
																<div class="media align-items-center">
																	<span class="time"><?php echo $duration; ?></span>
																	<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path fill-rule="evenodd" clip-rule="evenodd" d="M7.33333 4.66667V2H8C11.3133 2 14 4.68667 14 8C14 11.3133 11.3133 14 8 14C4.68 14 2 11.3133 2 8C2 6.03333 2.94667 4.3 4.40667 3.20667V3.19333L8.94 7.72667L8 8.66667L4.38667 5.05333C3.72667 5.85333 3.33333 6.88 3.33333 8C3.33333 10.58 5.42 12.6667 8 12.6667C10.58 12.6667 12.6667 10.58 12.6667 8C12.6667 5.64667 10.9267 3.71333 8.66667 3.38667V4.66667H7.33333ZM8.00006 12C7.63339 12 7.33339 11.7 7.33339 11.3333C7.33339 10.9666 7.63339 10.6666 8.00006 10.6666C8.36673 10.6666 8.66673 10.9666 8.66673 11.3333C8.66673 11.7 8.36673 12 8.00006 12ZM12 7.99995C12 7.63329 11.7 7.33328 11.3333 7.33328C10.9666 7.33328 10.6666 7.63329 10.6666 7.99995C10.6666 8.36662 10.9666 8.66662 11.3333 8.66662C11.7 8.66662 12 8.36662 12 7.99995ZM4.66682 8.66662C4.30015 8.66662 4.00015 8.36662 4.00015 7.99995C4.00015 7.63329 4.30015 7.33328 4.66682 7.33328C5.03348 7.33328 5.33348 7.63329 5.33348 7.99995C5.33348 8.36662 5.03348 8.66662 4.66682 8.66662Z" fill="white"/>
																	</svg>
																</div>
															</div>
														</div>

														<div class="content text-center">
															<div class="date fadein">
																<span><?php echo $date; ?></span>
															</div>
															
															<h3 class="fadein"><?php echo $title; ?></h3>
														</div>
													</a>
												</div>

											</div>
										<?php

										// increment counter
										$video_counter++;

									endwhile;
								endif;
							?>
						</div>

						<!--display only in mobile-->
						<div class="mt-3 mt-lg-0 view-videos d-block d-lg-none fadein">
							<div class="text-center">
								<a class="text-uppercase" href="javascript: void(0);">See All Videos</a>
							</div>
						</div>
						
					</div>
				</div>
				
			</div><!-- .video -->

		</div><!-- #primary -->
		
	</main><!-- #main -->

<?php
get_footer();