<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rs-theme
 */

$options = get_option( 'rs_theme_theme_options' );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php
wp_head();

// display tracking or analytics code
if( isset( $options['header_analytics'] ) ) {
	echo $options['header_analytics'];
}
?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="hfeed site">
	<div id="top_hidden"></div>
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'rs-theme' ); ?></a>
	
	<?php
	$masthead_style = '';
	if( get_header_image() ) {
		$masthead_style = 'background-image: url('. get_header_image() .') no-repeat 0 0; background-size: cover';
	}
	?>
	<header id="masthead" class="site-header" role="banner" style="<?php echo $masthead_style; ?>">
		
		<div class="site-header__top">
			
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-lg-12">
						
						<div class="text-right">
							
							<!-- .d-mobile -->
							<div class="d-block d-lg-none">
								<?php wp_nav_menu( array( 
									'theme_location' => 'mobile_header_top_menu',
									'menu_id' 		 => 'header-top-menu',
									'menu_class' 	 => 'm-0 header-top-menu d-flex justify-content-center list-unstyled',
									'container' 	 => 'ul'
								) ); ?>
							</div>
							
							<!-- .d-desktop -->
							<div class="d-none d-lg-block">
								<?php wp_nav_menu( array( 
									'theme_location' => 'header_top_menu',
									'menu_id' 		 => 'header-top-menu-desktop',
									'menu_class' 	 => 'm-0 header-top-menu d-flex justify-content-lg-end list-unstyled',
									'container' 	 => 'ul'
								) ); ?>
							</div>

						</div>

					</div>
				</div>
			</div>
			
		</div>

		<div class="site-header__bottom">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-lg-3">

						<div class="site-branding">
							
							<div class="media align-items-center justify-content-between justify-content-lg-start">
								<?php
									if ( is_front_page() && is_home() ) { ?>
										<h1 class="site-title mb-0">

											<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
												<?php if( isset( $options['logo'] ) ) { ?>

													<img class="d-block" src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />

												<?php } else { ?>

													<?php bloginfo( 'name' ); ?>
													
												<?php } ?>
											</a>

										</h1>
									<?php } else { ?>
										<p class="site-title mb-0">
											<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
												<?php if( isset( $options['logo'] ) ) { ?>

													<img src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />

												<?php } else { ?>

													<?php bloginfo( 'name' ); ?>

												<?php } ?>
											</a>
										</p>
									<?php
									}
								?>

								<button class="ml-4 menu-toggle" aria-controls="primary-menu" aria-expanded="false">
									<span></span>
									<span></span>
									<span></span>
								</button>
							</div>
							
							<?php
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
								<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
							<?php
							endif; ?>
						</div><!-- .site-branding -->
						
					</div>
					<div class="col-lg-9">

						<div class="float-right">
							
							<nav id="site-navigation" class="main-navigation" role="navigation">
								
								<!--tablet-->
								<div class="tablet-menu">
									<?php
										wp_nav_menu( array( 
											'theme_location' => 'tablet_menu',
											'menu_id' 		 => 'primary-menu',
											'menu_class' 	 => 'primary-menu',
											'container' 	 => 'ul'
										) );
										
										// generate_main_nav_menu();
									?>
								</div>
								
								<!--desktop-->
								<div class="desktop-menu">
									<?php
										wp_nav_menu( array( 
											'theme_location' => 'primary',
											'menu_id' 		 => 'primary-menu',
											'menu_class' 	 => 'primary-menu',
											'container' 	 => 'ul'
										) );
										
										// generate_main_nav_menu();
									?>
								</div>

							</nav><!-- #site-navigation -->
						</div>
						
					</div>
					
				</div>
				
			</div>
		</div>
	</header><!-- #masthead -->
	
	<div class="megadropdown-menu">
		
		<div class="motorcycle megamenu-item">
			<?php get_template_part('template-parts/menu/content', 'motorcycle'); ?>
		</div> <!-- .motorcycle -->
		
		<div class="activity megamenu-item">
			<?php get_template_part('template-parts/menu/content', 'activity'); ?>
		</div> <!-- .activity -->
		
		<div class="after-sale megamenu-item">
			<?php get_template_part('template-parts/menu/content', 'after-sale'); ?>
		</div> <!-- .after-sale -->

	</div> <!-- .header-navigation-megadrop-menu -->

	<div id="content" class="site-content">
