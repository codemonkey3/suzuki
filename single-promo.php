<?php
/**
 * The template for displaying all single promo posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package rs-theme
 */

get_header();

// get ACF value for later use
$the_trend_type				= get_field('field_5ec293d999797');
$hero_background 			= get_field('field_5dec6fa24d711');
$hero_branding              = get_field('field_5dec6fcb4d712');
$hero_heading 				= get_field('field_5dec6fdf4d713');
$hero_subheading            = get_field('field_5dec6ff44d714');
$hero_product_thumbnail 	= get_field('field_5dec700d4d715');

$promo_desktop				= get_field( 'field_5ec2940199798' );
$promo_tablet				= get_field( 'field_5ec2940999799' );
$promo_phone				= get_field( 'field_5ec294139979a' );
?>

    <style>
        @media (min-width: 1024px) {
            .set-the-trend.interactive {
                background-image: url(<?php echo $hero_background['url'] ?>), linear-gradient(to bottom,  hsla(0,0%,100%,1) 0%,hsla(0,0%,97%,1) 99%);
            }
                .set-the-trend.interactive:after {
                    background-image: url(<?php echo esc_url( $hero_product_thumbnail['url'] ); ?>);
                }
                
                .set-the-trend.image .promo-lg-image {
                    background-image: url(<?php echo $promo_tablet['url'] ?? $promo_desktop['url']; ?>);
                }
            }

            @media (min-width: 1200px) {
                .set-the-trend.image .promo-lg-image {
                    background-image: url(<?php echo $promo_desktop['url']; ?>);
                }
            }
    </style>
    
    <div class="set-the-trend <?php echo $the_trend_type; ?> fadein">
        <?php if( 'image' == $the_trend_type ) { ?>
					
            <a href="<?php echo esc_url( $the_trend_cta['url'] ); ?>">
                
                <!--.desktop-->
                <div class="d-none d-lg-block">
                    <figure class="promo-lg-image"></figure>
                </div>

                <!--.mobile-->
                <div class="d-block d-lg-none">
                    <img src="<?php echo $promo_desktop['url'] ?>" />
                </div>
                
            </a>

        <?php } else { ?>

            <div class="container">
                <div class="row">
                    <div class="offset-lg-3 col-lg-6">
                        
                        <div class="content text-center">
                            <figure class="mb-0 fadein">
                                <img src="<?php echo esc_url( $hero_branding['url'] ); ?>" alt="<?php echo esc_attr( $hero_branding['alt'] ); ?>" />
                            </figure>	
                            
                            <h3 class="text-uppercase fadein"><?php echo $hero_heading; ?></h3>
                            <div class="excerpt fadein">
                                <?php echo wpautop( $hero_subheading ); ?>
                            </div>
                        </div>

                    </div>
                    
                    <div class="col-lg-3">
                        
                        <div class="product-thumbnail">
                            <div class="d-block d-lg-none text-center">
                                <img src="<?php echo esc_url( $hero_product_thumbnail['url'] ); ?>" alt="<?php echo esc_attr( $hero_product_thumbnail['alt'] ); ?>">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
        <?php } ?>
    </div><!-- .set-the-trend -->
	
	<main id="main" class="site-main" role="main">
		<div class="container">
			
			<div class="row align-items-center">
                <div class="col-lg-2">
                    <div class="d-none d-sm-block">
                        <?php
                            /**
                             * Include share menu
                             */
                            get_template_part('template-parts/partials/content', 'menu');
                        ?>
                    </div>
                </div>
                
				<div class="col-lg-8">
                    
					<div id="primary" class="content-area">

                        <?php
                            while ( have_posts() ) : the_post();
                        
                                $start_date         = get_field('field_5dec6947a6c6b');
                                $end_date           = get_field('field_5dec6969a6c6c');
                                ?>

                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                    <div class="promo-period">
                                        <p class="mb-0">
                                            Promo Period: <?php echo date('M. j', strtotime($start_date)); ?> - <?php echo date('M. j, Y', strtotime($end_date)); ?>
                                        </p>
                                    </div>

                                    <header class="entry-header">
                                        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                                    </header><!-- .entry-header -->
                                    
                                    <div class="d-block d-sm-none">
                                        <?php
                                            /**
                                             * Include share menu
                                             */
                                            get_template_part('template-parts/partials/content', 'menu');
                                        ?>
                                    </div>

                                    <div class="entry-content">
                                        <?php
                                            the_content( sprintf(
                                                /* translators: %s: Name of current post. */
                                                wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'rs-theme' ), array( 'span' => array( 'class' => array() ) ) ),
                                                the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                            ) );
                                        ?>
                                    </div><!-- .entry-content -->

                                </article><!-- #post-## -->

                               <?php
                            endwhile;
                        ?>

                        <div class="cta text-center">
                            <p class="mb-0">
                                <a href="<?php echo get_permalink( 10 ); ?>">
                                    Back to list of Promos
                                </a>
                            </p>
                        </div>
						
					</div><!-- #primary -->

				</div>
			</div>

		</div> <!-- .container -->
	</main><!-- #main -->

<?php
get_footer();