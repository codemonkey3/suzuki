<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package rs-theme
 */

get_header(); ?>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
						
						<section class="error-404 not-found">
							<header class="page-header text-center">
								<h1 class="page-title"><?php _e( 'Error <strong class="fw-900">404</strong>', 'rs-theme' ); ?></h1>
							</header><!-- .page-header -->
							
							<div class="page-content">
								<p><?php esc_html_e( 'It looks like nothing was found at this location. We gonna fire a developer here, huh?', 'rs-theme' ); ?></p>
							</div><!-- .page-content -->
						</section><!-- .error-404 -->
						
					</main><!-- #main -->
				</div><!-- #primary -->

			</div>
		</div>
		
	</div> <!-- .container -->

<?php
get_footer();
