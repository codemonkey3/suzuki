<?php
/**
 * Social Media Connect
 */
final class Suzuki_Social_Connect extends WP_Widget {

	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var defaults
	 */
	protected $defaults;
	
	/**
	 * Constructor. Set the default widget options and create widget.
	 *
	 * @since 0.1.8
	 */
	public function __construct() {

		$this->defaults = array(
			'title'		=> 'Connect'
		);

		$widget_ops = array(
			'classname'   => 'suzuki-social-connect',
			'description' => __( 'Displays lists of social medias.', 'rs-theme' ),
		);

		$control_ops = array(
			'id_base' => 'suzuki-social-connect',
			'width'   => 240,
			'height'  => 300,
		);
		
		parent::__construct( 
			'suzuki-social-connect', 
			__( 'Suzuki Social Connect', 'rs-theme' ), 
			$widget_ops, 
			$control_ops
		);
	}

	public function widget( $args, $instance ) {
		extract( $args );

		/** Merge with defaults */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		echo $before_widget;

		// get ACF options settings
		$facebook_username 	= get_field('field_5deb573179c45');
		$instagram_username = get_field('field_5deb574179c46');
		$youtube_url 		= get_field('field_5deb575279c47');
		?>
		
		<div class="social-media-wrapper">
		
			<?php if( !empty( $instance['title'] ) ): ?>
				<h3 class="widget-title mt0"><?php _e( $instance['title'], 'rs-theme' ); ?></h3>
			<?php endif; ?>
			
			<ul class="social-media">
				<?php if( $facebook_username ) { ?>
					<li>
						<a href="//www.facebook.com/<?php echo $facebook_username; ?>" target="_blank">
							<svg width="13" height="13" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
								<g>
									<g>
										<path d="M288,176v-64c0-17.664,14.336-32,32-32h32V0h-64c-53.024,0-96,42.976-96,96v80h-64v80h64v256h96V256h64l32-80H288z"/>
									</g>
								</g>
							</svg>
						</a>
					</li>
				<?php } ?>

				<?php if( $instagram_username ) { ?>
					<li>
						<a href="//www.instagram.com/<?php echo $instagram_username; ?>" target="_blank">
							<svg width="13" height="13" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
								<g>
									<g>
										<path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
											C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
											h192c61.76,0,112,50.24,112,112V352z"/>
									</g>
								</g>
								<g>
									<g>
										<path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
											c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"/>
									</g>
								</g>
								<g>
									<g>
										<circle cx="393.6" cy="118.4" r="17.056"/>
									</g>
								</g>
							</svg>
						</a>
					</li>
				<?php } ?>
				
				<?php if( $youtube_url ) { ?>
					<li>
						<a href="<?php echo $youtube_url; ?>" target="_blank">
							<svg width="13" height="13" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
								<g>
									<g>
										<path d="M490.24,113.92c-13.888-24.704-28.96-29.248-59.648-30.976C399.936,80.864,322.848,80,256.064,80
											c-66.912,0-144.032,0.864-174.656,2.912c-30.624,1.76-45.728,6.272-59.744,31.008C7.36,138.592,0,181.088,0,255.904
											C0,255.968,0,256,0,256c0,0.064,0,0.096,0,0.096v0.064c0,74.496,7.36,117.312,21.664,141.728
											c14.016,24.704,29.088,29.184,59.712,31.264C112.032,430.944,189.152,432,256.064,432c66.784,0,143.872-1.056,174.56-2.816
											c30.688-2.08,45.76-6.56,59.648-31.264C504.704,373.504,512,330.688,512,256.192c0,0,0-0.096,0-0.16c0,0,0-0.064,0-0.096
											C512,181.088,504.704,138.592,490.24,113.92z M192,352V160l160,96L192,352z"/>
									</g>
								</g>
							</svg>
						</a>
					</li>
				<?php } ?>
			</ul>
			
		</div>

		<?php
		echo $after_widget;
	}

	public function update( $new_instance, $old_instance ) {
		$new_instance['title']				= $new_instance['title'];
		
		return $new_instance;
	}

	public function form( $instance ) {
		/** Merge with defaults */
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		?>
		<div class="widget-body">
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'rs-theme' ); ?>:</label>
				<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" class="widefat" />
			</p>

			<p>Social media settings is added under <a href="<?php echo admin_url('admin.php?page=theme-general-settings'); ?>" target="_blank">Theme Settings</a>.</p>
		</div>
		<?php
	}
	
}