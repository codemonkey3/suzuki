<?php
$product_thumbnail  = get_field('field_5df382a2fa7ec');
$price              = get_field('field_5dee25d53fe70');
$excerpt            = get_field('field_5df37d1dc71de'); 
?>

<div class="text-center grid-item">
    <a href="<?php the_permalink(); ?>">
        <figure>
            <img src="<?php echo esc_url( $product_thumbnail['url'] ); ?>" alt="<?php echo esc_attr( $product_thumbnail['alt'] ); ?>">
        </figure>

        <div class="content">
            <header>
                <h3><?php the_title(); ?></h3>
            </header>
            
            <div class="price">
                <span class="amount" data-price="<?php echo $price; ?>">PHP <?php echo number_format( $price, 2 ); ?></span>
            </div>
        
            <div class="excerpt">
                <?php echo wpautop( $excerpt ); ?>
            </div>

            <div class="cta">
                <p class="m-0">View Model</p>
            </div>
        </div>
    </a>
</div>