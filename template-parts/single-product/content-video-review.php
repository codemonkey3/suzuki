<div class="section-header">
    <h2 class="video-label">Video</h2>
    <h2 class="review-label">Review</h2>
</div>

<div class="d-flex flex-wrap">

    <?php
    if( have_rows('field_5dfb031ce92a1') ) :
        
        ?>
        <div class="col-left">
            
            <?php
                $video_counter = 1;
                
                // loop through the rows of data
                while ( have_rows('field_5dfb031ce92a1') ) : the_row();

                    // display a sub field value
                    $hero       = get_sub_field('field_5dfb04bfe92a6');
                    $title      = get_sub_field('field_5dfb032fe92a2');
                    $excerpt    = get_sub_field('field_5dfb0335e92a3');
                    $video_url  = get_sub_field('field_5dfb0424e92a4');
                    $duration   = get_sub_field('field_5dfb0468e92a5');

                    if( 1 == $video_counter ) {
                        ?>

                            <div class="video-review__item main d-flex align-items-center justify-content-center" style="background-image: url(<?php echo $hero['url']; ?>);">
                                <a href="<?php echo $video_url; ?>" target="_blank">
                                    <div class="content">
                                        <div class="header">
                                            <div class="media justify-content-center">
                                                <div>
                                                    <svg width="102" height="102" viewBox="0 0 102 102" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <g style="mix-blend-mode:multiply" opacity="0.8">
                                                            <ellipse cx="50.9279" cy="51.1879" rx="50.7387" ry="50.731" fill="#003145"/>
                                                        </g>
                                                        <path d="M68.1832 49.788C68.7601 50.1854 68.7601 51.0375 68.1832 51.4349L44.135 68.004C43.4716 68.461 42.5676 67.9861 42.5676 67.1805L42.5676 34.0424C42.5676 33.2368 43.4716 32.7619 44.135 33.219L68.1832 49.788Z" fill="white"/>
                                                    </svg>
                                                </div>
                                                <div>
                                                    <header>
                                                        <h2><?php echo $title; ?></h2>
                                                    </header>

                                                    <div class="excerpt">
                                                        <?php echo wpautop( $excerpt ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-end">
                                            <div class="duration">
                                                <div class="media align-items-center">
                                                    <span class="value"><?php echo $duration; ?></span>
                                                    <svg width="29" height="30" viewBox="0 0 29 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.2917 9.11847V4.41333H14.5C20.5054 4.41333 25.375 9.15376 25.375 14.9999C25.375 20.846 20.5054 25.5865 14.5 25.5865C8.4825 25.5865 3.625 20.846 3.625 14.9999C3.625 11.5299 5.34083 8.47151 7.98708 6.54241V6.51888L16.2038 14.5176L14.5 16.1762L7.95083 9.80072C6.75458 11.2123 6.04167 13.0237 6.04167 14.9999C6.04167 19.5521 9.82375 23.2339 14.5 23.2339C19.1763 23.2339 22.9583 19.5521 22.9583 14.9999C22.9583 10.8476 19.8046 7.43638 15.7083 6.86V9.11847H13.2917ZM14.5 22.0576C13.8354 22.0576 13.2917 21.5283 13.2917 20.8813C13.2917 20.2344 13.8354 19.705 14.5 19.705C15.1646 19.705 15.7083 20.2344 15.7083 20.8813C15.7083 21.5283 15.1646 22.0576 14.5 22.0576ZM21.75 14.9999C21.75 14.3529 21.2062 13.8236 20.5417 13.8236C19.8771 13.8236 19.3333 14.3529 19.3333 14.9999C19.3333 15.6469 19.8771 16.1762 20.5417 16.1762C21.2062 16.1762 21.75 15.6469 21.75 14.9999ZM8.45833 16.1762C7.79375 16.1762 7.25 15.6469 7.25 14.9999C7.25 14.3529 7.79375 13.8236 8.45833 13.8236C9.12292 13.8236 9.66667 14.3529 9.66667 14.9999C9.66667 15.6469 9.12292 16.1762 8.45833 16.1762Z" fill="white"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            </div>
                            
                        <?php
                    }

                    // increment video
                    $video_counter++;
                endwhile;

            
            ?>

        </div>
        
        <div class="col-right">

            <div class="row no-gutters">

                <?php

                    $video_counter = 1;
                        
                    // loop through the rows of data
                    while ( have_rows('field_5dfb031ce92a1') ) : the_row();

                        // display a sub field value
                        $hero       = get_sub_field('field_5dfb04bfe92a6');
                        $title      = get_sub_field('field_5dfb032fe92a2');
                        $excerpt    = get_sub_field('field_5dfb0335e92a3');
                        $video_url  = get_sub_field('field_5dfb0424e92a4');
                        $duration   = get_sub_field('field_5dfb0468e92a5');

                        if( 1 != $video_counter ) {
                            ?>
                                
                                <div class="col-lg-6 col-xl-12">
                                    <div class="video-review__item featured <?php echo 'item-' . $video_counter; ?> d-flex align-items-center align-items-lg-end justify-content-center" style="background-image: url(<?php echo $hero['url']; ?>);">

                                        <a href="<?php echo $video_url; ?>" target="_blank">
                                            
                                                <div class="content">
                                                    <div class="header">
                                                        <div class="media align-items-center justify-content-center">
                                                            <div>
                                                                <?php if( $video_url ) { ?>
                                                                    <svg width="102" height="102" viewBox="0 0 102 102" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <g style="mix-blend-mode:multiply" opacity="0.8">
                                                                            <ellipse cx="50.9279" cy="51.1879" rx="50.7387" ry="50.731" fill="#003145"/>
                                                                        </g>
                                                                        <path d="M68.1832 49.788C68.7601 50.1854 68.7601 51.0375 68.1832 51.4349L44.135 68.004C43.4716 68.461 42.5676 67.9861 42.5676 67.1805L42.5676 34.0424C42.5676 33.2368 43.4716 32.7619 44.135 33.219L68.1832 49.788Z" fill="white"/>
                                                                    </svg>
                                                                <?php } ?>
                                                            </div>
                                                            <div>
                                                                <header>
                                                                    <h2><?php echo $title; ?></h2>
                                                                </header>

                                                                <div class="excerpt d-block d-lg-none">
                                                                    <?php echo wpautop( $excerpt ); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="d-flex justify-content-end">
                                                        <?php if( $video_url ) { ?>
                                                            <div class="duration">
                                                                <div class="media align-items-center">
                                                                    <span class="value"><?php echo $duration; ?></span>
                                                                    <svg width="29" height="30" viewBox="0 0 29 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.2917 9.11847V4.41333H14.5C20.5054 4.41333 25.375 9.15376 25.375 14.9999C25.375 20.846 20.5054 25.5865 14.5 25.5865C8.4825 25.5865 3.625 20.846 3.625 14.9999C3.625 11.5299 5.34083 8.47151 7.98708 6.54241V6.51888L16.2038 14.5176L14.5 16.1762L7.95083 9.80072C6.75458 11.2123 6.04167 13.0237 6.04167 14.9999C6.04167 19.5521 9.82375 23.2339 14.5 23.2339C19.1763 23.2339 22.9583 19.5521 22.9583 14.9999C22.9583 10.8476 19.8046 7.43638 15.7083 6.86V9.11847H13.2917ZM14.5 22.0576C13.8354 22.0576 13.2917 21.5283 13.2917 20.8813C13.2917 20.2344 13.8354 19.705 14.5 19.705C15.1646 19.705 15.7083 20.2344 15.7083 20.8813C15.7083 21.5283 15.1646 22.0576 14.5 22.0576ZM21.75 14.9999C21.75 14.3529 21.2062 13.8236 20.5417 13.8236C19.8771 13.8236 19.3333 14.3529 19.3333 14.9999C19.3333 15.6469 19.8771 16.1762 20.5417 16.1762C21.2062 16.1762 21.75 15.6469 21.75 14.9999ZM8.45833 16.1762C7.79375 16.1762 7.25 15.6469 7.25 14.9999C7.25 14.3529 7.79375 13.8236 8.45833 13.8236C9.12292 13.8236 9.66667 14.3529 9.66667 14.9999C9.66667 15.6469 9.12292 16.1762 8.45833 16.1762Z" fill="white"/>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="placeholder" style="height: 100px;"></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            
                                        </a>
                                        
                                    </div>
                                </div>
                            <?php

                        }

                        // increment video
                        $video_counter++;
                    endwhile;
                ?>

            </div>
        </div>
        
    <?php
        else :

            // no rows found

        endif;
    ?>

</div>