<?php
/**
 * specifications
 */

// Overview
$overview_heading       = get_field('field_5dee3ddd52d45');
$overview_description   = get_field('field_5dee3de852d46');
$overview_max_power     = get_field('field_5dee3e0052d47');
$overview_max_torque    = get_field('field_5dee3e0a52d48');
$overview_brochure      = get_field('field_5dee3e1c52d49');

// Engine
$engine_thubmnail       = get_field('field_5df7038479cc7');
$engine_heading         = get_field('field_5df6ec313c3ff');
$engine_description     = get_field('field_5df6ec373c400');

// Chassis
$chassis_thubmnail      = get_field('field_5df703a079cc8');
$chassis_heading        = get_field('field_5df6ebf7f8013');
$chassis_description    = get_field('field_5df6ebfcf8014');

// Dimensions
$dimensions_thubmnail   = get_field('field_5df703be79cc9');
$dimensions_heading     = get_field('field_5df6eb9ce11d7');
$dimensions_description = get_field('field_5df6ebace11d8');
?>

<div class="product-specifications__filter-wrap">
    <div class="container">
        
        <div class="row no-gutters no-sm-gutters">
            <div class="col-md-12">
                
                <div class="product-specifications__filter">
                    <ul class="m-0 list-unstyled d-flex justify-content-sm-center">
                        <li class="active">
                            <a href="javascript: void(0);" data-id="0">Overview</a>
                        </li>
                        <li>
                            <a href="javascript: void(0);" data-id="1">Engine</a>
                        </li>
                        <li>
                            <a href="javascript: void(0);" data-id="2">Chassis</a>
                        </li>
                        <li>
                            <a href="javascript: void(0);" data-id="3">Dimensions</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>

    </div>
</div>
            

<div class="product-specifications__content-wrap">
    <div class="container">
        
        <div class="row no-gutters no-sm-gutters">
            <div class="col-md-12">
                <div class="product-specifications__content">

                    <ul class="m-0 list-unstyled">
                        <li data-id="0" class="active">
                            <div class="overview">
                                <div class="row">
                                    <div class="col-md-8">
                                        <header>
                                            <h2><?php echo $overview_heading; ?></h2>
                                        </header>

                                        <div class="excerpt">
                                            <?php echo $overview_description; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="item-property">
                                            <p>
                                                Category
                                                <span class="d-block">
                                                    <?php
                                                        $terms = get_the_terms( get_the_ID(), 'product-cat' );

                                                        if ( $terms && ! is_wp_error( $terms ) ) : 
                                                            $separator = '';
                                                            
                                                            foreach ( $terms as $term ) {
                                                                echo $separator . $term->name;
                                                                
                                                                // create separator
                                                                $separator = ', ';
                                                            }
                                                        endif;
                                                    ?>
                                                </span>
                                            </p>

                                            <p>
                                                Max Power
                                                <span class="d-block"><?php echo $overview_max_power; ?></span>
                                            </p>

                                            <p>
                                                Max Torque
                                                <span class="d-block"><?php echo $overview_max_torque; ?></span>
                                            </p>

                                            <?php if( $overview_brochure ) : ?>
                                                <p>
                                                    <a class="brochure-btn text-uppercase" href="<?php echo $overview_brochure['url'] ?>">
                                                        Download Brochure
                                                    </a>
                                                </p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .overview -->
                        </li>
                        
                        <li data-id="1">
                            <div class="engine">
                                
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="d-none d-sm-block">
                                            <figure class="m-0 text-center">
                                                <img src="<?php echo $engine_thubmnail['url']; ?>" alt="<?php echo $engine_thubmnail['alt']; ?>" />
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <header>
                                            <h2><?php echo $engine_heading; ?></h2>
                                        </header>

                                        <div class="excerpt">
                                            <?php echo wpautop( $engine_description ); ?>
                                        </div>
                                        
                                        <div class="d-block d-sm-none">
                                            <figure class="ml-0 mb-3 mb-sm-0 text-center">
                                                <img src="<?php echo $engine_thubmnail['url']; ?>" alt="<?php echo $engine_thubmnail['alt']; ?>" />
                                            </figure>
                                        </div>

                                        <div class="specifications">
                                            <?php
                                                // check if the repeater field has rows of data
                                                if( have_rows('field_5df6ec3b3c401') ) :

                                                    // loop through the rows of data
                                                    while ( have_rows('field_5df6ec3b3c401') ) : the_row();

                                                        // display a sub field value
                                                        $label = get_sub_field('field_5df6ec3b3c402');
                                                        $value = get_sub_field('field_5df6ec3b3c403');
                                                        
                                                        ?>
                                                        <div class="specifications__item">
                                                            <div class="media align-items-center justify-content-between">
                                                                <div class="label">
                                                                    <span><?php echo $label; ?></span>
                                                                </div>
                                                                <div class="value">
                                                                    <span><?php echo $value; ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php

                                                    endwhile;

                                                else :

                                                    // no rows found

                                                endif;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            
                            </div><!-- .engine -->
                        </li>

                        <li data-id="2">
                            <div class="chassis">
                                
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="d-none d-sm-block">
                                            <figure class="m-0 text-center">
                                                <img src="<?php echo $chassis_thubmnail['url']; ?>" alt="<?php echo $chassis_thubmnail['alt']; ?>" />
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <header>
                                            <h2><?php echo $chassis_heading; ?></h2>
                                        </header>
                                                
                                        <div class="excerpt">
                                            <?php echo wpautop( $chassis_description ); ?>
                                        </div>

                                        <div class="d-block d-sm-none">
                                            <figure class="ml-0 mb-3 mb-sm-0 text-center">
                                                <img src="<?php echo $chassis_thubmnail['url']; ?>" alt="<?php echo $chassis_thubmnail['alt']; ?>" />
                                            </figure>
                                        </div>

                                        <div class="specifications">
                                            <?php
                                                // check if the repeater field has rows of data
                                                if( have_rows('field_5df6ec00f8015') ) :
                                                    
                                                    // loop through the rows of data
                                                    while ( have_rows('field_5df6ec00f8015') ) : the_row();

                                                        // display a sub field value
                                                        $label = get_sub_field('field_5df6ec00f8016');
                                                        $value = get_sub_field('field_5df6ec00f8017');
                                                        
                                                        ?>
                                                        <div class="specifications__item">
                                                            <div class="media align-items-center justify-content-between">
                                                                <div class="label">
                                                                    <span><?php echo $label; ?></span>
                                                                </div>
                                                                <div class="value">
                                                                    <span><?php echo $value; ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php

                                                    endwhile;

                                                else :

                                                    // no rows found

                                                endif;
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- .chassis -->
                        </li>

                        <li data-id="3">
                            <div class="dimensions">

                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="d-none d-sm-block">
                                            <figure class="m-0 text-center">
                                                <img src="<?php echo $dimensions_thubmnail['url']; ?>" alt="<?php echo $dimensions_thubmnail['alt']; ?>" />
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <header>
                                            <h2><?php echo $dimensions_heading; ?></h2>
                                        </header>

                                        <div class="excerpt">
                                            <?php echo wpautop( $dimensions_description ); ?>
                                        </div>

                                        <div class="d-block d-sm-none">
                                            <figure class="ml-0 mb-3 mb-sm-0 text-center">
                                                <img src="<?php echo $dimensions_thubmnail['url']; ?>" alt="<?php echo $dimensions_thubmnail['alt']; ?>" />
                                            </figure>
                                        </div>

                                        <div class="specifications">
                                            <?php
                                                // check if the repeater field has rows of data
                                                if( have_rows('field_5df6ebc5e11d9') ) :

                                                    // loop through the rows of data
                                                    while ( have_rows('field_5df6ebc5e11d9') ) : the_row();

                                                        // display a sub field value
                                                        $label = get_sub_field('field_5df6ebd4e11da');
                                                        $value = get_sub_field('field_5df6ebdee11db');
                                                        
                                                        ?>
                                                        <div class="specifications__item">
                                                            <div class="media align-items-center justify-content-between">
                                                                <div class="label">
                                                                    <span><?php echo $label; ?></span>
                                                                </div>
                                                                <div class="value">
                                                                    <span><?php echo $value; ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php

                                                    endwhile;

                                                else :

                                                    // no rows found

                                                endif;
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- .dimensions -->
                        </li>
                    </ul>

                </div>

            </div>
        </div>
        
    </div> <!-- .container -->
</div>