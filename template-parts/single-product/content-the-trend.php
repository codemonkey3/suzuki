<?php
// Set the Trend
$promo_type  			    = get_field( 'field_5ebea50073307' );

$the_trend_hero 			= get_field( 'field_5dfa384ea8389' );
$the_trend_branding_logo 	= get_field( 'field_5dfa385aa838a' );
$the_trend_heading 			= get_field( 'field_5dfa3869a838b' );
$the_trend_subheading 		= get_field( 'field_5dfa3881a838c' );
$the_trend_prod_thumbnail 	= get_field( 'field_5dfa3893a838d' );

$promo_desktop				= get_field( 'field_5ebea53274103' );
$promo_tablet				= get_field( 'field_5ebea53974104' );
$promo_phone				= get_field( 'field_5ebea53f74105' );

$the_trend_cta 				= get_field( 'field_5dfa389ea838e' );
?>

<style>
    @media (min-width: 1024px) {
        .set-the-trend.interactive {
            background-image: url(<?php echo $the_trend_hero['url'] ?>), linear-gradient(to bottom,  hsla(0,0%,100%,1) 0%,hsla(0,0%,97%,1) 99%);
        }
            .set-the-trend.interactive:after {
                background-image: url(<?php echo esc_url( $the_trend_prod_thumbnail['url'] ); ?>);
            }
        
        .set-the-trend.image .promo-lg-image {
            background-image: url(<?php echo $promo_tablet['url'] ?? $promo_desktop['url']; ?>);
        }
    }

    @media (min-width: 1200px) {
        .set-the-trend.image .promo-lg-image {
            background-image: url(<?php echo $promo_desktop['url']; ?>);
        }
    }
</style>

<div class="set-the-trend <?php echo $promo_type; ?> fadein">
    
    <?php if( 'image' == $promo_type ) { ?>
        
        <a href="<?php echo esc_url( $the_trend_cta['url'] ); ?>">
						
            <!--.desktop-->
            <div class="d-none d-lg-block">
                <figure class="promo-lg-image"></figure>
            </div>
            
            <!--.mobile-->
            <div class="d-block d-lg-none">
                <img src="<?php echo $promo_desktop['url'] ?>" />
            </div>
            
        </a>

    <?php } else { ?>

        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    
                    <div class="content text-center">
                        <figure class="fadein">
                            <img src="<?php echo esc_url( $the_trend_branding_logo['url'] ); ?>" alt="<?php echo esc_attr( $the_trend_branding_logo['alt'] ); ?>" />
                        </figure>	

                        <h3 class="text-uppercase fadein"><?php echo $the_trend_heading; ?></h3>
                        <div class="excerpt fadein">
                            <?php echo wpautop( $the_trend_subheading ); ?>
                        </div>
                    </div>

                </div>
                
                <div class="col-lg-3">
                    
                    <div class="product-thumbnail">
                        <div class="d-block d-lg-none text-center">
                            <img src="<?php echo esc_url( $the_trend_prod_thumbnail['url'] ); ?>" alt="<?php echo esc_attr( $the_trend_prod_thumbnail['alt'] ); ?>">
                        </div>
                    </div>

                </div>
            </div>
        </div>

    <?php } ?>

</div><!-- .set-the-trend -->