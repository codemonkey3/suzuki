<?php
$terms = get_the_terms( get_the_ID(), 'product-cat' );

if ( $terms && ! is_wp_error( $terms ) ) : 
    
    foreach ( $terms as $term ) {
        $term_ids[] = $term->term_id;
        $term_name  = $term->name;
    }
    
endif;
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <header class="text-center">
                <h2 class="text-uppercase">
                    Related Motorcycles
                    <span class="d-block"><?php echo $term_name; ?></span>
                </h2>
            </header>

        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <?php
            $post_not_in            = [ get_the_ID() ];
            
            // related products
            $args = [
                'post_type'         => 'product',
                'posts_per_page'    => '3',
                'tax_query'         => [
                    [
                        'taxonomy'      => 'product-cat',
                        'field'         => 'term_id',
                        'terms'         => $term_ids,
                    ]
                ],
                'post__not_in'  => $post_not_in,
                'orderby'       => 'rand',
            ];

            // The Query
            $the_query = new WP_Query( $args );
            
            // The Loop
            if ( $the_query->have_posts() ) {
                
                while ( $the_query->have_posts() ) { $the_query->the_post();
                                                     
                    ?>
                        <div class="col-lg-6 col-xl-4">
                            
                            <div class="related-product__item fadein">
                                
                                <?php 
                                /**
                                 * Include content-grid
                                 */
                                get_template_part('template-parts/single-product/content', 'grid'); ?>
                                
                            </div>

                        </div>
                    <?php
                }
                
            }
            else {
                // no posts found
            }

            /* Restore original Post Data */
            wp_reset_postdata();
        ?>
    </div>
</div>