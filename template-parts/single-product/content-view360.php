<?php
// 360 view
$opacity_text               = get_field( 'field_5eae6f5365453' );
$opacity_text_subheading    = get_field( 'field_5e16d6eaa7ec7' );
?>
<div class="container">
    
    <div class="row justify-content-center">
        <div class="col-lg-10"> 

            <div class="view360__content">

                <div class="view360-the-trend">
                    <h2>
                        <?php echo "" != $opacity_text ? '<span class="heading">'. $opacity_text .'</span>' : ''; ?>
                        <?php echo "" != $opacity_text_subheading ? '<span class="sub-heading">'. $opacity_text_subheading .'</span>' : ''; ?></span>
                    </h2>
                </div>
                
                <div class="view360-wrap">
                    <ul class="m-0 list-unstyled">
                        <?php
                            // check if the repeater field has rows of data
                            if( have_rows('field_5dee5fc6291ce') ) :

                                // set icon view counter
                                (int) $view_counter    = 0;
                                
                                (array) $color_options = [];

                                // loop through the rows of data
                                while ( have_rows('field_5dee5fc6291ce') ) : the_row();

                                ?>
                                    <li class="<?php echo (0 == $view_counter) ? 'active' : ''; ?>" data-id="<?php echo $view_counter; ?>">
                                        <?php
                                            // display a sub field value
                                            $color_name         = get_sub_field('field_5dee5fd8291cf');
                                            $color_option       = get_sub_field('field_5dee5fff291d0');
                                            $text_color         = get_sub_field('field_5e0da120aa599');
                                            $text_size          = get_sub_field('field_5eb0cdf037452');
                                            $variation_price    = get_sub_field('field_5eb3f15d18989');
                                            $images             = get_sub_field('field_5dee6022291d1');
                                            
                                            $color_options[] = [
                                                'name'              => $color_name,
                                                'color'             => $color_option,
                                                'color_hex'         => $text_color,
                                                'text_size'         => $text_size,
                                                'variation_price'   => $variation_price,
                                            ];

                                            // set array for image-lists
                                            $imageLists     = [];
                                            
                                            if( $images ) :
                                                foreach( $images as $image ) :
                                                    // set image list of later use
                                                    $imageLists[] = '"'. $image['url'] . '"';
                                                endforeach;
                                            endif;

                                            // add javascript code here
                                            ?>
                                            <script>
                                                $(function() {
                                                    var imgList_<?php echo $view_counter; ?> = [<?php echo implode( ",", $imageLists ); ?>];
                                                    
                                                    view360( "360viewer_<?php echo $view_counter; ?>", imgList_<?php echo $view_counter; ?>);
                                                });
                                            </script>
                                            
                                            <canvas id="360viewer_<?php echo $view_counter; ?>" width="1200" height="800"></canvas>
                                            <?php

                                            // increment counter
                                            $view_counter++;

                                            ?>
                                        </li>
                                    <?php
                                endwhile;
                                

                            else :

                                // no rows found

                            endif;
                        ?>
                    </ul>
                </div>

                <div class="view360-arrow">
                    <svg width="916" height="177" viewBox="0 0 916 177" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M477.473 166.223C476.746 166.223 476.116 165.683 476.022 164.943C475.92 164.14 476.487 163.407 477.29 163.305C481.918 162.715 486.063 161.461 488.961 159.772C491.611 158.229 493.07 156.414 493.07 154.662C493.07 152.731 491.363 151.152 489.93 150.166C489.264 149.707 489.095 148.796 489.553 148.129C490.012 147.462 490.924 147.294 491.591 147.752C494.475 149.737 496 152.126 496 154.662C496 157.541 494.076 160.183 490.436 162.304C487.16 164.213 482.742 165.564 477.66 166.211C477.597 166.219 477.534 166.223 477.473 166.223Z" fill="#C7C7C7"/>
                        <path d="M470.643 164.124L466.737 160.218C466.164 159.646 465.237 159.646 464.665 160.218C464.093 160.79 464.093 161.718 464.665 162.289L465.806 163.431C461.43 162.979 457.454 161.96 454.376 160.479C450.915 158.815 448.93 156.694 448.93 154.662C448.93 152.938 450.351 151.146 452.932 149.617C453.628 149.204 453.858 148.305 453.445 147.61C453.032 146.913 452.134 146.684 451.438 147.096C446.943 149.76 446 152.681 446 154.662C446 157.912 448.524 160.916 453.106 163.12C456.663 164.83 461.264 165.976 466.277 166.419L464.665 168.031C464.093 168.602 464.093 169.53 464.665 170.102C464.951 170.388 465.326 170.531 465.701 170.531C466.076 170.531 466.451 170.388 466.737 170.102L470.643 166.196C471.215 165.624 471.215 164.696 470.643 164.124Z" fill="#C7C7C7"/>
                        <path d="M461.342 151.729V151.377C461.342 150.133 460.581 149.892 459.56 149.892C458.929 149.892 458.725 149.336 458.725 148.779C458.725 148.222 458.929 147.666 459.56 147.666C460.265 147.666 461.007 147.573 461.007 146.07C461.007 144.994 460.395 144.734 459.634 144.734C458.725 144.734 458.261 144.957 458.261 145.681C458.261 146.311 457.983 146.738 456.907 146.738C455.571 146.738 455.404 146.46 455.404 145.569C455.404 144.122 456.443 142.248 459.634 142.248C461.991 142.248 463.772 143.101 463.772 145.606C463.772 146.96 463.271 148.222 462.343 148.649C463.438 149.057 464.236 149.874 464.236 151.377V151.729C464.236 154.772 462.139 155.923 459.541 155.923C456.35 155.923 455.125 153.974 455.125 152.416C455.125 151.581 455.478 151.358 456.498 151.358C457.686 151.358 457.983 151.618 457.983 152.323C457.983 153.195 458.799 153.399 459.634 153.399C460.896 153.399 461.342 152.935 461.342 151.729Z" fill="#C7C7C7"/>
                        <path d="M475.499 151.377V151.544C475.499 154.735 473.513 155.923 470.953 155.923C468.392 155.923 466.388 154.735 466.388 151.544V146.627C466.388 143.435 468.448 142.248 471.12 142.248C474.256 142.248 475.499 144.196 475.499 145.736C475.499 146.627 475.072 146.905 474.144 146.905C473.347 146.905 472.641 146.701 472.641 145.847C472.641 145.142 471.899 144.771 471.027 144.771C469.932 144.771 469.283 145.346 469.283 146.627V148.296C469.877 147.647 470.712 147.48 471.602 147.48C473.717 147.48 475.499 148.408 475.499 151.377ZM469.283 151.748C469.283 153.028 469.914 153.585 470.953 153.585C471.992 153.585 472.604 153.028 472.604 151.748V151.581C472.604 150.226 471.992 149.707 470.934 149.707C469.932 149.707 469.283 150.189 469.283 151.414V151.748Z" fill="#C7C7C7"/>
                        <path d="M477.744 151.544V146.627C477.744 143.435 479.729 142.248 482.29 142.248C484.85 142.248 486.854 143.435 486.854 146.627V151.544C486.854 154.735 484.85 155.923 482.29 155.923C479.729 155.923 477.744 154.735 477.744 151.544ZM483.959 146.627C483.959 145.346 483.329 144.771 482.29 144.771C481.251 144.771 480.638 145.346 480.638 146.627V151.544C480.638 152.824 481.251 153.399 482.29 153.399C483.329 153.399 483.959 152.824 483.959 151.544V146.627Z" fill="#C7C7C7"/>
                        <path d="M490.371 142.234C487.947 142.234 485.976 140.263 485.976 137.84C485.976 135.417 487.947 133.445 490.371 133.445C492.794 133.445 494.765 135.417 494.765 137.84C494.765 140.263 492.794 142.234 490.371 142.234ZM490.371 136.375C489.563 136.375 488.906 137.032 488.906 137.84C488.906 138.648 489.563 139.305 490.371 139.305C491.178 139.305 491.835 138.648 491.835 137.84C491.835 137.032 491.178 136.375 490.371 136.375Z" fill="#C7C7C7"/>
                        <g opacity="0.9">
                            <path opacity="0.5" d="M389.011 129.016L381.642 138.484C152.438 126.041 74.8695 104.898 64.7359 95.8813C-2.33022 40.1601 317.931 27.4843 451.51 22.7507C824.235 30.3253 853.93 53.2678 868.976 74.9182C884.022 96.5686 639.895 135.103 529.961 138.484L564.968 165.533C909.142 142.812 935.576 85.305 907.018 61.637C855.645 3.63721 502.017 -4.65126 373.35 1.88561C122.774 10.0003 22.6661 58.0125 3.93425 81.0043C-29.9673 132.939 222.575 158.996 353.083 165.533L345.713 175L419.412 154.037L389.011 129.016Z" fill="#005E90"/>
                        </g>
                    </svg>
                </div>
                
                <div class="key-points" id="key_points">
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('field_5dee3c513fe83') ):

                            $pinpoint_counter = 1;

                            // loop through the rows of data
                            while ( have_rows('field_5dee3c513fe83') ) : the_row();

                                // display a sub field value
                                $heading    = get_sub_field('field_5dee3c513fe84');
                                $excerpt    = get_sub_field('field_5dee3c513fe85');
                                $thumbnail  = get_sub_field('field_5df8297698d88');
                                $x_pos      = get_sub_field('field_5dee63416cfcb');
                                $y_pos      = get_sub_field('field_5dee63526cfcc');

                                // check if the pinpoint is move to the bottom
                                $top_spacing = 0;

                                if( $y_pos > 60 ) {
                                    $top_spacing = 220;
                                }
                                else {
                                    $top_spacing = 120;
                                }
                                ?>
                                    <style>
                                        .single-product .view360 .key-points__item .spot__indicator_<?php echo $pinpoint_counter; ?> {
                                            left: <?php echo $x_pos ?? '0'; ?>%;
                                            top: <?php echo $y_pos ?? '0'; ?>%;
                                        }
                                        
                                        @media (min-width: 1024.98px) {
                                            .single-product .view360 .key-points__item .spot__content_<?php echo $pinpoint_counter; ?> {
                                                left: calc(<?php echo $x_pos ?? '0'; ?>% - 300px);
                                                top: calc(<?php echo $y_pos ?? '0'; ?>% - <?php echo $top_spacing; ?>px);
                                            }
                                        }
                                        
                                        @media (min-width: 1200px) {
                                            .single-product .view360 .key-points__item .spot__indicator_<?php echo $pinpoint_counter; ?> {}
                                            .single-product .view360 .key-points__item .spot__content_<?php echo $pinpoint_counter; ?> {
                                                left: calc(<?php echo $x_pos ?? '0'; ?>% - 365px);
                                            }
                                        }
                                    </style>

                                    <div class="key-points__item">
                                        <div class="spot">
                                            <span class="spot__indicator spot__indicator_<?php echo $pinpoint_counter; ?>"></span>
                                            <div class="spot__content spot__content_<?php echo $pinpoint_counter; ?>">
                                                <a href="javascript: void(0);" class="close-btn">
                                                    <svg width="14" height="13" viewBox="0 0 14 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M7.70723 6.50001L13.3532 0.854013C13.5482 0.659013 13.5482 0.342013 13.3532 0.147013C13.1582 -0.0479871 12.8412 -0.0479871 12.6462 0.147013L7.00023 5.79301L1.35423 0.147013C1.15923 -0.0479871 0.842227 -0.0479871 0.647227 0.147013C0.452227 0.342013 0.452227 0.659013 0.647227 0.854013L6.29323 6.50001L0.647227 12.146C0.452227 12.341 0.452227 12.658 0.647227 12.853C0.745227 12.951 0.873226 12.999 1.00123 12.999C1.12923 12.999 1.25723 12.95 1.35523 12.853L7.00123 7.20701L12.6472 12.853C12.7452 12.951 12.8732 12.999 13.0012 12.999C13.1292 12.999 13.2572 12.95 13.3552 12.853C13.5502 12.658 13.5502 12.341 13.3552 12.146L7.70923 6.50001H7.70723Z" fill="white"/>
                                                    </svg>
                                                </a>

                                                <figure class="m-0">
                                                    <img src="<?php echo $thumbnail['url'] ?>" alt="<?php echo $thumbnail['alt']; ?>">
                                                </figure>

                                                <div class="description">
                                                    <h3><?php echo $heading; ?></h3>
                                                    <div class="excerpt">
                                                        <?php echo wpautop( $excerpt ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php

                                // increment our counter
                                $pinpoint_counter++;
                            endwhile;

                        else :

                            // no rows found

                        endif;
                    ?>
                </div>
            </div>
            
            <div class="view360__filter">
                <?php
                    if( $color_options ) :

                        (int) $view_filter_counter = 0;
                        ?>
                            <ul class="m-0 list-unstyled d-flex flex-wrap justify-content-center">
                                <?php
                                    foreach( $color_options as $option ) {
                                        // get original product price
                                        $original_price     = get_field('field_5dee25d53fe70');

                                        $variation_price    = $option['variation_price'] ? $option['variation_price'] : $original_price;
                                        ?>
                                            <li class="<?php echo (0 == $view_filter_counter) ? 'active' : ''; ?>">
                                                <a data-id="<?php echo $view_filter_counter; ?>" href="javascript: void(0);"
                                                    data-price="<?php echo number_format($variation_price, 2); ?>"
                                                    style="background-color: <?php echo $option['color']; ?>; font-size: <?php echo $option['text_size']; ?>px;">
                                                    <span style="color:<?php echo $option['color_hex']; ?>;"><?php echo $option['name']; ?></span>
                                                </a>
                                                
                                                <?php if( $option['variation_price'] ) { ?>
                                                    <div class="tool-tip">
                                                        <p class="mb-0">Color variants for this model have different prices</p>
                                                    </div>
                                                <?php } ?>
                                            </li>
                                        <?php
                                        
                                        // increment filter
                                        $view_filter_counter++;
                                    }
                                ?>
                            </ul>
                        <?php
                    endif;
                ?>
            </div>
        </div>

    </div>
</div><!-- .container -->