<div class="container">
    <div class="row flex-nowrap">
        <div class="col-lg-3">
            
            <?php
                $activity_post_types = [
                    'news' => [
                        'post_type' => 'news',
                        'label'     => 'News',
                    ],
                    'events' => [
                        'post_type' => 'events',
                        'label'     => 'Events',
                    ],
                    'racings' => [
                        'post_type' => 'racing',
                        'label'     => 'Racing',
                    ],
                    'ssras' => [
                        'post_type' => 'ssra',
                        'label'     => 'SSRA',
                    ],
                    'clubs' => [
                        'post_type' => 'club',
                        'label'     => 'Suzuki Clubs',
                    ]
                ];
            ?>
            
            <ul class="m-0 list-unstyled activity__items">
                <?php
                    if( $activity_post_types ) {
                        // set post-type counter
                        $activity_counter = 1;

                        foreach( $activity_post_types as $post_type_link => $post_type ) {
                            ?>
                                <li>
                                    <a class="<?php echo (1 == $activity_counter) ? ' active' : ''; ?>" 
                                        href="<?php echo get_permalink() . $post_type_link; ?>" 
                                        data-id="<?php echo $post_type['post_type']; ?>">
                                        <?php echo $post_type['label']; ?>
                                    </a>
                                </li>
                            <?php
                            
                            // increment counter
                            $activity_counter++;
                        }
                    }
                ?>
            </ul>
            
        </div>
        <div class="col-lg-12">
            
            <div class="activity__item">
                <?php
                    if( $activity_post_types ) {

                        // set post-type counter
                        $post_type_counter = 1;

                        foreach( $activity_post_types as $post_type_link => $post_type ) {
                            
                            ?>
                            <div class="post-type-item <?php echo $post_type['post_type']; echo (1 == $post_type_counter) ? ' active' : ''; ?>">
                                <?php
                                    // check if post-type exist
                                    // if( post_type_exists( $post_type['post_type'] ) ) {

                                        if( 'news' == $post_type['post_type'] ) {
                                            
                                            // get post sticky posts
                                            $sticky      = get_option( 'sticky_posts' );

                                            // var_dump( $sticky );
                                            
                                            $post_type_args = [
                                                'post_type'         => 'post',
                                                'category_name'     => $post_type['post_type'],
                                                'posts_per_page'    => 1,
                                            ];
                                            
                                            if( $sticky && is_array( $sticky ) ) {
                                                $post_type_args['post__in'] = $sticky;
                                            }
                                            else {
                                                $post_type_args['order']   = 'DESC';
                                                $post_type_args['orderby'] = 'date menu_order';
                                            }

                                        }
                                        else {
                                            
                                            $post_type_args = [
                                                'post_type'         => 'post',
                                                'category_name'     => $post_type['post_type'],
                                                'posts_per_page'    => 1,
                                            ];
                                            
                                        }
                                        
                                        // The Query
                                        $post_type_the_query = new WP_Query( $post_type_args );
                                        
                                        // The Loop
                                        if ( $post_type_the_query->have_posts() ) {
                                            while ( $post_type_the_query->have_posts() ) { $post_type_the_query->the_post();

                                                // check post-type
                                                if( 'post' == get_post_type() ) {
                                                    /* grab the url for the full size featured image */
                                                    $hero_url	= get_the_post_thumbnail_url(get_the_ID(), 'full'); 
                                                }
                                                else if( 'event' == get_post_type() ) {
                                                    $hero		= get_field( 'field_5decd1feac80f' );
                                                    $hero_url	= $hero['url'];
                                                }
                                                else {
                                                    // empty url
                                                }
                                                ?>
                                                    <div class="post-type-hero <?php echo $post_type['post_type']; ?>" style="background-image: url(<?php echo $hero_url; ?>);">
                                                        <div class="row align-items-end">
                                                            <div class="col-md-4">
                                                                
                                                                <header>
                                                                    <p class="label">Latest <?php echo $post_type['label']; ?></p>
                                                                    <h3><?php the_title(); ?></h3>
                                                                </header>
                                                                
                                                                <div class="date">
                                                                    <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished">
                                                                        <?php echo get_the_date('M j, Y'); ?>
                                                                    </time>
                                                                </div>

                                                                <div class="excerpt">
                                                                    <?php the_excerpt(); ?>
                                                                </div>

                                                                <div class="cta">
                                                                    <p class="mb-0">
                                                                        <a class="btn" href="<?php the_permalink(); ?>">Read Article</a>
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                            }
                                        }
                                        else {
                                            // no posts found
                                        }
                                        
                                        // Restore original Post Data
                                        wp_reset_postdata();
                                    // }
                                ?>
                            </div>
                            <?php

                            // post-type counter
                            $post_type_counter++;
                        }
                    }
                ?>
            </div>

        </div>
    </div>
</div>