<div class="container">
    <div class="row">
        <div class="col-lg-2">
            
            <?php
                // get product category list
                $temp_terms = get_terms( array(
                    'taxonomy'      => 'product-cat',
                    'hide_empty'    => false,
                ) );
                
                if ( ! empty( $temp_terms ) && ! is_wp_error( $temp_terms ) ) {
                    
                    $terms = [];

                    // loop through the terms
                    foreach ( $temp_terms as $temp_term ) {
                        $order = get_field( 'order', $temp_term );

                        $terms[$order] = $temp_term;
                    }
                    
                    // order product-category
                    ksort( $terms );
                }

                if( $terms ) :
                    ?>
                        <ul class="m-0 list-unstyled prod-category">
                            <?php foreach( $terms as $term ) : ?>
                                <li>
                                    <a href="<?php echo get_permalink( 335 ); ?>#<?php echo $term->slug; ?>" data-id="cat-<?php echo $term->term_id; ?>">
                                        <?php echo $term->name; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php
                endif;
            ?>

        </div>
        
        <div class="col-lg-3">
            
            <?php
                if( $terms ) :
                    // store product_item for later use
                    $product_items = [];

                    // product-cat counter
                    (int) $productcat_counter = 1;

                    foreach( $terms as $term ) :

                        $args = [
                            'post_type' => 'product',
                            'tax_query' => [
                                [
                                    'taxonomy'  => 'product-cat',
                                    'field'     => 'term_id',
                                    'terms'     => $term->term_id
                                ]
                            ],
                            'posts_per_page'	=> 5,
                            'orderby' 			=> 'menu_order title',
                            'order'   			=> 'ASC',
                        ];
                        
                        // The Query
                        $the_query = new WP_Query( $args );
                        
                        // The Loop
                        if ( $the_query->have_posts() ) {

                            ?>
                            <ul class="m-0 list-unstyled prod-category__items cat-<?php echo $term->term_id; echo ( 1 == $productcat_counter ) ? ' active' : ''; ?>">
                                <?php
                                    while ( $the_query->have_posts() ) { $the_query->the_post();

                                        // menu excerpt
                                        $menu_excerpt = get_field('field_5e0db75d87619');

                                        ?>
                                            <li>
                                                <a href="javascript: void(0);" data-id="<?php echo get_the_ID(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </li>
                                        <?php

                                        $product_items[get_the_ID()] = '
                                            <div class="product-info">
                                                <h4>'. get_the_title() .'</h4>
                                                <div class="excerpt">
                                                    <p class="mb-0">'. $menu_excerpt .'</p>
                                                </div>
                                            </div>
                                        ';
                                    }
                                ?>
                            </ul>
                            <?php
                        }
                        else {
                            // no posts found
                        }
                        
                        // Restore original Post Data
                        wp_reset_postdata();

                        // increment counter
                        $productcat_counter++;

                    endforeach;
                endif;
                
                if( $product_items && is_array( $product_items ) ) {

                    // set products array
                    $products = [];
                    
                    // product-info counter
                    $productinfo_counter = 1;
                    ?>
                    <ul class="ml-0 list-unstyled prod-category__items--info">
                        <?php
                            foreach( $product_items as $prod_id => $product_item ) {
                                $product_thumbnail = get_field('field_5df382a2fa7ec', $prod_id);

                                $products[] = [
                                    'product_id'        => $prod_id,
                                    'product_thumbnail' => $product_thumbnail,
                                    'the_permalink'     => get_the_permalink( $prod_id )
                                ];

                                ?>
                                    <li class="product-item product-item-<?php echo $prod_id; echo (1 == $productinfo_counter ) ? ' active' : ''; ?>">
                                        <?php echo $product_item; ?>
                                    </li>
                                <?php

                                // increment product-info
                                $productinfo_counter++;
                            }
                        ?>
                    </ul>
                    <?php
                }
            ?>

        </div>

        <div class="col-lg-7">
            
            <?php
                // set product thumbnail counter
                $product_thumbnail_counter = 1;

                if( $products ) {
                    ?>
                    <ul class="m-0 list-unstyled prod-category__items--thumbnails">
                        <?php
                            foreach( $products as $product ) {
                                ?>
                                    <li class="item-thumbnail item-thumbnail-<?php echo $product['product_id']; echo 1 == $product_thumbnail_counter ? ' active' : ''; ?>">
                                        <div class="text-center">
                                            <figure>
                                                <img src="<?php echo $product['product_thumbnail']['url'] ?>" alt="<?php echo $product['product_thumbnail']['alt'] ?>" />
                                            </figure>
                                            
                                            <div class="cta">
                                                <p class="mb-0">
                                                    <a class="btn pr-5 pl-5" href="<?php echo $product['the_permalink']; ?>">Learn More</a>
                                                </p>
                                            </div>
                                        </div>										
                                    </li>
                                <?php

                                // increment counter
                                $product_thumbnail_counter++;
                            }
                        ?>
                    </ul>
                    <?php
                }
            ?>

        </div>
    </div>
</div>