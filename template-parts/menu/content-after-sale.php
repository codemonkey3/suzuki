<div class="container">
    <div class="row">
        <div class="col-lg-2">

            <?php
                $after_sale_post_types = [
                    'spare-parts'    => 'Spare Parts',
                    'apparels' 	     => 'Apparel'
                ];
            ?>

            <ul class="m-0 list-unstyled after-sale__items">
                <?php
                    // set post-type counter
                    $after_sale_counter = 1;
                    
                    foreach( $after_sale_post_types as $post_type => $post_type_label ) { ?>
                    <li>
                        <a class="<?php echo (1 == $after_sale_counter) ? ' active' : ''; ?>" href="<?php echo get_permalink() . $post_type; ?>" data-id="<?php echo $post_type; ?>">
                            <?php echo $post_type_label; ?>
                        </a>
                    </li>
                    <?php
                    
                    // increment counter
                    $after_sale_counter++;
                }
                ?>
            </ul>

        </div>

        <div class="col-lg-7">

            

        </div>
    </div>
</div>