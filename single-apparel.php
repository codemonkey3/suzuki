<?php
/**
 * The template for displaying all single promo posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package rs-theme
 */

get_header();

$thumbnail      = get_field('field_5dec8d6ea4767');
$price 	        = get_field('field_5dec8cb9a4766');
?>
	
	<main id="main" class="site-main spare-part" role="main">

        <header class="heading text-center">
            <h3>
                Suzuki Guaranted
            </h3>
            <h2>Apparel</h2>
        </header><!-- .entry-header -->

		<div class="container">
			
			<div class="row justify-content-center">
				<div class="col-lg-10">

					<div id="primary" class="content-area">

                        <?php while ( have_posts() ) : the_post(); ?>

                            <div class="row align-items-center">
                                <div class="col-md-5">

                                    <figure>
                                        <img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt'] ?>" />
                                    </figure>

                                </div>

                                <div class="col-md-7">

                                    <header class="entry-header">
                                        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                                    </header><!-- .entry-header -->

                                    <div class="price">
                                        <h3>
                                            <span class="d-block">Price</span>
                                            PHP <?php echo number_format( $price, 2 ); ?>
                                        </h3>
                                    </div>

                                    <div class="excerpt">
                                        <?php the_content(); ?>
                                    </div>

                                </div>
                            </div>

                        <?php endwhile; ?>
						
                    </div><!-- #primary -->
                    
                    <div class="cta">
                        <div class="row align-items-center">
                            <div class="col-md-12">

                                <div class="text-center">
                                    <a href="<?php echo get_permalink( 14 ); ?>">
                                        Get to list of Apparel
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

				</div>

			</div>

		</div> <!-- .container -->
	</main><!-- #main -->

<?php
get_footer();