<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package rs-theme
 */

get_header();

// get ACF value for later use
$thumbnail          = get_field('field_5deb4a97b057e');
$price              = get_field('field_5deb4aa1b057f');
$maintenance_guide  = get_field('field_5deb4ac708f0a');
?>
	
	<main id="main" class="site-main spare-part" role="main">

        <header class="heading text-center">
            <h3>
                Suzuki Guaranted
            </h3>
            <h2>Spare Parts</h2>
        </header><!-- .entry-header -->

        <div class="spare-parts-wrap">
            <div class="container">
                
                <div class="row justify-content-center">
                    <div class="col-lg-10">

                        <div id="primary" class="content-area">

                            <?php while ( have_posts() ) : the_post(); ?>

                                <div class="row align-items-center">
                                    <div class="col-md-5">

                                        <figure>
                                            <img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt'] ?>" />
                                        </figure>

                                    </div>
                                    <div class="col-md-7">

                                        <header class="entry-header">
                                            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                                        </header><!-- .entry-header -->

                                        <div class="price">
                                            <h3>
                                                <span class="d-block">Price</span>
                                                PHP
                                                <?php
                                                    if( $price ) {
                                                        echo number_format( $price, 2 );
                                                    }
                                                    else {
                                                        echo 'n/a';
                                                    }
                                                ?>
                                            </h3>
                                        </div>

                                        <div class="excerpt">
                                            <?php the_content(); ?>
                                        </div>

                                        <?php
                                            // check if the repeater field has rows of data
                                            if( have_rows('field_5deb4ad878322') ):

                                                ?>
                                                    <div class="specifications">
                                                        <?php
                                                            // loop through the rows of data
                                                            while ( have_rows('field_5deb4ad878322') ) : the_row();
                                                                
                                                                // display a sub field value
                                                                $spec_name  = get_sub_field('spec_name');
                                                                $spec_value = get_sub_field('spec_value');
                                                                ?>
                                                                    <div class="specifications__item">
                                                                        <div class="media align-items-center justify-content-between">
                                                                            <div class="label">
                                                                                <span><?php echo $spec_name; ?></span>
                                                                            </div>
                                                                            <div class="value">
                                                                                <span><?php echo $spec_value; ?></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php

                                                            endwhile;
                                                        ?>
                                                    </div>
                                                <?php

                                            else :

                                                // no rows found

                                            endif;
                                        ?>
                                        
                                        <?php if( $maintenance_guide ) : ?>
                                            
                                            <div class="cta">
                                                <a href="<?php echo esc_url( $maintenance_guide['url'] ); ?>" target="<?php echo $maintenance_guide['target']; ?>">
                                                    
                                                    <div class="media align-items-center">
                                                        <svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M12.5003 6.65969H15.8337L10.0003 11.5668L4.16699 6.65969H7.50033V2.45361H12.5003V6.65969ZM4.16699 14.3708V12.9688H15.8337V14.3708H4.16699Z" fill="#007BBB"/>
                                                        </svg>
                                                        
                                                        <span>
                                                            Download Maintenance Guide
                                                        </span>
                                                    </div>
                                                </a>
                                            </div>

                                        <?php endif; ?>
                                        
                                    </div>
                                </div>

                            <?php endwhile; // End of the loop. ?>
                            
                        </div><!-- #primary -->

                    </div>
                    
                </div>

            </div> <!-- .container -->
        </div>
	</main><!-- #main -->

<?php
get_footer();